<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UomConversion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uom_conversion', function (Blueprint $table) {
            Schema::dropIfExists('uom_conversion_master');
            Schema::dropIfExists('brand_master');
            Schema::dropIfExists('item_brand');

            $table->id();
            $table->string('kode_barang');
            $table->string('tipe_satuan');
            $table->decimal('value_uom_sell')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uom_conversion');
    }
}
