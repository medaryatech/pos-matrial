<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductVarian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_varian', function (Blueprint $table) {
            $table->id();
            $table->string('kode_barang');
            $table->string('nama_varian');
            $table->integer('conv_varian');
            $table->integer('hpp_varian');
            $table->integer('sku_varian');
            $table->integer('harga_varian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_varian');
    }
}
