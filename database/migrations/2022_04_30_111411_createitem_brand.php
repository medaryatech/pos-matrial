<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateitemBrand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_brand', function (Blueprint $table) {
            $table->id();
            $table->integer('brand_id');
            $table->string('nama');
            $table->string('kode_item')->nullable();
            $table->string('value_satuan')->nullable();
            $table->string('type_volume')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_brand');
    }
}
