<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblMasterUom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uom_conversion_master', function (Blueprint $table) {
            $table->id();
            $table->integer('brand_id');
            $table->integer('item_id');
            // $table->string('hitung_satuan')->nullable();
            // $table->string('tipe_volume')->nullable();
            $table->decimal('tipe_uom_sell')->nullable();
            $table->integer('harga_beli');
            $table->integer('harga_jual');
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uom_conversion_master');
    }
}
