<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductHargaGrosir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_harga_grosir', function (Blueprint $table) {
            $table->id();
            $table->string('kode_barang');
            $table->integer('minimal_grosir');
            $table->integer('harga_grosir');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_harga_grosir');
    }
}
