<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('kode_transaksi');
            $table->string('kode_barang');
            $table->string('nama_barang');
            $table->bigInteger('hpp');
            $table->bigInteger('harga');
            $table->integer('jumlah');
            $table->bigInteger('total_barang');
            $table->bigInteger('total_barang_hpp');
            $table->bigInteger('subtotal');
            $table->bigInteger('subtotal_hpp');
            $table->integer('diskon');
            $table->bigInteger('total');
            $table->bigInteger('total_hpp');
            $table->integer('po');
            $table->integer('id_pelanggan');
            $table->string('pelanggan');
            $table->bigInteger('bayar');
            $table->bigInteger('kembali');
            $table->integer('id_kasir');
            $table->string('kasir');
            $table->string('add_val')->nullable();
            $table->integer('value_add_val');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
