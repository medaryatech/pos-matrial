<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Category;
use Illuminate\Database\Eloquent\Model;

class CategoryMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
        [
            'nama' => 'Cat tembok, Pelapis anti Bocor, dan keperluannya',
            'keterangan' => 'Cat tembok dan plamir berbagai merk, Aquaproof dan sejenisnya, Serat Fiber, Pylox dan cat minyak'
        ],[
            'nama' => 'Kayu',
            'keterangan' => 'Berbagai Jenis kayu, triplek, lis kayu, reng, kaso'
        ],[
            'nama' => 'Semen, gypsum, Papan Semen, Beragam jenis Bata dan konblok',
            'keterangan' => 'Semen berbagai merk, gymsum, papan semen, Semen khusus (hebel, keramik, acian), Bata Merah, Konblock, Hebel, Batako, Loster, Grassblock, Glassblock, '
        ],[
            'nama' => 'Keramik dan keperluannya',
            'keterangan' => 'Keramik Lantai, keramik kamar mandi, keramik dinding, Nat Keramik, '
        ],[
            'nama' => 'PVC Peralon dan acc',
            'keterangan' => 'pipa, klem pipa, talang, Keni (sambungan pipa), '
        ],[
            'nama' => 'Perlengkapan Air, Toren dan Accerories Kamar Mandi dalam',
            'keterangan' => 'Keran, shower, kloset, bak mandi, fleksibel, keran cabang, jet shower, selang air, selang timbang'
        ],[
            'nama' => 'Paku, Baut dan Kawat',
            'keterangan' => 'Paku, beragam kawat (kawat nyamuk, kawat ayak), Baut dan fisher, dinabol, Baut, Kawat bendrat, Kawat Jemuran'
        ],[
            'nama' => 'Listrik',
            'keterangan' => 'Kabel, Fitting lampu, Stopkontak, Steker, Lampu, klem kabel'
        ],[
            'nama' => 'Baja ringan, Besi, keperluan Atap dan Cor',
            'keterangan' => 'Baja ringan, reng baja, spandek, bondeck, karpet talang, seng talang, Fiber Atap, Beragam jenis besi, Ring, Wermes, Aquagard, supersemen, Plastik Cor'
        ],[
            'nama' => 'alat-alat tukang',
            'keterangan' => 'Palu, Meteran, tang, alat potong, bor grinda, mata bor, kape, golok, gerobak, Ember, Bak Rol Cat, Kuas, Rol Cat, Tangga, tespen, Kunci Pas, Kikir, Kunci Rivet, mesin potong'
        ],[
            'nama' => 'Beragam jenis Lem',
            'keterangan' => 'lem kayu, besi, kaca, lem tembak, lem fiber, lem asbes, '
        ],[
            'nama' => 'Pintu dan Kunci',
            'keterangan' => 'Pintu PVC Kamar mandi, Pintu kayu, kunci kecil sedang besar, slot, silinder kunci, tarikan pintu, rel pintu geser, Gembok, rel pager, Engsel Pintu'
        ],[
            'nama' => 'Lain-lain',
            'keterangan' => 'Soda Api, Spirtus, Karbit Kiloan, Karbit buah, Porfix'
        ]
        ]);
    }
}
