<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use PDF;
use Auth;
use Carbon\Carbon;
use App\User;
use App\Acces;
use App\Market;
use App\Supply;
use App\SupplyBahanBaku;
use App\Transaction;
use App\Exports\TransactionExport;
use App\Exports\IncomeExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportManageController extends Controller
{
    // Show View Report Transaction
    public function reportTransaction()
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)->first();

        if ($check_access->kelola_laporan != 1) {
            return back();
        }

        if(Transaction::first()) {
            $start_date = Transaction::oldest()->first()->created_at;
        } else {
            $start_date = Carbon::now();
        }
        $end_date = Carbon::now()->endOfDay();

        /*
         * Kalau mau setiap tanggal ada row data nya, walaupun tidak ada transaksi pada tanggal tersebut
         */
//        $period = new \DatePeriod(new \DateTime($start_date), new \DateInterval('P1D'), new \DateTime($end_date .' +1 day'));
//        foreach ($period as $date) {
//            $range[$date->format("Y-m-d")] = 0;
//        }
//
//        $data = DB::table('transactions')
//            ->select(DB::raw('DATE(created_at) as time'), DB::raw('count(*) as count'))
//            ->whereDate('created_at', '>=', date($start_date).' 00:00:00')
//            ->whereDate('created_at', '<=', date($end_date).' 00:00:00')
//            ->groupBy('time')
//            ->orderBy('time')
//            ->get();
//
//        foreach($data as $val){
//            $dbData[$val->time] = $val->count;
//        }
//
//        $data = array_replace($range, $dbData);

        /*
         * Hanya tanggal yang ada data transaksinya saja (seperti yg berjalan sekarang)
         */
        $transactions = DB::table('transactions')
            ->select('kode_transaksi', 'created_at', 'total', 'diskon', 'pelanggan', 'po', 'kasir', 'add_val', 'value_add_val')
            ->whereBetween('created_at', [$start_date, $end_date])
            ->groupBy('kode_transaksi')
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        $income = $transactions->sum('total');

        $array = array();
        foreach ($transactions as $no => $transaction) {
            array_push($array, $transactions[$no]->created_at);
        }
        $dates = array_unique($array);
        rsort($dates);

        $arr_ammount = count($dates);
        $incomes_data = array();
        if($arr_ammount > 1){
            for ($i = 0; $i < 1; $i++) { 
                array_push($incomes_data, $dates[$i]);	
            }
        }elseif($arr_ammount > 0){
            for ($i = 0; $i < $arr_ammount; $i++) { 
                array_push($incomes_data, $dates[$i]);
            }
        }
        $incomes = array_reverse($incomes_data);
        $keyword = '';
        $start_date_search = '';
        $end_date_search = '';
        return view('report.report_transaction', compact('transactions', 'income', 'start_date', 'end_date', 'incomes', 'keyword', 'start_date_search', 'end_date_search'));
    }

    public function reportTransactionIncome()
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)
        ->first();

        if ($check_access->kelola_laporan != 1) {
            return back();
        }

        if(Transaction::first()) {
            $start_date = Transaction::oldest()->first()->created_at;
        } else {
            $start_date = Carbon::now();
        }
        $end_date = Carbon::now()->endOfDay();

        $transactions = DB::table('transactions')
            ->select('kode_transaksi', 'created_at', 'total', 'diskon', 'pelanggan', 'po', 'kasir', 'add_val', 'value_add_val', 'total_hpp')
            ->whereBetween('created_at', [$start_date, $end_date])
            ->groupBy('kode_transaksi')
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        $income = $transactions->sum('total');
        $total_hpp = $transactions->sum('total_hpp');

        $array = array();
        foreach ($transactions as $no => $transaction) {
            array_push($array, $transactions[$no]->created_at);
        }
        $dates = array_unique($array);
        rsort($dates);

        $arr_ammount = count($dates);
        $incomes_data = array();
        if($arr_ammount > 1){
            for ($i = 0; $i < 1; $i++) {
                array_push($incomes_data, $dates[$i]);
            }
        }elseif($arr_ammount > 0){
            for ($i = 0; $i < $arr_ammount; $i++) {
                array_push($incomes_data, $dates[$i]);
            }
        }
        $incomes = array_reverse($incomes_data);
        $keyword = '';
        $start_date_search = '';
        $end_date_search = '';
        return view('report.report_transaction_income', compact('transactions', 'income', 'start_date', 'end_date', 'incomes', 'total_hpp', 'keyword', 'start_date_search', 'end_date_search'));
    }

    // Show View Report Worker
    public function reportWorker()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if ($check_access->kelola_laporan != 1) {
            return back();
        }
        $users = User::all();
        return view('report.report_worker', compact('users'));
    }

    // Filter Report Transaction
    public function filterTransaction(Request $req)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if ($check_access->kelola_laporan != 1) {
            return back();
        }
        $start_date = '';
        $end_date = '';
        if($req->tgl_awal && $req->tgl_akhir) {
            $start_date = $req->tgl_awal;
            $end_date = $req->tgl_akhir;
            $start_date2 = $start_date[6].$start_date[7].$start_date[8].$start_date[9].'-'.$start_date[3].$start_date[4].'-'.$start_date[0].$start_date[1].' 00:00:00';
            $end_date2 = $end_date[6].$end_date[7].$end_date[8].$end_date[9].'-'.$end_date[3].$end_date[4].'-'.$end_date[0].$end_date[1].' 23:59:59';
        } else if($req->tgl_awal && !$req->tgl_akhir) {
            $start_date = $req->tgl_awal;
            $end_date = $req->tgl_awal;
            $start_date2 = $start_date[6].$start_date[7].$start_date[8].$start_date[9].'-'.$start_date[3].$start_date[4].'-'.$start_date[0].$start_date[1].' 00:00:00';
            $end_date2 = $end_date[6].$end_date[7].$end_date[8].$end_date[9].'-'.$end_date[3].$end_date[4].'-'.$end_date[0].$end_date[1].' 23:59:59';
        }
        

        if($req->tgl_awal) {
            $name = $req->search;
            
            if($name === 'po' || $name === 'cash' || $name === 'transfer' || $name === 'PO' || $name === 'CASH' || $name === 'TRANSFER') {
                if($name === 'po' || $name === 'PO') {
                    $name = 1;
                }else if($name === 'cash' || $name === 'CASH') {
                    $name = 2;
                }else if($name === 'transfer' || $name === 'TRANSFER') {
                    $name = 3;
                }
                $transactions = DB::table('transactions')
                ->where(function($query) use ($name){
                    $query->where('po', 'LIKE', '%'.$name.'%');
                })
                ->orderBy('created_at', 'desc')
                ->whereBetween('created_at', array($start_date2, $end_date2))
                ->groupBy('kode_transaksi')
                ->paginate(20);
            } else {
                $transactions = DB::table('transactions')
                ->where(function($query) use ($name){
                    $query->where('kode_transaksi', 'LIKE', '%'.$name.'%');
                    $query->orWhere('pelanggan', 'LIKE', '%'.$name.'%');
                    $query->orWhere('po', 'LIKE', '%'.$name.'%');
                    $query->orWhere('nama_barang', 'LIKE', '%'.$name.'%');
                })
                ->orderBy('created_at', 'desc')
                ->whereBetween('created_at', array($start_date2, $end_date2))
                ->groupBy('kode_transaksi')
                ->paginate(20);
            }
            
            
           
        } else {
            $name = $req->search;
            if($name === 'po' || $name === 'cash' || $name === 'transfer' || $name === 'PO' || $name === 'CASH' || $name === 'TRANSFER') {
                if($name === 'po' || $name === 'PO') {
                    $name = 1;
                }else if($name === 'cash' || $name === 'CASH') {
                    $name = 2;
                }else if($name === 'transfer' || $name === 'TRANSFER') {
                    $name = 3;
                }
                $transactions = DB::table('transactions')
                ->where(function($query) use ($name){
                    $query->where('po', 'LIKE', '%'.$name.'%');
                })
                ->orderBy('created_at', 'desc')
                ->groupBy('kode_transaksi')
                ->paginate(20);
            } else {
                $transactions = DB::table('transactions')
                ->where(function($query) use ($name){
                    $query->where('kode_transaksi', 'LIKE', '%'.$name.'%');
                    $query->orWhere('pelanggan', 'LIKE', '%'.$name.'%');
                    $query->orWhere('po', 'LIKE', '%'.$name.'%');
                    $query->orWhere('nama_barang', 'LIKE', '%'.$name.'%');
                })
                ->orderBy('created_at', 'desc')
                ->groupBy('kode_transaksi')
                ->paginate(20);
            }
        }

        $transactions->appends(['search' => $req->search]);

        $income = $transactions->sum('total');

        $array = array();
        foreach ($transactions as $no => $transaction) {
            array_push($array, $transactions[$no]->created_at);
        }
        $dates = array_unique($array);
        rsort($dates);

        $arr_ammount = count($dates);
        $incomes_data = array();
        if($arr_ammount > 1){
            for ($i = 0; $i < 1; $i++) { 
                array_push($incomes_data, $dates[$i]);	
            }
        }elseif($arr_ammount > 0){
            for ($i = 0; $i < $arr_ammount; $i++) { 
                array_push($incomes_data, $dates[$i]);
            }
        }
        $incomes = array_reverse($incomes_data);
        $keyword = $req->search;
        $start_date_search = $start_date;
        $end_date_search = $end_date;
        return view('report.report_transaction', compact('transactions', 'income', 'start_date', 'end_date', 'incomes', 'keyword', 'start_date_search', 'end_date_search'));
    }

    // Filter Report Transaction
    public function filterTransactionIncome(Request $req)
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if ($check_access->kelola_laporan != 1) {
            return back();
        }
        $start_date = '';
        $end_date = '';
        if($req->tgl_awal && $req->tgl_akhir) {
            $start_date = $req->tgl_awal;
            $end_date = $req->tgl_akhir;
            $start_date2 = $start_date[6].$start_date[7].$start_date[8].$start_date[9].'-'.$start_date[3].$start_date[4].'-'.$start_date[0].$start_date[1].' 00:00:00';
            $end_date2 = $end_date[6].$end_date[7].$end_date[8].$end_date[9].'-'.$end_date[3].$end_date[4].'-'.$end_date[0].$end_date[1].' 23:59:59';
        } else if($req->tgl_awal && !$req->tgl_akhir) {
            $start_date = $req->tgl_awal;
            $end_date = $req->tgl_awal;
            $start_date2 = $start_date[6].$start_date[7].$start_date[8].$start_date[9].'-'.$start_date[3].$start_date[4].'-'.$start_date[0].$start_date[1].' 00:00:00';
            $end_date2 = $end_date[6].$end_date[7].$end_date[8].$end_date[9].'-'.$end_date[3].$end_date[4].'-'.$end_date[0].$end_date[1].' 23:59:59';
        }
        

        if($req->tgl_awal) {
            $name = $req->search;
            
            if($name === 'po' || $name === 'cash' || $name === 'transfer' || $name === 'PO' || $name === 'CASH' || $name === 'TRANSFER') {
                if($name === 'po' || $name === 'PO') {
                    $name = 1;
                }else if($name === 'cash' || $name === 'CASH') {
                    $name = 2;
                }else if($name === 'transfer' || $name === 'TRANSFER') {
                    $name = 3;
                }
                $transactions = DB::table('transactions')
                ->where(function($query) use ($name){
                    $query->where('po', 'LIKE', '%'.$name.'%');
                })
                ->orderBy('created_at', 'desc')
                ->whereBetween('created_at', array($start_date2, $end_date2))
                ->groupBy('kode_transaksi')
                ->paginate(20);
            } else {
                $transactions = DB::table('transactions')
                ->where(function($query) use ($name){
                    $query->where('kode_transaksi', 'LIKE', '%'.$name.'%');
                    $query->orWhere('pelanggan', 'LIKE', '%'.$name.'%');
                    $query->orWhere('po', 'LIKE', '%'.$name.'%');
                    $query->orWhere('nama_barang', 'LIKE', '%'.$name.'%');
                })
                ->orderBy('created_at', 'desc')
                ->whereBetween('created_at', array($start_date2, $end_date2))
                ->groupBy('kode_transaksi')
                ->paginate(20);
            }
            
            
           
        } else {
            $name = $req->search;
            if($name === 'po' || $name === 'cash' || $name === 'transfer' || $name === 'PO' || $name === 'CASH' || $name === 'TRANSFER') {
                if($name === 'po' || $name === 'PO') {
                    $name = 1;
                }else if($name === 'cash' || $name === 'CASH') {
                    $name = 2;
                }else if($name === 'transfer' || $name === 'TRANSFER') {
                    $name = 3;
                }
                $transactions = DB::table('transactions')
                ->where(function($query) use ($name){
                    $query->where('po', 'LIKE', '%'.$name.'%');
                })
                ->orderBy('created_at', 'desc')
                ->groupBy('kode_transaksi')
                ->paginate(20);
            } else {
                $transactions = DB::table('transactions')
                ->where(function($query) use ($name){
                    $query->where('kode_transaksi', 'LIKE', '%'.$name.'%');
                    $query->orWhere('pelanggan', 'LIKE', '%'.$name.'%');
                    $query->orWhere('po', 'LIKE', '%'.$name.'%');
                    $query->orWhere('nama_barang', 'LIKE', '%'.$name.'%');
                })
                ->orderBy('created_at', 'desc')
                ->groupBy('kode_transaksi')
                ->paginate(20);
            }
        }

        $transactions->appends(['search' => $req->search]);
        $income = $transactions->sum('total');
        $total_hpp = $transactions->sum('total_hpp');

        $array = array();
        foreach ($transactions as $no => $transaction) {
            array_push($array, $transactions[$no]->created_at);
        }
        $dates = array_unique($array);
        rsort($dates);

        $arr_ammount = count($dates);
        $incomes_data = array();
        if($arr_ammount > 1){
            for ($i = 0; $i < 1; $i++) {
                array_push($incomes_data, $dates[$i]);
            }
        }elseif($arr_ammount > 0){
            for ($i = 0; $i < $arr_ammount; $i++) {
                array_push($incomes_data, $dates[$i]);
            }
        }
        $incomes = array_reverse($incomes_data);
        $keyword = $req->search;
        $start_date_search = $start_date;
        $end_date_search = $end_date;
        return view('report.report_transaction_income', compact('transactions', 'income', 'start_date', 'end_date', 'incomes', 'total_hpp', 'keyword', 'start_date_search', 'end_date_search'));
    }

    // Filter Report Worker
    public function filterWorker($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_laporan != 1){
            return back();
        }
        $users = User::orderBy($id, 'asc')
        ->get();
        return view('report.filter_table.filter_table_worker', compact('users'));
    }

    // Filter Chart Transaction
    public function chartTransaction($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_laporan == 1){
        	$supplies = Transaction::all();
            $array = array();
            foreach ($supplies as $no => $supply) {
                array_push($array, $supplies[$no]->created_at->toDateString());
            }
            $dates = array_unique($array);
            rsort($dates);
            $arr_ammount = count($dates);
            $incomes_data = array();

            if($id == 'hari'){
        		if($arr_ammount > 1){
    	        	for ($i = 0; $i < 1; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }elseif($arr_ammount > 0){
    	        	for ($i = 0; $i < $arr_ammount; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }
    	        $incomes = array_reverse($incomes_data);
    	        $total = array();
    	        foreach ($incomes as $no => $income) {
    	        	array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang'));
    	        }

    	        return response()->json([
    	        	'incomes' => $incomes,
    	        	'total' => $total
    	        ]);
        	}elseif($id == 'minggu'){
        		if($arr_ammount > 7){
    	        	for ($i = 0; $i < 7; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }elseif($arr_ammount > 0){
    	        	for ($i = 0; $i < $arr_ammount; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }
    	        $incomes = array_reverse($incomes_data);
    	        $total = array();
    	        foreach ($incomes as $no => $income) {
    	        	array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang'));
    	        }

    	        return response()->json([
    	        	'incomes' => $incomes,
    	        	'total' => $total
    	        ]);
        	}elseif($id == 'bulan'){
        		if($arr_ammount > 30){
    	        	for ($i = 0; $i < 30; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }elseif($arr_ammount > 0){
    	        	for ($i = 0; $i < $arr_ammount; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }
    	        $incomes = array_reverse($incomes_data);
    	        $total = array();
    	        foreach ($incomes as $no => $income) {
    	        	array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang'));
    	        }

    	        return response()->json([
    	        	'incomes' => $incomes,
    	        	'total' => $total
    	        ]);
        	}elseif($id == 'tahun'){
        		if($arr_ammount > 365){
    	        	for ($i = 0; $i < 365; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }elseif($arr_ammount > 0){
    	        	for ($i = 0; $i < $arr_ammount; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }
    	        $incomes = array_reverse($incomes_data);
    	        $total = array();
    	        foreach ($incomes as $no => $income) {
    	        	array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang'));
    	        }

    	        return response()->json([
    	        	'incomes' => $incomes,
    	        	'total' => $total
    	        ]);
        	}
        }else{
            return back();
        }
    }

    // Filter Chart Transaction
    public function totTransaction($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_laporan == 1){
        	$supplies = Transaction::all();
            $array = array();
            foreach ($supplies as $no => $supply) {
                array_push($array, $id);
            }
            $dates = array_unique($array);
            rsort($dates);
            $arr_ammount = count($dates);
            $incomes_data = array();

            if($arr_ammount > 1){
                for ($i = 0; $i < 1; $i++) {
                    array_push($incomes_data, $dates[$i]);
                }
            }elseif($arr_ammount > 0){
                for ($i = 0; $i < $arr_ammount; $i++) {
                    array_push($incomes_data, $dates[$i]);
                }
            }
            $incomes = array_reverse($incomes_data);
            $total = array();
            foreach ($incomes as $no => $income) {
                array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang'));
            }

            return response()->json([
                'incomes' => $incomes,
                'total' => $total
            ]);
        }else{
            return back();
        }
    }

    // Filter Chart Transaction
    public function chartTransactionLaba($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_laporan == 1){
        	$supplies = Transaction::all();
            $array = array();
            foreach ($supplies as $no => $supply) {
                array_push($array, $supplies[$no]->created_at->toDateString());
            }
            $dates = array_unique($array);
            rsort($dates);
            $arr_ammount = count($dates);
            $incomes_data = array();

            if($id == 'hari'){
        		if($arr_ammount > 1){
    	        	for ($i = 0; $i < 1; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }elseif($arr_ammount > 0){
    	        	for ($i = 0; $i < $arr_ammount; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }
    	        $incomes = array_reverse($incomes_data);
    	        $total = array();
    	        foreach ($incomes as $no => $income) {
    	        	array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang')-Transaction::whereDate('created_at', $income)->sum('total_barang_hpp'));
    	        }

    	        return response()->json([
    	        	'incomes' => $incomes,
    	        	'total' => $total
    	        ]);
        	}elseif($id == 'minggu'){
        		if($arr_ammount > 7){
    	        	for ($i = 0; $i < 7; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }elseif($arr_ammount > 0){
    	        	for ($i = 0; $i < $arr_ammount; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }
    	        $incomes = array_reverse($incomes_data);
    	        $total = array();
    	        foreach ($incomes as $no => $income) {
    	        	array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang')-Transaction::whereDate('created_at', $income)->sum('total_barang_hpp'));
    	        }

    	        return response()->json([
    	        	'incomes' => $incomes,
    	        	'total' => $total
    	        ]);
        	}elseif($id == 'bulan'){
        		if($arr_ammount > 30){
    	        	for ($i = 0; $i < 30; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }elseif($arr_ammount > 0){
    	        	for ($i = 0; $i < $arr_ammount; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }
    	        $incomes = array_reverse($incomes_data);
    	        $total = array();
    	        foreach ($incomes as $no => $income) {
    	        	array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang')-Transaction::whereDate('created_at', $income)->sum('total_barang_hpp'));
    	        }
    	        return response()->json([
    	        	'incomes' => $incomes,
    	        	'total' => $total
    	        ]);
        	}elseif($id == 'tahun'){
        		if($arr_ammount > 365){
    	        	for ($i = 0; $i < 365; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }elseif($arr_ammount > 0){
    	        	for ($i = 0; $i < $arr_ammount; $i++) {
    	        		array_push($incomes_data, $dates[$i]);
    	        	}
    	        }
    	        $incomes = array_reverse($incomes_data);
    	        $total = array();
    	        foreach ($incomes as $no => $income) {
    	        	array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang')-Transaction::whereDate('created_at', $income)->sum('total_barang_hpp'));
    	        }

    	        return response()->json([
    	        	'incomes' => $incomes,
    	        	'total' => $total
    	        ]);
        	}
        }else{
            return back();
        }
    }

    // Filter Chart Transaction
    public function totTransactionLaba($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_laporan == 1){
        	$supplies = Transaction::all();
            $array = array();
            foreach ($supplies as $no => $supply) {
                array_push($array, $id);
            }
            $dates = array_unique($array);
            rsort($dates);
            $arr_ammount = count($dates);
            $incomes_data = array();
            if($arr_ammount > 1){
                for ($i = 0; $i < 1; $i++) {
                    array_push($incomes_data, $dates[$i]);
                }
            }elseif($arr_ammount > 0){
                for ($i = 0; $i < $arr_ammount; $i++) {
                    array_push($incomes_data, $dates[$i]);
                }
            }
            $incomes = array_reverse($incomes_data);
            $total = array();
            foreach ($incomes as $no => $income) {
                array_push($total, Transaction::whereDate('created_at', $income)->sum('total_barang')-Transaction::whereDate('created_at', $income)->sum('total_barang_hpp'));
            }

            return response()->json([
                'incomes' => $incomes,
                'total' => $total
            ]);
        }else{
            return back();
        }
    }

    // Detail Report Worker
    public function detailWorker($section, $id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if ($check_access->kelola_laporan != 1){
            return back();
        }
        $worker = User::find($id);
        $supplies = Supply::select('supplies.*')
        ->where('id_pemasok', $id)
        ->orderBy('created_at', 'desc')
        ->paginate(20);

        $transactions = Transaction::select('transactions.*')
        ->where('id_kasir', $id)
        ->groupBy('kode_transaksi')
        ->orderBy('created_at', 'desc')
        ->paginate(20);

        $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
        ->where('id_pemasok', $id)
        ->get();
        $array_3 = array();
        foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
            array_push($array_3, $supply_bahan_bakus[$no]->created_at->toDateString());
        }
        $dates_3 = array_unique($array_3);
        rsort($dates_3);

        return view('report.detail_report_worker', compact('worker', 'supplies', 'transactions', 'dates_3', 'section', 'id'));
    }

    // Export Transaction Report
    public function exportTransaction(Request $req)
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)
            ->first();

        if($check_access->kelola_laporan != 1){
            return back();
        }

        $jenis_laporan = $req->jns_laporan;
        $current_time = Carbon::now()->isoFormat('Y-MM-DD') . ' 23:59:59';
        if($jenis_laporan == 'period' && $req->period == 'hari') {
            $last_time = Carbon::now()->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($jenis_laporan == 'period' && $req->period == 'minggu') {
            $last_time = Carbon::now()->subWeeks($req->time)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($jenis_laporan == 'period' && $req->period == 'bulan') {
            $last_time = Carbon::now()->subMonths($req->time)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($jenis_laporan == 'period' && $req->period == 'tahun') {
            $last_time = Carbon::now()->subYears($req->time)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }

        if($jenis_laporan == 'manual') {
            $start_date = $req->tgl_awal_export;
            $end_date = $req->tgl_akhir_export;
            $start_date2 = $start_date[6] . $start_date[7] . $start_date[8] . $start_date[9] . '-' . $start_date[3] . $start_date[4] . '-' . $start_date[0] . $start_date[1] . ' 00:00:00';
            $end_date2 = $end_date[6] . $end_date[7] . $end_date[8] . $end_date[9] . '-' . $end_date[3] . $end_date[4] . '-' . $end_date[0] . $end_date[1] . ' 23:59:59';
            $tgl_awal = $start_date2;
            $tgl_akhir = $end_date2;
        }

        $transactions = DB::table('transactions')
            ->select('kode_transaksi', 'created_at', 'total', 'diskon', 'pelanggan', 'po', 'kasir', 'add_val', 'value_add_val')
            ->whereBetween('created_at', [$tgl_awal, $tgl_akhir])
            ->groupBy('kode_transaksi')
            ->orderBy('created_at', 'desc')
            ->get();

        $market = Market::first();

        $income = $transactions->sum('total');
        $incomepo = $transactions->where('po', 1)->sum('total');
        $incomecash = $transactions->where('po', 2)->sum('total');
        $incometransfer = $transactions->where('po', 3)->sum('total');

        if($req->filetype === 'pdf') {
            $pdf = PDF::loadview('report.export_report_transaction', compact('tgl_awal', 'tgl_akhir', 'market', 'transactions', 'income', 'incomepo', 'incomecash', 'incometransfer'));
            return $pdf->stream();
        } else {
            return Excel::download(new TransactionExport($tgl_awal, $tgl_akhir, $market, $transactions, $income, $incomepo, $incomecash, $incometransfer), 'transaction'. time() .'.xlsx');
        }
    }

    // Export Transaction Report
    public function exportTransactionIncome(Request $req)
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        
        if($check_access->kelola_laporan != 1){
            return back();
        }

        $jenis_laporan = $req->jns_laporan;
        $current_time = Carbon::now()->isoFormat('Y-MM-DD') . ' 23:59:59';
        if($jenis_laporan == 'period' && $req->period == 'hari') {
            $last_time = Carbon::now()->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($jenis_laporan == 'period' && $req->period == 'minggu') {
            $last_time = Carbon::now()->subWeeks($req->time)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($jenis_laporan == 'period' && $req->period == 'bulan') {
            $last_time = Carbon::now()->subMonths($req->time)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($jenis_laporan == 'period' && $req->period == 'tahun') {
            $last_time = Carbon::now()->subYears($req->time)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }

        if($jenis_laporan == 'manual') {
            $start_date = $req->tgl_awal_export;
            $end_date = $req->tgl_akhir_export;
            $start_date2 = $start_date[6] . $start_date[7] . $start_date[8] . $start_date[9] . '-' . $start_date[3] . $start_date[4] . '-' . $start_date[0] . $start_date[1] . ' 00:00:00';
            $end_date2 = $end_date[6] . $end_date[7] . $end_date[8] . $end_date[9] . '-' . $end_date[3] . $end_date[4] . '-' . $end_date[0] . $end_date[1] . ' 23:59:59';
            $tgl_awal = $start_date2;
            $tgl_akhir = $end_date2;
        }

        $transactions = DB::table('transactions')
            ->select('kode_transaksi', 'created_at', 'total', 'diskon', 'pelanggan', 'po', 'kasir', 'add_val', 'value_add_val', 'total_hpp')
            ->whereBetween('created_at', [$tgl_awal, $tgl_akhir])
            ->groupBy('kode_transaksi')
            ->orderBy('created_at', 'desc')
            ->get();

        $market = Market::first();

        $income = $transactions->sum('total');
        $incomepo = $transactions->where('po', 1)->sum('total');
        $incomecash = $transactions->where('po', 2)->sum('total');
        $incometransfer = $transactions->where('po', 3)->sum('total');
        if($req->filetype === 'pdf') {
            $pdf = PDF::loadview('report.export_report_income', compact('dates', 'tgl_awal', 'tgl_akhir', 'market'));
            return $pdf->stream();
        } else {
            return Excel::download(new IncomeExport($tgl_awal, $tgl_akhir, $market, $transactions, $income, $incomepo, $incomecash, $incometransfer), 'income'. time() .'.xlsx');
        }
    }

    // Export Worker Report
    public function exportWorker(Request $req, $id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_laporan == 1){
            $jml_laporan = count($req->laporan);

            $jenis_laporan = $req->jns_laporan;
            $current_time = Carbon::now()->isoFormat('Y-MM-DD') . ' 23:59:59';
            if($jenis_laporan == 'period'){
                if($req->period == 'hari'){
                    $last_time = Carbon::now()->isoFormat('Y-MM-DD') . ' 00:00:00';
                    if(count($req->laporan) == 2){
                        $transactions = Transaction::select('transactions.*')
                        ->where('id_kasir', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($transactions as $no => $transaction) {
                            array_push($array, $transactions[$no]->created_at->toDateString());
                        }
                        $transaksi = array_unique($array);
                        rsort($transaksi);
                        $supplies = Supply::select('supplies.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($supplies as $no => $supply) {
                            array_push($array, $supplies[$no]->created_at->toDateString());
                        }
                        $pasok = array_unique($array);
                        rsort($pasok);
                        $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $arraybb = array();
                        foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                            array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                        }
                        $pasokbb = array_unique($arraybb);
                        rsort($pasokbb);
                    }elseif($req->laporan[0] == 'pasok'){
                        $transaksi = '';
                        $supplies = Supply::select('supplies.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($supplies as $no => $supply) {
                            array_push($array, $supplies[$no]->created_at->toDateString());
                        }
                        $pasok = array_unique($array);
                        rsort($pasok);
                        $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $arraybb = array();
                        foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                            array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                        }
                        $pasokbb = array_unique($arraybb);
                        rsort($pasokbb);
                    }elseif($req->laporan[0] == 'transaksi'){
                        $transactions = Transaction::select('transactions.*')
                        ->where('id_kasir', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($transactions as $no => $transaction) {
                            array_push($array, $transactions[$no]->created_at->toDateString());
                        }
                        $transaksi = array_unique($array);
                        rsort($transaksi);
                        $pasok = '';
                        $pasokbb = '';
                    }
                    $tgl_awal = $last_time;
                    $tgl_akhir = $current_time;
                }elseif($req->period == 'minggu'){
                    $last_time = Carbon::now()->subWeeks($req->time)->isoFormat('Y-MM-DD') . ' 00:00:00';
                    if(count($req->laporan) == 2){
                        $transactions = Transaction::select('transactions.*')
                        ->where('id_kasir', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($transactions as $no => $transaction) {
                            array_push($array, $transactions[$no]->created_at->toDateString());
                        }
                        $transaksi = array_unique($array);
                        rsort($transaksi);
                        $supplies = Supply::select('supplies.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($supplies as $no => $supply) {
                            array_push($array, $supplies[$no]->created_at->toDateString());
                        }
                        $pasok = array_unique($array);
                        rsort($pasok);
                        $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $arraybb = array();
                        foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                            array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                        }
                        $pasokbb = array_unique($arraybb);
                        rsort($pasokbb);
                    }elseif($req->laporan[0] == 'pasok'){
                        $transaksi = '';
                        $supplies = Supply::select('supplies.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($supplies as $no => $supply) {
                            array_push($array, $supplies[$no]->created_at->toDateString());
                        }
                        $pasok = array_unique($array);
                        rsort($pasok);

                        $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $arraybb = array();
                        foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                            array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                        }
                        $pasokbb = array_unique($arraybb);
                        rsort($pasokbb);
                    }elseif($req->laporan[0] == 'transaksi'){
                        $transactions = Transaction::select('transactions.*')
                        ->where('id_kasir', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($transactions as $no => $transaction) {
                            array_push($array, $transactions[$no]->created_at->toDateString());
                        }
                        $transaksi = array_unique($array);
                        rsort($transaksi);
                        $pasok = '';
                        $pasokbb = '';
                    }
                    $tgl_awal = $last_time;
                    $tgl_akhir = $current_time;
                }elseif($req->period == 'bulan'){
                    $last_time = Carbon::now()->subMonths($req->time)->isoFormat('Y-MM-DD') . ' 00:00:00';
                    if(count($req->laporan) == 2){
                        $transactions = Transaction::select('transactions.*')
                        ->where('id_kasir', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($transactions as $no => $transaction) {
                            array_push($array, $transactions[$no]->created_at->toDateString());
                        }
                        $transaksi = array_unique($array);
                        rsort($transaksi);
                        $supplies = Supply::select('supplies.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($supplies as $no => $supply) {
                            array_push($array, $supplies[$no]->created_at->toDateString());
                        }
                        $pasok = array_unique($array);
                        rsort($pasok);

                        $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $arraybb = array();
                        foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                            array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                        }
                        $pasokbb = array_unique($arraybb);
                        rsort($pasokbb);
                    }elseif($req->laporan[0] == 'pasok'){
                        $transaksi = '';
                        $supplies = Supply::select('supplies.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($supplies as $no => $supply) {
                            array_push($array, $supplies[$no]->created_at->toDateString());
                        }
                        $pasok = array_unique($array);
                        rsort($pasok);
                        $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $arraybb = array();
                        foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                            array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                        }
                        $pasokbb = array_unique($arraybb);
                        rsort($pasokbb);
                    }elseif($req->laporan[0] == 'transaksi'){
                        $transactions = Transaction::select('transactions.*')
                        ->where('id_kasir', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($transactions as $no => $transaction) {
                            array_push($array, $transactions[$no]->created_at->toDateString());
                        }
                        $transaksi = array_unique($array);
                        rsort($transaksi);
                        $pasok = '';
                        $pasokbb = '';
                    }
                    $tgl_awal = $last_time;
                    $tgl_akhir = $current_time;
                }elseif($req->period == 'tahun'){
                    $last_time = Carbon::now()->subYears($req->time)->isoFormat('Y-MM-DD') . ' 00:00:00';
                    if(count($req->laporan) == 2){
                        $transactions = Transaction::select('transactions.*')
                        ->where('id_kasir', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($transactions as $no => $transaction) {
                            array_push($array, $transactions[$no]->created_at->toDateString());
                        }
                        $transaksi = array_unique($array);
                        rsort($transaksi);
                        $supplies = Supply::select('supplies.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($supplies as $no => $supply) {
                            array_push($array, $supplies[$no]->created_at->toDateString());
                        }
                        $pasok = array_unique($array);
                        rsort($pasok);

                        $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $arraybb = array();
                        foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                            array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                        }
                        $pasokbb = array_unique($arraybb);
                        rsort($pasokbb);
                    }elseif($req->laporan[0] == 'pasok'){
                        $transaksi = '';
                        $supplies = Supply::select('supplies.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($supplies as $no => $supply) {
                            array_push($array, $supplies[$no]->created_at->toDateString());
                        }
                        $pasok = array_unique($array);
                        rsort($pasok);

                        $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                        ->where('id_pemasok', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $arraybb = array();
                        foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                            array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                        }
                        $pasokbb = array_unique($arraybb);
                        rsort($pasokbb);
                    }elseif($req->laporan[0] == 'transaksi'){
                        $transactions = Transaction::select('transactions.*')
                        ->where('id_kasir', $id)
                        ->whereBetween('created_at', array($last_time, $current_time))
                        ->get();
                        $array = array();
                        foreach ($transactions as $no => $transaction) {
                            array_push($array, $transactions[$no]->created_at->toDateString());
                        }
                        $transaksi = array_unique($array);
                        rsort($transaksi);
                        $pasok = '';
                        $pasokbb = '';
                    }
                    $tgl_awal = $last_time;
                    $tgl_akhir = $current_time;
                }
            }else{
                $start_date = $req->tgl_awal_export;
                $end_date = $req->tgl_akhir_export;
                $start_date2 = $start_date[6].$start_date[7].$start_date[8].$start_date[9].'-'.$start_date[3].$start_date[4].'-'.$start_date[0].$start_date[1].' 00:00:00';
                $end_date2 = $end_date[6].$end_date[7].$end_date[8].$end_date[9].'-'.$end_date[3].$end_date[4].'-'.$end_date[0].$end_date[1].' 23:59:59';
                if(count($req->laporan) == 2){
                    $transactions = Transaction::select('transactions.*')
                    ->where('id_kasir', $id)
                    ->whereBetween('created_at', array($start_date2, $end_date2))
                    ->get();
                    $array = array();
                    foreach ($transactions as $no => $transaction) {
                        array_push($array, $transactions[$no]->created_at->toDateString());
                    }
                    $transaksi = array_unique($array);
                    rsort($transaksi);
                    $supplies = Supply::select('supplies.*')
                    ->where('id_pemasok', $id)
                    ->whereBetween('created_at', array($start_date2, $end_date2))
                    ->get();
                    $array = array();
                    foreach ($supplies as $no => $supply) {
                        array_push($array, $supplies[$no]->created_at->toDateString());
                    }
                    $pasok = array_unique($array);
                    rsort($pasok);

                    $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                    ->where('id_pemasok', $id)
                    ->whereBetween('created_at', array($start_date2, $end_date2))
                    ->get();
                    $arraybb = array();
                    foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                        array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                    }
                    $pasokbb = array_unique($arraybb);
                    rsort($pasokbb);
                }elseif($req->laporan[0] == 'pasok'){
                    $transaksi = '';
                    $supplies = Supply::select('supplies.*')
                    ->where('id_pemasok', $id)
                    ->whereBetween('created_at', array($start_date2, $end_date2))
                    ->get();
                    $array = array();
                    foreach ($supplies as $no => $supply) {
                        array_push($array, $supplies[$no]->created_at->toDateString());
                    }
                    $pasok = array_unique($array);
                    rsort($pasok);

                    $supply_bahan_bakus = SupplyBahanBaku::select('supply_bahan_bakus.*')
                    ->where('id_pemasok', $id)
                    ->whereBetween('created_at', array($start_date2, $end_date2))
                    ->get();
                    $arraybb = array();
                    foreach ($supply_bahan_bakus as $no => $supply_bahan_baku) {
                        array_push($arraybb, $supply_bahan_bakus[$no]->created_at->toDateString());
                    }
                    $pasokbb = array_unique($arraybb);
                    rsort($pasokbb);
                }elseif($req->laporan[0] == 'transaksi'){
                    $transactions = Transaction::select('transactions.*')
                    ->where('id_kasir', $id)
                    ->whereBetween('created_at', array($start_date2, $end_date2))
                    ->get();
                    $array = array();
                    foreach ($transactions as $no => $transaction) {
                        array_push($array, $transactions[$no]->created_at->toDateString());
                    }
                    $transaksi = array_unique($array);
                    rsort($transaksi);
                    $pasok = '';
                    $pasokbb = '';
                }
                $tgl_awal = $start_date2;
                $tgl_akhir = $end_date2;
            }
            $jml_act_pasok = Supply::where('id_pemasok', $id)
            ->count();
            $jml_act_trans = Transaction::where('id_kasir', $id)
            ->count();
            $market = Market::first();

            $pdf = PDF::loadview('report.export_report_worker', compact('transaksi', 'pasok', 'tgl_awal', 'tgl_akhir', 'id', 'jml_act_pasok', 'jml_act_trans', 'market', 'pasokbb'));
            return $pdf->stream();
        }else{
            return back();
        }
    }

    public function reportSales()
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)->first();

        if ($check_access->kelola_laporan != 1) {
            return back();
        }

        if(Transaction::first()) {
            $start_date = Transaction::oldest()->first()->created_at;
        } else {
            $start_date = Carbon::now();
        }
        $end_date = Carbon::now()->endOfDay();

        $data_customers = DB::table('transactions')
            ->select('id_pelanggan', 'pelanggan', DB::raw('COUNT(DISTINCT kode_transaksi) as total_transaksi'), DB::raw('sum(total_barang + value_add_val) as total_omzet'))
            ->join('distributors', 'id_pelanggan', '=', 'distributors.id')
            ->groupBy('id_pelanggan')
            ->orderBy('total_omzet', 'desc');

        $all_customers = $data_customers->get();

        $top_customers =  $data_customers->limit(10)->get();

        $income = DB::table('transactions')
            ->select('total')
            ->where('id_pelanggan', $top_customers->first()->id_pelanggan)
            ->groupBy('kode_transaksi')
            ->orderBy('created_at', 'desc')
            ->get()
            ->sum('total');

        return view('report.report_sales', compact('income', 'start_date', 'end_date', 'top_customers', 'all_customers'));
    }

    public function reportSalesFilter(Request $req)
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)->first();

        if ($check_access->kelola_laporan != 1) {
            return back();
        }

        if ($req->tgl_awal) {
            $start_date_temp = $req->tgl_awal;
            $start_date = $start_date_temp[6].$start_date_temp[7].$start_date_temp[8].$start_date_temp[9].'-'.$start_date_temp[3].$start_date_temp[4].'-'.$start_date_temp[0].$start_date_temp[1].' 00:00:00';
        } else {
            if(Transaction::first()) {
                $start_date = Transaction::oldest()->first()->created_at;
            } else {
                $start_date = Carbon::now();
            }
        }

        if ($req->tgl_akhir) {
            $end_date_temp = $req->tgl_akhir;
            $end_date = $end_date_temp[6].$end_date_temp[7].$end_date_temp[8].$end_date_temp[9].'-'.$end_date_temp[3].$end_date_temp[4].'-'.$end_date_temp[0].$end_date_temp[1].' 23:59:59';
        } else {
            $end_date = Carbon::now()->endOfDay();
        }

        $transactions = DB::table('transactions')
            ->select('kode_transaksi', 'created_at', 'total', 'diskon', 'pelanggan', 'po', 'kasir', 'add_val', 'value_add_val')
            ->where('id_pelanggan', $req->idCustomer)
            ->whereBetween('created_at', [$start_date, $end_date])
            ->groupBy('kode_transaksi')
            ->orderBy('created_at', 'desc')
            ->get();
        
        $income = DB::table('transactions')
            ->select('total')
            ->where('id_pelanggan', $req->idCustomer)
            ->whereBetween('created_at', [$start_date, $end_date])
            ->groupBy('kode_transaksi')
            ->orderBy('created_at', 'desc')
            ->get()
            ->sum('total');

        return view('report.report_sales_filter', compact('transactions', 'income', 'start_date', 'end_date'));
    }

    public function topCustomersTable($period)
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        
        if($check_access->kelola_laporan != 1){
            return back();
        }

        $current_time = Carbon::now()->isoFormat('Y-MM-DD') . ' 23:59:59';
        if($period == 'minggu') {
            $last_time = Carbon::now()->subWeeks(1)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($period == 'bulan') {
            $last_time = Carbon::now()->subMonths(1)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($period == 'tahun') {
            $last_time = Carbon::now()->subYears(1)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }

        $top_customers = DB::table('transactions')
            ->select('id_pelanggan', 'pelanggan', DB::raw('COUNT(DISTINCT kode_transaksi) as total_transaksi'), DB::raw('sum(total_barang + value_add_val) as total_omzet'))
            ->join('distributors', 'id_pelanggan', '=', 'distributors.id')
            ->whereBetween('transactions.created_at', [$tgl_awal, $tgl_akhir])
            ->groupBy('id_pelanggan')
            ->orderBy('total_omzet', 'desc')
            ->limit(5)
            ->get();

        return view('report.report_sales_top_table', compact('top_customers'));
    }

    public function reportSalesProduct()
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)->first();

        if ($check_access->kelola_laporan != 1) {
            return back();
        }

        if(Transaction::first()) {
            $start_date = Transaction::oldest()->first()->created_at;
        } else {
            $start_date = Carbon::now();
        }
        $end_date = Carbon::now()->endOfDay();

        $data_products = DB::table('transactions')
            ->select('products.kode_barang', 'products.nama_barang', DB::raw('sum(jumlah) as jumlah_barang_terjual'), DB::raw('sum(total_barang + value_add_val) as total_omzet'))
            ->join('products', 'transactions.kode_barang', '=', 'products.kode_barang')
            ->groupBy('transactions.kode_barang')
            ->orderBy('jumlah_barang_terjual', 'desc');

        $all_products = $data_products->get();

        $income = $data_products
            ->where('transactions.kode_barang', $all_products->first()->kode_barang)
            ->get()
            ->sum('total_omzet');

        return view('report.report_sales_product', compact('income', 'start_date', 'end_date', 'all_products'));
    }

    public function topSalesProductsTable($period)
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        
        if($check_access->kelola_laporan != 1){
            return back();
        }

        $current_time = Carbon::now()->isoFormat('Y-MM-DD') . ' 23:59:59';
        if($period == 'minggu') {
            $last_time = Carbon::now()->subWeeks(1)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($period == 'bulan') {
            $last_time = Carbon::now()->subMonths(1)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }
        if($period == 'tahun') {
            $last_time = Carbon::now()->subYears(1)->isoFormat('Y-MM-DD') . ' 00:00:00';
            $tgl_awal = $last_time;
            $tgl_akhir = $current_time;
        }

        $top_products = DB::table('transactions')
            ->select('products.kode_barang', 'products.nama_barang', DB::raw('sum(jumlah) as jumlah_barang_terjual'), DB::raw('sum(total_barang + value_add_val) as total_omzet'))
            ->join('products', 'transactions.kode_barang', '=', 'products.kode_barang')
            ->whereBetween('transactions.created_at', [$tgl_awal, $tgl_akhir])
            ->groupBy('transactions.kode_barang')
            ->orderBy('jumlah_barang_terjual', 'desc')
            ->limit(5)
            ->get();

        return view('report.report_sales_product_table', compact('top_products'));
    }

    public function salesProduct(Request $req)
    {
        $id_account = auth()->id();
        $check_access = Acces::where('user', $id_account)->first();

        if ($check_access->kelola_laporan != 1) {
            return back();
        }

        $oldest_transactions_by_id = DB::table('transactions')
            ->select('created_at')
            ->where('kode_barang', $req->id)
            ->oldest()
            ->first();

        if ($req->tgl_awal) {
            $start_date_temp = $req->tgl_awal;
            $start_date = $start_date_temp[6].$start_date_temp[7].$start_date_temp[8].$start_date_temp[9].'-'.$start_date_temp[3].$start_date_temp[4].'-'.$start_date_temp[0].$start_date_temp[1].' 00:00:00';
        } else {
            if($oldest_transactions_by_id) {
                $start_date = $oldest_transactions_by_id->created_at;
            } else {
                $start_date = Carbon::now();
            }
        }

        if ($req->tgl_akhir) {
            $end_date_temp = $req->tgl_akhir;
            $end_date = $end_date_temp[6].$end_date_temp[7].$end_date_temp[8].$end_date_temp[9].'-'.$end_date_temp[3].$end_date_temp[4].'-'.$end_date_temp[0].$end_date_temp[1].' 23:59:59';
        } else {
            $end_date = Carbon::now()->endOfDay();
        }

        $transactions = DB::table('transactions')
            ->select('products.kode_barang', 'products.nama_barang', DB::raw('sum(jumlah) as jumlah_barang_terjual'), DB::raw('sum(total_barang + value_add_val) as total_omzet'))
            ->where('transactions.kode_barang', $req->id)
            ->whereBetween('transactions.created_at', [$start_date, $end_date])
            ->join('products', 'transactions.kode_barang', '=', 'products.kode_barang')
            ->groupBy('transactions.kode_barang')
            ->orderBy('jumlah_barang_terjual', 'desc')
            ->get();
            
        $income = $transactions->sum('total_omzet');
        $qty = $transactions->sum('jumlah_barang_terjual');

        return response()->json([
            'total_sales' => "Rp. ".number_format($income, 0, ',', '.'),
            'qty' => $qty,
            'datarange' => str_replace(',', '', date('d M, Y', strtotime($start_date)))." sampai ".str_replace(',', '', date('d M, Y', strtotime($end_date))),
        ]);
    }
}
