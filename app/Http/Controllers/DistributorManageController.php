<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Distributor;
use App\Acces;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DistributorManageController extends Controller
{

    // Show View Distributor
    public function viewDistributor()
    {
        $id_distributor = Auth::id();
        $check_access = Acces::where('user', $id_distributor)
        ->first();
        if($check_access->kelola_akun == 1){
            $users = Distributor::all();

            return view('manage_distributor.distributor', compact('users'));    
        }else{
            return back();
        }
    }

    // Show View New Distributor
    public function viewNewDistributor()
    {
        $id_distributor = Auth::id();
        $check_access = Acces::where('user', $id_distributor)
        ->first();
        if($check_access->kelola_akun == 1){
        	return view('manage_distributor.new_distributor');
        }else{
            return back();
        }
    }

    // Filter Distributor Table
    public function filterTable($id)
    {
        $id_distributor = Auth::id();
        $check_access = Acces::where('user', $id_distributor)
        ->first();
        if($check_access->kelola_akun == 1){
        	$users = Distributor::orderBy($id, 'asc')
            ->get();

        	return view('manage_distributor.filter_table.table_view', compact('users'));
        }else{
            return back();
        }
    }

    // Create New Distributor
    public function createDistributor(Request $req)
    {
        $id_distributor = Auth::id();
        $check_access = Acces::where('user', $id_distributor)
        ->first();
        if($check_access->kelola_akun == 1){

        	$check_email = Distributor::all()
        	->where('email', $req->email)
        	->count();

        	if($check_email == 0)
        	{
        		$users = new Distributor;
    	    	$users->nama = $req->nama;
    	    	$users->nama_toko = $req->nama_toko;
                $users->email = $req->email;
                $users->telp = $req->telp;
                $users->alamat = $req->alamat;
                $users->no_kontrak = 'SIMA'.$req->tanggal_join[6].$req->tanggal_join[7].$req->tanggal_join[8].$req->tanggal_join[9].$req->tanggal_join[3].$req->tanggal_join[4].$req->tanggal_join[0].$req->tanggal_join[1];
    	    	$users->save();

    	    	Session::flash('create_success', 'Customer baru berhasil dibuat');

    	    	return redirect('/distributor');
        	}
        	else if($check_email != 0)
        	{
        		Session::flash('email_error', 'Email telah digunakan, silakan coba lagi');

        		return back();
        	}
        }else{
            return back();
        }
    }

    // Create New Distributor
    public function createDistributorGet(Request $req)
    {
        $id_distributor = Auth::id();
        $check_access = Acces::where('user', $id_distributor)
        ->first();
        if($check_access->kelola_akun == 1){

        	$check_email = Distributor::all()
        	->where('email', $req->email)
        	->count();

        	if($check_email == 0)
        	{
        		$users = new Distributor;
    	    	$users->nama = $req->nama;
    	    	$users->nama_toko = $req->nama_toko;
                $users->email = $req->email;
                $users->telp = $req->telp;
                $users->alamat = $req->alamat;
                $users->no_kontrak = 'SIMA'.$req->tanggal_join[6].$req->tanggal_join[7].$req->tanggal_join[8].$req->tanggal_join[9].$req->tanggal_join[3].$req->tanggal_join[4].$req->tanggal_join[0].$req->tanggal_join[1];
    	    	$users->save();

    	    	Session::flash('create_success', 'Customer baru berhasil dibuat');

                // return response()->json([
                //     'messages' => 'success'
                // ]);
                return redirect('/transaction');
        	}
        	else if($check_email != 0)
        	{
        		Session::flash('email_error', 'Email telah digunakan, silakan coba lagi');

        		// return response()->json([
                //     'messages' => 'error'
                // ]);
                return redirect('/transaction');
        	}
        }else{
            // return response()->json([
            //     'messages' => 'error'
            // ]);
            return redirect('/transaction');
        }
    }

    // Edit Distributor
    public function editDistributor($id)
    {
        $id_distributor = Auth::id();
        $check_access = Acces::where('user', $id_distributor)
        ->first();
        if($check_access->kelola_akun == 1){
            $user = Distributor::find($id);

            return response()->json(['user' => $user]);
        }else{
            return back();
        }
    }

    // Update Distributor
    public function updateDistributor(Request $req)
    {
        $id_distributor = Auth::id();
        $check_access = Acces::where('user', $id_distributor)
        ->first();
        if($check_access->kelola_akun == 1){
            $user_distributor = Distributor::find($req->id);
            $check_email = Distributor::all()
            ->where('email', $req->email)
            ->count();

            if($check_email == 0 || $user_distributor->email == $req->email)
            {
                $user = Distributor::find($req->id);
                $user->nama = $req->nama;
                $user->nama_toko = $req->nama_toko;
                $user->email = $req->email;
                $user->telp = $req->telp;
                $user->alamat = $req->alamat;
                $user->no_kontrak = 'SIMA'.$req->tanggal_join[6].$req->tanggal_join[7].$req->tanggal_join[8].$req->tanggal_join[9].$req->tanggal_join[3].$req->tanggal_join[4].$req->tanggal_join[0].$req->tanggal_join[1];
                $user->save();

                Session::flash('update_success', 'Akun berhasil diubah');

                return redirect('/distributor');
            }
            else if($check_email != 0 && $user_distributor->email != $req->email)
            {
                Session::flash('email_error', 'Email telah digunakan, silakan coba lagi');

                return back();
            }
        }else{
            return back();
        }
    }

    // Delete Distributor
    public function deleteDistributor($id)
    {
        $id_distributor = Auth::id();
        $check_access = Acces::where('user', $id_distributor)
        ->first();
        if($check_access->kelola_akun == 1){
            Distributor::destroy($id);

            Session::flash('delete_success', 'Akun berhasil dihapus');

            return back();
        }else{
            return back();
        }
    }
}
