<?php

namespace App\Http\Controllers;

use PDF;
use Auth;
use Session;
use App\Acces;
use App\BahanBaku;
use App\Imports\BahanBakuImport;
use App\Exports\BahanBakuExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class BahanBakuManageController extends Controller
{
    // Show View Product
    public function countBahanBaku()
    {
        $bahanbakus = BahanBaku::all()
        ->sortBy('kode_bahan');
        $count = 0;
        foreach($bahanbakus as $bahanbaku){
            $count += ($bahanbaku->harga * $bahanbaku->stok);
        }
        return $count;
    }
    // Show View BahanBaku
    public function viewBahanBaku()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
        	$bahan_baku = BahanBaku::all()
            ->sortBy('kode_bahan');

        	return view('manage_bahan_baku.product', compact('bahan_baku'));
        }else{
            return back();
        }
    }

    // Show View New BahanBaku
    public function viewNewBahanBaku()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){

        	return view('manage_bahan_baku.new_product');
        }else{
            return back();
        }
    }

    // Filter BahanBaku Table
    public function filterTable($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $bahan_baku = BahanBaku::orderBy($id, 'asc')
            ->get();

            return view('manage_bahan_baku.filter_table.table_view', compact('bahan_baku'));
        }else{
            return back();
        }
    }

    // Create New BahanBaku
    public function createBahanBaku(Request $req)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
        	$check_product = BahanBaku::where('kode_bahan', $req->kode_bahan)
        	->count();

        	if($check_product == 0)
        	{
        		$product = new BahanBaku;
    	    	$product->kode_bahan = $req->kode_bahan;
    	    	$product->jenis_bahan = $req->jenis_bahan;
    	    	$product->nama_bahan = $req->nama_bahan;
    	        $product->berat_bahan = $req->satuan_berat;
                $product->stok = $req->stok;
    	    	$product->harga = $req->harga;
    	    	$product->save();

    	    	Session::flash('create_success', 'Barang baru berhasil ditambahkan');

    		    return redirect('/bahan-baku');
        	}else{
        		Session::flash('create_failed', 'Kode bahan telah digunakan');

    		    return back();
        	}
    	}else{
            return back();
        }
    }

    // Import New BahanBaku
    public function importBahanBaku(Request $req)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
        	try{
        		$file = $req->file('excel_file');
    			$nama_file = rand().$file->getClientOriginalName();
    			$file->move('excel_file', $nama_file);
    			Excel::import(new BahanBakuImport, public_path('/excel_file/'.$nama_file));

    			Session::flash('import_success', 'Data bahan berhasil diimport');
        	}catch(\Exception $ex){
        		Session::flash('import_failed', 'Cek kembali terdapat data kosong atau kode bahan yang telah tersedia');

        		return back();
        	}

        	return redirect('/bahan-baku');
        }else{
            return back();
        }
    }

    // Edit BahanBaku
    public function editBahanBaku($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $product = BahanBaku::find($id);

            return response()->json(['product' => $product]);
        }else{
            return back();
        }
    }

    // Update BahanBaku
    public function updateBahanBaku(Request $req)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $check_product = BahanBaku::where('kode_bahan', $req->kode_bahan)
            ->count();
            $product_data = BahanBaku::find($req->id);
            if($check_product == 0 || $product_data->kode_bahan == $req->kode_bahan)
            {
                $product = BahanBaku::find($req->id);
                $kode_bahan = $product->kode_bahan;
                $product->kode_bahan = $req->kode_bahan;
                $product->jenis_bahan = $req->jenis_bahan;
                $product->nama_bahan = $req->nama_bahan;
                $product->berat_bahan = $req->satuan_berat;
                $product->stok = $req->stok;
                $product->harga = $req->harga;
                if($req->stok <= 0)
                {
                    $product->keterangan = "Habis";
                }else{
                    $product->keterangan = "Tersedia";
                }
                $product->save();
                Session::flash('update_success', 'Data bahan berhasil diubah');

                return redirect('/bahan-baku');
            }else{
                Session::flash('update_failed', 'Kode bahan telah digunakan');

                return back();
            }
        }else{
            return back();
        }
    }

    // Delete BahanBaku
    public function deleteBahanBaku($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            BahanBaku::destroy($id);

            Session::flash('delete_success', 'Barang berhasil dihapus');

            return back();
        }else{
            return back();
        }
    }

    public function exportBahanBakuStockExport()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $bahan_baku = BahanBaku::all()
            ->sortBy('kode_bahan');
            $count = 0;
            foreach($bahan_baku as $product){
                $count += ($product->harga * $product->stok);
            }
            $pdf = PDF::loadview('report.export_report_bahan_baku', compact('bahan_baku', 'count'));
            return $pdf->stream();
        }else{
            return back();
        }
    }

    public function exportBahanBakuStockExportExcel()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $bahan_baku = BahanBaku::all()
            ->sortBy('kode_bahan');
            $count = 0;
            foreach($bahan_baku as $product){
                $count += ($product->harga * $product->stok);
            }
            return Excel::download(new BahanBakuExport($bahan_baku, $count), 'bahanbaku.xlsx');
        }else{
            return back();
        }
    }
}
