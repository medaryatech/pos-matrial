<?php

namespace App\Http\Controllers\Uom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BrandMaster;
use App\Models\ItemBrand;
use Session;

class BrandMasterController extends Controller
{
    public function __construct()
    {
        $this->brandMaster = new BrandMaster();
        $this->itemBrand = new ItemBrand();
    }

    public function index()
    {
        $data = $this->brandMaster->get();

        return view('uom_conversion.indexBrand',[
            'data' => $data
        ]);
    }

    public function store(Request $req)
    {
        $exist = $this->brandMaster->where('kode_brand', $req->kode_brand)->count();

        if ( $exist > 0 ) {
            Session::flash('error', 'Kode Brand sudah ada');
            return back();
        }

        $this->brandMaster->create($req->all());

        Session::flash('success', 'Tambah brand baru berhasil');
        return back();
    }

    public function jsonGetData($id)
    {
        return response()->json($this->brandMaster->find($id), 200);
    }

    public function update(Request $req, $id)
    {
        $data = $this->brandMaster->find($id);

        if ( $data == NULL ) {
            Session::flash('error', 'Tidak di temukan data brand, silahkan hubungi admin');
            return back();
        }

        $data->update([
            'kode_brand' => $req->kode_brand,
            'nama' => $req->nama
        ]);

        Session::flash('success', 'Update brand berhasil');
        return back();
    }

    public function delete($id)
    {
        $data = $this->brandMaster->find($id);

        if ( $data == NULL ) {
            Session::flash('error', 'Tidak di temukan data brand, silahkan hubungi admin');
            return back();
        }

        $exisInItemData = $this->itemBrand->where('brand_id', $id)->count();
        if ( $exisInItemData > 0 ) {
            Session::flash('error', 'Terdapat relasi data dari item barang ke merek barang !');
            return back();
        }

        $data->delete();

        Session::flash('success', 'Hapus brand berhasil');
        return back();
    }
}
