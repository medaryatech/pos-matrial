<?php

namespace App\Http\Controllers\Uom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UomConversionMaster;
use App\Models\BrandMaster;
use App\Models\ItemBrand;
use Session;
use Validator;

class UomConversionController extends Controller
{
    public function __construct()
    {
        $this->uomMaster = new UomConversionMaster();
        $this->brandMaster = new BrandMaster();
        $this->itemBrand = new ItemBrand();
    }

    public function index()
    {
        $data = $this->uomMaster->get();
        $brand = $this->brandMaster->get();

        return view('uom_conversion.index',[
            'data' => $data,
            'brand' => $brand
        ]);
    }

    public function getItemBrand($id)
    {
        $data = $this->itemBrand->where('brand_id', $id)->get();
        return response()->json($data, 200);
    }

    public function store(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'item_id' => 'required'
        ],[
            'item_id.required' => 'Nama Item tidak boleh kosong'
        ]);

        if ($validator->fails()) {
            Session::flash('error', $validator->errors()->first());
            return back();
        }

        foreach ($req->item_id as $key => $val) {
            $data = [
                'brand_id' => $req->brand_id,
                'item_id' => $val,
                // 'hitung_satuan' => $req->hitung_satuan,
                // 'tipe_volume' => $req->tipe_volume,
                'tipe_uom_sell' => $req->tipe_uom_sell,
                'harga_jual' => getOnlyNumber($req->harga_jual),
                'harga_beli' => getOnlyNumber($req->harga_beli),
                'quantity' => $req->quantity,
                'nilai_hpp' => getOnlyNumber($req->nilai_hpp),
            ];

            $this->uomMaster->updateOrCreate([
                'brand_id' => $data['brand_id'],
                'item_id' => $data['item_id'],
                // 'hitung_satuan' => $data['hitung_satuan']
            ], $data);
        }

        Session::flash('success', 'Tambah data berhasil');
        return back();
    }

    public function jsonGetData($id)
    {
        $data = $this->uomMaster->find($id);
        return response()->json([
            'data' => $data,
            'itemBrand' => $this->itemBrand->where('brand_id', $data->brand_id)->get()
        ], 200);
    }

    public function update(Request $req, $id)
    {
        $data = $this->uomMaster->find($id)->update([
            'brand_id' => $req->brand_id,
            'item_id' => $req->item_id[0],
            // 'hitung_satuan' => $req->hitung_satuan,
            // 'tipe_volume' => $req->tipe_volume,
            'tipe_uom_sell' => $req->tipe_uom_sell,
            'harga_jual' => getOnlyNumber($req->harga_jual),
            'harga_beli' => getOnlyNumber($req->harga_beli),
            'quantity' => $req->quantity,
            'nilai_hpp' => getOnlyNumber($req->nilai_hpp)
        ]);

        Session::flash('success', 'Update data berhasil');
        return back();
    }

    public function delete($id)
    {
        $data = $this->uomMaster->find($id);

        if ( $data == NULL ) {
            Session::flash('error', 'Tidak di temukan data, silahkan hubungi admin');
            return back();
        }

        $data->delete();

        Session::flash('success', 'Hapus data berhasil');
        return back();
    }
}
