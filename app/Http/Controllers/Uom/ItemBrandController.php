<?php

namespace App\Http\Controllers\Uom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ItemBrand;
use App\Models\BrandMaster;
use Session;

class ItemBrandController extends Controller
{
    public function __construct()
    {
        $this->itemBrand = new ItemBrand();
        $this->brandMaster = new BrandMaster();
    }

    public function index()
    {
        $data = $this->itemBrand->get();
        $brand = $this->brandMaster->get();

        return view('uom_conversion.indexItemBrand',[
            'data' => $data,
            'brand' => $brand
        ]);
    }

    public function store(Request $req)
    {
        $exist = $this->itemBrand->where('kode_item', $req->kode_item)->count();

        if ( $req->kode_item != NULL && $exist > 0 ) {
            Session::flash('error', 'Kode Item sudah ada');
            return back();
        }

        $this->itemBrand->create($req->all());

        Session::flash('success', 'Tambah item baru berhasil');
        return back();
    }

    public function jsonGetData($id)
    {
        return response()->json($this->itemBrand->find($id), 200);
    }

    public function update(Request $req, $id)
    {
        $data = $this->itemBrand->find($id);

        if ( $data == NULL ) {
            Session::flash('error', 'Tidak di temukan data item, silahkan hubungi admin');
            return back();
        }

        $data->update([
            'brand_id' => $req->brand_id,
            'nama' => $req->nama,
            'kode_item' => $req->kode_item,
            'value_satuan' => $req->value_satuan,
            'type_volume' => $req->type_volume
        ]);

        Session::flash('success', 'Update item berhasil');
        return back();
    }

    public function delete($id)
    {
        $data = $this->itemBrand->find($id);

        if ( $data == NULL ) {
            Session::flash('error', 'Tidak di temukan data item, silahkan hubungi admin');
            return back();
        }

        $data->delete();

        Session::flash('success', 'Hapus item berhasil');
        return back();
    }
}
