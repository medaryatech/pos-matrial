<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Validator;

class CategoryController extends Controller
{
    public function index()
    {
        return view('master_data.category.index',[
            'category' => $this->category->get()
        ]);
    }

    public function store(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'nama' => 'required'
        ],[
            'nama.required' => 'Nama Category tidak boleh kosong'
        ]);

        if ($validator->fails()) {
            Session::flash('error', $validator->errors()->first());
            return back();
        }

        $this->category->create($req->all());

        Session::flash('success', 'Tambah kategori berhasil');
        return back();
    }

    public function getJsonData($id)
    {
        $data = $this->category->find($id);
        return response()->json($data, 200);
    }

    public function update(Request $req, $id)
    {
        $data = $this->category->find($id)->update([
            'nama' => $req->nama,
            'keterangan' => $req->keterangan
        ]);

        Session::flash('success', 'Update kategori berhasil');
        return back();
    }

    public function delete($id)
    {
        $data = $this->category->find($id);

        if ( $data == NULL ) {
            Session::flash('error', 'Tidak di temukan data, silahkan hubungi admin');
            return back();
        }

        $data->delete();

        Session::flash('success', 'Hapus data berhasil');
        return back();
    }
}
