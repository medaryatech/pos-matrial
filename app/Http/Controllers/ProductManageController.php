<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use PDF;
use Auth;
use Session;

use App\Acces;
use App\Supply;
use App\Product;
use App\Transaction;
use App\Supply_system;
use App\Imports\ProductImport;
USE App\Models\Master\Varian;
use App\Exports\StockExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Validator;

class ProductManageController extends Controller
{
    // Show View Product
    public function countProduct()
    {
        $products = Product::all()
        ->sortBy('kode_barang');
        $count = 0;
        foreach($products as $product){
            $count += ($product->harga * $product->stok);
        }
        return $count;
    }

    // Show View Product
    public function viewProduct()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){

            $products = DB::table('products')
            // ->join('product_varian', 'products.kode_barang', '=', 'product_varian.kode_barang')
            ->paginate(20);
            $supply_system = Supply_system::first();
            $category = $this->category->get();
            $varians = Varian::all()
        	->sortBy('nama_varian');

        	return view('manage_product.product', compact('products', 'supply_system', 'category', 'varians'));
        }else{
            return back();
        }
    }

    // Show View New Product
    public function viewNewProduct()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $supply_system = Supply_system::first();
            $category = $this->category->get();

        	return view('manage_product.new_product', compact('supply_system', 'category'));
        }else{
            return back();
        }
    }

    // Filter Product Table
    public function filterTable($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $supply_system = Supply_system::first();
            $products = Product::orderBy($id, 'asc')
            ->get();

            return view('manage_product.filter_table.table_view', compact('products', 'supply_system'));
        }else{
            return back();
        }
    }

    // Create New Product
    public function createProduct(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'file_gambar' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'kode_barang' => 'required'
        ],[
            'file_gambar.image' => 'Tipe File harus berupa gambar jpeg, png, jpg',
            'kode_barang.required' => 'Kode Barang harus di isi'
        ]);

        if ($validator->fails()) {
            Session::flash('create_failed', $validator->errors());
            return redirect()->back();
        }
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
        	$check_product = Product::where('kode_barang', $req->kode_barang)
        	->count();
            $supply_system = Supply_system::first();

        	if($check_product == 0)
        	{
        		$product = new Product;
    	    	$product->kode_barang = $req->kode_barang;
    	    	$product->jenis_barang = $req->jenis_barang;
    	    	$product->nama_barang = $req->nama_barang;
                $product->category_id = $req->kategori_id;
    	    	if($req->berat_barang != '')
    	    	{
    	    		$product->berat_barang = $req->berat_barang . ' ' . $req->satuan_berat;
    	    	}
    	    	if($req->merek != '')
    	    	{
    	    		$product->merek = $req->merek;
    	    	}
                if($supply_system->status == true){
                    $product->stok = $req->stok;
                }else{
                    $product->stok = 1;
                }
                $product->hpp = getOnlyNumber($req->hpp);
    	    	$product->harga = getOnlyNumber($req->harga);
                $product->harga_reseller = 0;
                if ( $req->isSetConversion == 'on' ) {
                    $uom = $this->uom_conversion;
                    $uom->kode_barang = $req->kode_barang;
                    $uom->tipe_satuan = $req->tipe_satuan;
                    $uom->value_uom_sell = $req->value_uom_sell;
                    $uom->save();
                }

                if ( isset($req->minimal_grosir) ) {
                    foreach ($req->minimal_grosir as $key => $val) {
                        $this->harga_grosir->create([
                            'kode_barang' => $req->kode_barang,
                            'minimal_grosir' => $val,
                            'harga_grosir' => $req->harga_grosir[$key]
                        ]);
                    }
                }

                if ( isset($req->sku_varian) ) {
                    foreach ($req->sku_varian as $key => $val) {
                        $this->varian->create([
                            'kode_barang' => $req->kode_barang,
                            'nama_varian' => $req->nama_varian[$key],
                            'conv_varian' => $req->conv_varian[$key],
                            'hpp_varian' => $req->hpp_varian[$key],
                            'sku_varian' => $val,
                            'harga_varian' => $req->harga_varian[$key]
                        ]);
                    }
                }
                
                if ( $req->hasFile('file_gambar') ) {
                    $name = $req->file('file_gambar')->getClientOriginalName();
                    $getName = pathinfo($name, PATHINFO_FILENAME);
                    $ext = $req->file('file_gambar')->getClientOriginalExtension();
                    $filename = $getName.'_'.time().'.'.$ext;
                    $path = $req->file('file_gambar')->storeAs('public/gambar_produk', $filename);
                    $filename = $path;
                } else {
                    $filename = NULL;
                }
                $product->gambar_produk = $filename;
    	    	$product->save();

    	    	Session::flash('create_success', 'Barang baru berhasil ditambahkan');

    		    return redirect('/product');
        	}else{
        		Session::flash('create_failed', 'Kode barang telah digunakan');

    		    return back();
        	}
    	}else{
            return back();
        }
    }

    // Import New Product
    public function importProduct(Request $req)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
        	try{
        		$file = $req->file('excel_file');
    			$nama_file = rand().$file->getClientOriginalName();
    			$file->move('excel_file', $nama_file);
    			Excel::import(new ProductImport, public_path('/excel_file/'.$nama_file));

    			Session::flash('import_success', 'Data barang berhasil diimport');
        	}catch(\Exception $ex){
        		Session::flash('import_failed', 'Cek kembali terdapat data kosong atau kode barang yang telah tersedia');

        		return back();
        	}

        	return redirect('/product');
        }else{
            return back();
        }
    }

    public function filterProduct(Request $req) {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            if($req->search) {
                $products = Product::where('nama_barang', 'LIKE', '%' . $req->search . '%')
                ->orWhere('kode_barang', 'LIKE', '%' . $req->search . '%')
                // ->join('product_varian', 'products.kode_barang', '=', 'product_varian.kode_barang')
                ->paginate(20);
                $products->appends(['search' => $req->search]);
                $supply_system = Supply_system::first();
                $category = $this->category->get();
                $varians = Varian::all()
        	    ->sortBy('nama_varian');

                return view('manage_product.product', compact('products', 'supply_system', 'category', 'varians'));
            } else {
                return redirect('/product');
            }
            
        }else{
            return back();
        }
    }

    // Edit Product
    public function editProduct($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $product = Product::find($id);
            $uom = $this->uom_conversion->select('kode_barang','tipe_satuan', 'value_uom_sell')->where('kode_barang',$product->kode_barang)->first();
            $hargaGrosir = $this->harga_grosir->select('id','kode_barang','minimal_grosir', 'harga_grosir')->where('kode_barang',$product->kode_barang)->get();
            $varian = $this->varian->select('id','kode_barang','nama_varian', 'conv_varian', 'hpp_varian', 'sku_varian', 'harga_varian')->where('kode_barang',$product->kode_barang)->get();

            return response()->json([
                'product' => $product,
                'uom' => $uom,
                'harga_grosir' => ( $hargaGrosir->count() > 0 ) ? $hargaGrosir : NULL,
                'varian' => ( $varian->count() > 0 ) ? $varian : NULL
            ]);
        }else{
            return back();
        }
    }

    // Update Product
    public function updateProduct(Request $req)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $check_product = Product::where('kode_barang', $req->kode_barang)
            ->count();
            $product_data = Product::find($req->id);
            if($check_product == 0 || $product_data->kode_barang == $req->kode_barang)
            {
                $product = Product::find($req->id);
                $kode_barang = $product->kode_barang;
                $product->kode_barang = $req->kode_barang;
                $product->jenis_barang = $req->jenis_barang;
                $product->nama_barang = $req->nama_barang;
                // $product->berat_barang = $req->berat_barang . ' ' . $req->satuan_berat;
                $product->merek = $req->merek;
                $product->stok = $req->stok;
                $product->hpp = getOnlyNumber($req->hpp);
                $product->harga = getOnlyNumber($req->harga);
                $product->harga_reseller = 0;
                $product->category_id = $req->kategori_id;
                if($req->stok <= 0)
                {
                    $product->keterangan = "Habis";
                }else{
                    $product->keterangan = "Tersedia";
                }

                if ( $req->isSetConversion == 'on' ) {
                    $uom = $this->uom_conversion->updateOrCreate([
                        'kode_barang' => $req->kode_barang
                    ],[
                        'tipe_satuan' => $req->tipe_satuan,
                        'value_uom_sell' => $req->value_uom_sell
                    ]);
                } else {
                    $this->uom_conversion->where('kode_barang',$req->kode_barang)->delete();
                }

                if ( $req->status == 1 ) {
                    $dataGr = $this->harga_grosir->where('kode_barang',$req->kode_barang)->get();
                    if ( $dataGr->count() > 0 ) {
                        foreach ($dataGr as $key => $val) {
                            $this->harga_grosir->find($val->id)->delete();
                        }
                        foreach ($req->minimal_grosir as $key => $val) {
                            $this->harga_grosir->create([
                                'kode_barang' => $req->kode_barang,
                                'minimal_grosir' => $val,
                                'harga_grosir' => $req->harga_grosir[$key]
                            ]);
                        }
                    } else {
                        foreach ($req->minimal_grosir as $key => $val) {
                            $this->harga_grosir->create([
                                'kode_barang' => $req->kode_barang,
                                'minimal_grosir' => $val,
                                'harga_grosir' => $req->harga_grosir[$key]
                            ]);
                        }
                    }
                } else {
                    $this->harga_grosir->where('kode_barang', $req->kode_barang)->delete();
                }

                if ( $req->varians == 1 ) {
                    $dataGr_varians = $this->varian->where('kode_barang',$req->kode_barang)->get();
                    if ( $dataGr_varians->count() > 0 ) {
                        foreach ($dataGr_varians as $key => $val) {
                            $this->varian->find($val->id)->delete();
                        }
                        foreach ($req->sku_varian as $key => $val) {
                            $this->varian->create([
                                'kode_barang' => $req->kode_barang,
                                'nama_varian' => $req->nama_varian[$key],
                                'conv_varian' => $req->conv_varian[$key],
                                'hpp_varian' => $req->hpp_varian[$key],
                                'sku_varian' => $val,
                                'harga_varian' => $req->harga_varian[$key]
                            ]);
                        }
                    } else {
                        foreach ($req->sku_varian as $key => $val) {
                            $this->varian->create([
                                'kode_barang' => $req->kode_barang,
                                'nama_varian' => $req->nama_varian[$key],
                                'conv_varian' => $req->conv_varian[$key],
                                'hpp_varian' => $req->hpp_varian[$key],
                                'sku_varian' => $val,
                                'harga_varian' => $req->harga_varian[$key]
                            ]);
                        }
                    }
                } else {
                    $this->varian->where('kode_barang', $req->kode_barang)->delete();
                }

                if ( $req->hasFile('file_gambar') ) {
                    Storage::delete($product->gambar_produk);

                    $name = $req->file('file_gambar')->getClientOriginalName();
                    $getName = pathinfo($name, PATHINFO_FILENAME);
                    $ext = $req->file('file_gambar')->getClientOriginalExtension();
                    $filename = $getName.'_'.time().'.'.$ext;
                    $path = $req->file('file_gambar')->storeAs('public/gambar_produk', $filename);

                    $product->gambar_produk = $path;
                }

                $product->save();

                Supply::where('kode_barang', $kode_barang)
                ->update(['kode_barang' => $req->kode_barang]);
                Transaction::where('kode_barang', $kode_barang)
                ->update(['kode_barang' => $req->kode_barang]);

                Session::flash('update_success', 'Data barang berhasil diubah');
                // $get_route = ;
                if($req->urlsearch !== 'false') {
                    $pagess = '?search='.$req->urlsearch;
                    $page = 'page='.$req->url;
                    return redirect('/product/search'.$pagess.'&'.$page);
                } else {
                    $page = '?page='.$req->url;
                    return redirect('/product'.$page);
                }
            }else{
                Session::flash('update_failed', 'Kode barang telah digunakan');
                return back();
            }
        }else{
            return back();
        }
    }

    // Delete Product
    public function deleteProduct($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $pr = Product::find($id);
            $uomExist = $this->uom_conversion->where('kode_barang', $pr->kode_barang)->first();
            if ( $uomExist ) {
                $uomExist->delete();
            }

            Storage::delete($pr->gambar_produk);

            Product::destroy($id);

            Session::flash('delete_success', 'Barang berhasil dihapus');

            return back();
        }else{
            return back();
        }
    }

    public function exportProductStockExport(Request $req)
    {
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        ini_set('memory_limit', '3000M'); //This might be too large, but depends on the data set
        // dd($req->search_key);
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        
        if($check_access->kelola_barang == 1){
            
            if($req->search_key !== 'false') {
                // dd($req->search_key);
                $products = Product::where('nama_barang', 'LIKE', '%' . $req->search_key . '%')
                ->orWhere('kode_barang', 'LIKE', '%' . $req->search_key . '%')
                ->get();
                $supply_system = Supply_system::first();
                $count = 0;
                if($supply_system->status) {
                    foreach($products as $product){
                        $count += ($product->harga * $product->stok);
                    }
                }
                $pdf = PDF::loadview('report.export_report_stock', compact('products', 'supply_system', 'count'));
                return $pdf->stream();
            } else {
                // dd($req->search_key);
                $products = Product::all()
                ->sortBy('kode_barang');
                
                $supply_system = Supply_system::first();
                $count = 0;
                if($supply_system->status) {
                    foreach($products as $product){
                        $count += ($product->harga * $product->stok);
                    }
                }
                $pdf = PDF::loadview('report.export_report_stock', compact('products', 'supply_system', 'count'));
                return $pdf->stream();
            }
        }else{
            return back();
        }
    }

    public function exportProductStockExportExcel(Request $req)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            
            if($req->search_key !== 'false') {
                
                $products = Product::where('nama_barang', 'LIKE', '%' . $req->search_key . '%')
                ->orWhere('kode_barang', 'LIKE', '%' . $req->search_key . '%')
                ->get();    
                $supply_system = Supply_system::first();
                $count = 0;
                if($supply_system->status) {
                    foreach($products as $product){
                        $count += ($product->harga * $product->stok);
                    }
                }
                return Excel::download(new StockExport($products, $supply_system, $count), 'stock.xlsx');
            } else {
                $products = Product::all()
                ->sortBy('kode_barang');
                $supply_system = Supply_system::first();
                $count = 0;
                if($supply_system->status) {
                    foreach($products as $product){
                        $count += ($product->harga * $product->stok);
                    }
                }
                return Excel::download(new StockExport($products, $supply_system, $count), 'stock.xlsx');
            }
        }else{
            return back();
        }
    }
}
