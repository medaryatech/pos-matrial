<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Master\Category;
use App\Models\Master\HargaGrosir;
use App\Models\Master\Varian;
use App\Models\UomConversion;
use App\Product;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->category = new Category();
        $this->uom_conversion = new UomConversion();
        $this->product = new Product();
        $this->harga_grosir = new HargaGrosir();
        $this->varian = new Varian();
    }
}
