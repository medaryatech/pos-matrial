<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use PDF;
use Auth;
use Session;
use App\Acces;
use App\Market;
use App\Product;
use App\Distributor;
use App\Activity;
use App\Transaction;
use App\Supply_system;
use App\Supply;
use Illuminate\Http\Request;
use App\Models\UomConversion;
use App\Models\Master\Varian;
// use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
// use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
// use Mike42\Escpos\CapabilityProfile;
// use Mike42\Escpos\Printer;
// use Mike42\Escpos\EscposImage;



class TransactionManageController extends Controller
{   
    public function getPoAll()
    {
        $data_customers = DB::table('transactions')
            ->select('id_pelanggan', 'pelanggan', DB::raw('COUNT(DISTINCT kode_transaksi) as total_transaksi'), DB::raw('sum(total) as total'))
            ->where('po', 1)
            ->groupBy('id_pelanggan')
            ->get();
        return $data_customers;
    }

    public function getPoList($id) {
        $transaction = Transaction::where('transactions.po', '=', 1)
            ->select('transactions.kode_transaksi', 'transactions.created_at', 'transactions.pelanggan', 'transactions.id_pelanggan', 'transactions.total')
            ->where('id_pelanggan', $id)
            ->orderBy('id_pelanggan', 'asc')
            ->get();
        return $transaction;
    }

    // Show View Transaction
    public function viewTransaction()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
        	$products = Product::all()
        	->sortBy('kode_barang');
            $varians = Varian::all()
        	->sortBy('nama_varian');
            $distributors = Distributor::all()
        	->sortBy('nama');
            $supply_system = Supply_system::first();

            return view('transaction.transaction', compact('products', 'supply_system', 'distributors', 'varians'));
        }else{
            return back();
        }
    }

    // Refresh Stock
    public function refreshStock()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
        	$products = Product::all()
        	->sortBy('kode_barang')->values()->all();
            $distributors = Distributor::all();
            return response()->json([
        		'products' => $products,
                'distributors' => $distributors
        	]);
        }else{
            return back();
        }
    }

    public function transactionProductConversion($id) {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
            $varian = Varian::where('id', '=', $id)->first();

        	return response()->json([
        		'varian' => $varian,
        	]);
        }else{
            return back();
        }
    }

    // Take Transaction Product
    public function transactionProduct($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
        	$product = Product::with('uom_conversion')->where('kode_barang', '=', $id)
        	->first();
            $daftarGrosir = $this->harga_grosir->where('kode_barang',$id)->get();
        	$supply_system = Supply_system::first();
        	$status = $supply_system->status;
            $varian = $this->varian->select('id','kode_barang','nama_varian', 'conv_varian', 'hpp_varian', 'sku_varian', 'harga_varian')->where('kode_barang',$product->kode_barang)->get();

        	return response()->json([
        		'product' => $product,
                'harga_grosir' => $daftarGrosir->count() > 0 ? $daftarGrosir : NULL,
                'varian' => ( $varian->count() > 0 ) ? $varian : NULL,
        		'status' => $status
        	]);
        }else{
            return back();
        }
    }

    // Check Transaction Product
    public function transactionProductCheck($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
        	$product_check = Product::where('kode_barang', '=', $id)
        	->count();

        	if($product_check != 0){
        		$product = Product::where('kode_barang', '=', $id)
    	    	->first();
    	    	$supply_system = Supply_system::first();
    	    	$status = $supply_system->status;
        		$check = "tersedia";
        	}else{
        		$product = '';
        		$status = '';
        		$check = "tidak tersedia";
        	}

        	return response()->json([
        		'product' => $product,
        		'status' => $status,
        		'check' => $check
        	]);
        }else{
            return back();
        }
    }

    // Transaction Process
    public function transactionProcess(Request $req)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
    		$jml_barang = count($req->kode_barang);
        	for($i = 0; $i < $jml_barang; $i++){
        		$transaction = new Transaction;
        		$transaction->kode_transaksi = $req->kode_transaksi;
        		$transaction->kode_barang = $req->kode_barang[$i];
                // $product_data = Product::where('kode_barang', $req->kode_barang[$i])
                // ->first();
                $transaction->nama_barang = $req->nama_barang[$i];
                $transaction->hpp = $req->harga_barang_hpp[$i];
                if($req->price_status === '1') {
                    // $transaction->harga = $product_data->harga;
                    $transaction->harga = $req->harga_barang[$i];
                } else {
                    $transaction->harga = 0;
                }
        		$transaction->jumlah = $req->jumlah_barang[$i];
        		$transaction->total_barang = $req->total_barang[$i];
                $transaction->total_barang_hpp = $req->total_barang_hpp[$i];
        		$transaction->subtotal = $req->subtotal;
                $transaction->subtotal_hpp = $req->subtotal_hpp;
        		$transaction->diskon = $req->diskon;
        		$transaction->total = $req->total;
                $transaction->total_hpp = $req->total_hpp;
        		$transaction->bayar = $req->bayar;
                if($req->pelanggan) {
                    $transaction->pelanggan = $req->pelanggan;
                    $transaction->id_pelanggan = $req->id_pelanggan;
                } else {
                    $transaction->pelanggan = 'No Name';
                    $transaction->id_pelanggan = 0;
                }
                $transaction->po = $req->po;
                if($req->name_add_value) {
                    $transaction->add_val = $req->name_add_value;
                    $transaction->value_add_val = $req->value_add_value;
                } else {
                    $transaction->add_val = null;
                    $transaction->value_add_val = 0;
                }
        		$transaction->kembali = $req->bayar - $req->total;
        		$transaction->id_kasir = Auth::id();
                $transaction->kasir = Auth::user()->nama;
        		$transaction->save();
        	}

        	$check_supply_system = Supply_system::first();
        	if($check_supply_system->status == true){
        		for($j = 0; $j < $jml_barang; $j++){
                    if($req->idvar[$j]) {
                        $cekkode = explode('-', $req->kode_barang[$j]);
                        $kodeready = $cekkode[0];
                        $varianget = Varian::where('id', '=', $req->idvar[$j])
                        ->first();
                        $product = Product::where('kode_barang', '=', $kodeready)
                        ->first();
                        $product->stok = $product->stok - ($req->jumlah_barang[$j] * $varianget->conv_varian);
                        $product->save();
                        $product_status = Product::where('kode_barang', '=', $kodeready)
                        ->first();
                        if($product_status->stok <= 0){
                            $product_status->keterangan = 'Habis';
                            $product_status->save();
                        }
                    } else {
                        $product = Product::where('kode_barang', '=', $req->kode_barang[$j])
                        ->first();
                        $product->stok = $product->stok - $req->jumlah_barang[$j];
                        $product->save();
                        $product_status = Product::where('kode_barang', '=', $req->kode_barang[$j])
                        ->first();
                        if($product_status->stok <= 0){
                            $product_status->keterangan = 'Habis';
                            $product_status->save();
                        }
                    }
        			
        		}
        	}

            $activity = new Activity;
            $activity->id_user = Auth::id();
            $activity->user = Auth::user()->nama;
            $activity->nama_kegiatan = 'transaksi';
            $activity->jumlah = $jml_barang;
            $activity->save();

        	Session::flash('transaction_success', $req->kode_transaksi);

        	return back();
        }else{
            return back();
        }
    }

    

    // Transaction Receipt
    public function receiptTransaction($id)
    {
        $market = Market::first();
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
            $transaction = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->first();
            $transactions = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->get();
            $diskon = $transaction->subtotal * $transaction->diskon / 100;

            $customPaper = array(0,0,400.00,283.80);
            $pdf = PDF::loadview('transaction.receipt_transaction', compact('transaction', 'transactions', 'diskon', 'market'))->setPaper($customPaper, 'landscape');
            return $pdf->stream();
            // function buatBaris1Kolom($kolom1){
            //     $lebar_kolom_1 = 48;
            //     $kolom1 = wordwrap($kolom1, $lebar_kolom_1, "\n", true);
            //     $kolom1Array = explode("\n", $kolom1);
            //     $jmlBarisTerbanyak = count($kolom1Array);
            //     $hasilBaris = array();
            //     for ($i = 0; $i < $jmlBarisTerbanyak; $i++) {
            //         $hasilKolom1 = str_pad((isset($kolom1Array[$i]) ? $kolom1Array[$i] : ""), $lebar_kolom_1, " ");
            //         $hasilBaris[] = $hasilKolom1;
            //     }
            //     return implode("\n", $hasilBaris) . "\n";
            // }
            // function buatBaris3Kolom($kolom1, $kolom2, $kolom3, $stats){
            //     $lebar_kolom_1 = 0;
            //     $lebar_kolom_2 = 0;
            //     $lebar_kolom_3 = 0;
            //     if($stats === 1) {
            //         $lebar_kolom_1 = 17;
            //         $lebar_kolom_2 = 9;
            //         $lebar_kolom_3 = 20;
            //     } else if($stats === 2) {
            //         $lebar_kolom_1 = 20;
            //         $lebar_kolom_2 = 1;
            //         $lebar_kolom_3 = 25;
            //     } else if($stats === 3) {
            //         $lebar_kolom_1 = 30;
            //         $lebar_kolom_2 = 1;
            //         $lebar_kolom_3 = 15;
            //     } else if($stats === 4) {
            //         $lebar_kolom_1 = 1;
            //         $lebar_kolom_2 = 44;
            //         $lebar_kolom_3 = 1;
            //     }
            //     $kolom1 = wordwrap($kolom1, $lebar_kolom_1, "\n", true);
            //     $kolom2 = wordwrap($kolom2, $lebar_kolom_2, "\n", true);
            //     $kolom3 = wordwrap($kolom3, $lebar_kolom_3, "\n", true);
            //     $kolom1Array = explode("\n", $kolom1);
            //     $kolom2Array = explode("\n", $kolom2);
            //     $kolom3Array = explode("\n", $kolom3);
            //     $jmlBarisTerbanyak = max(count($kolom1Array), count($kolom2Array), count($kolom3Array));
            //     $hasilBaris = array();
            //     for ($i = 0; $i < $jmlBarisTerbanyak; $i++) {
            //         $hasilKolom1 = '';
            //         $hasilKolom2 = '';
            //         $hasilKolom3 = '';
            //         if($stats === 4) {
            //             $hasilKolom1 = str_pad((isset($kolom1Array[$i]) ? $kolom1Array[$i] : ""), $lebar_kolom_1, " ");
            //             $hasilKolom2 = str_pad((isset($kolom2Array[$i]) ? $kolom2Array[$i] : ""), $lebar_kolom_2, " ", STR_PAD_BOTH);
            //             $hasilKolom3 = str_pad((isset($kolom3Array[$i]) ? $kolom3Array[$i] : ""), $lebar_kolom_3, " ");
            //         } else {
            //             $hasilKolom1 = str_pad((isset($kolom1Array[$i]) ? $kolom1Array[$i] : ""), $lebar_kolom_1, " ");
            //             $hasilKolom2 = str_pad((isset($kolom2Array[$i]) ? $kolom2Array[$i] : ""), $lebar_kolom_2, " ", STR_PAD_LEFT);
    
            //             $hasilKolom3 = str_pad((isset($kolom3Array[$i]) ? $kolom3Array[$i] : ""), $lebar_kolom_3, " ", STR_PAD_LEFT);
            //         }
            //         $hasilBaris[] = $hasilKolom1 . " " . $hasilKolom2 . " " . $hasilKolom3;
            //     }
            //     return implode("\n", $hasilBaris) . "\n";
            // }
            // // $profile = CapabilityProfile::load("simple");
            // // $connector = new WindowsPrintConnector("printer_sima");
            // // $printer = new Printer($connector, $profile);
            // $connector = new NetworkPrintConnector("192.168.1.125", 9100);
            // $printer = new Printer($connector);
            // $nama_kasir = explode(' ', $transaction->kasir);
            // $kasir = $nama_kasir[0];
            // $pelanggan = $transaction->pelanggan;
            // $printer->initialize();
            // $printer->selectPrintMode(Printer::MODE_FONT_A);
            // $printer->text(buatBaris3Kolom('', $market->nama_toko, '', 4));
            // $printer->text(buatBaris3Kolom('', $market->alamat, '', 4));
            // $printer->text(buatBaris3Kolom('', $market->no_telp, '', 4));
            // $printer->text("\n");
            // $printer->text(buatBaris3Kolom('No: '.$transaction->kode_transaksi, '', 'Kasir: '.$kasir, 2));
            // $printer->text(buatBaris3Kolom('Tanggal: '.date('d M, Y', strtotime($transaction->created_at)), '', 'Jam: '.date('H:i', strtotime($transaction->created_at)), 3));
            // $printer->text(buatBaris1Kolom('Cust: '.$pelanggan));
            // $printer->text(buatBaris1Kolom('------------------------------------------------'));
            // $counttotalbarang = 0;
            // foreach ($transactions as $transaksi) {
            //     $counttotalbarang += $transaksi->jumlah;
            //     $printer->text(buatBaris1Kolom($transaksi->nama_barang));
            //     $printer->text(buatBaris3Kolom(number_format($transaksi->harga,0,',','.'), $transaksi->jumlah.' Item', number_format($transaksi->total_barang,0,',','.'), 1));
            // }
            // $printer->text(buatBaris1Kolom('------------------------------------------------'));
            // $printer->text(buatBaris3Kolom('Jumlah: ', '', $counttotalbarang.' Item', 1));
            // $dsc = '';
            // if($transaction->diskon > 0) {
            //     $dsc = buatBaris3Kolom('Diskon ('.$transaction->diskon.'%): ', '', number_format($diskon,0,',','.'), 1);
            // } else {
            //     $dsc = buatBaris3Kolom('Diskon: ', '', '0%', 1);
            // }
            // $printer->text( $dsc);
            // if($transaction->add_val) {
            //     $printer->text(buatBaris3Kolom($transaction->add_val.': ', '', number_format($transaction->value_add_val,0,',','.'), 1));
            // }
            // $printer->text(buatBaris3Kolom('Total: ', '', number_format($transaction->total,0,',','.'), 1));
            // $statspo = '';
            // if($transaction->po === 1) {
            //     $statspo = 'PO';
            // } else if($transaction->po === 2) {
            //     $statspo = 'CASH';
            // } else {
            //     $statspo = 'TRANSFER';
            // }
            // $printer->text(buatBaris3Kolom('Bayar '.$statspo.': ', '', number_format($transaction->bayar,0,',','.'), 1));
            // $printer->text(buatBaris3Kolom('Kembali: ', '', number_format($transaction->kembali,0,',','.'), 1));
            // $printer->text("\n\n");
            // $printer->text(buatBaris3Kolom('', "Terima Kasih", '', 4));
            // $printer->text(buatBaris3Kolom('', "*Barang yang sudah dibeli tidak dapat dikembalikan/ditukar.", '', 4));
            // $printer->feed(4);
            // $printer->cut();
            // $printer->close();
            // return response()->json([
        	// 	'status' => 1
        	// ]);
        }else{
            // return response()->json([
        	// 	'status' => 0
        	// ]);
            return back();
        }
    }

    // Transaction Receipt
    public function receiptTransactionFull($id)
    {
        $market = Market::first();
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
            $transaction = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->first();
            $transactions = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->get();
            $diskon = $transaction->subtotal * $transaction->diskon / 100;
            // return view('transaction.receipt_transaction_invoice', compact('transaction', 'transactions', 'diskon', 'market'));
            $pdf = PDF::loadview('transaction.receipt_transaction_invoice', compact('transaction', 'transactions', 'diskon', 'market'));
            return $pdf->stream();
        }else{
            return back();
        }
    }

    public function receiptTransactionDo($id)
    {
        $market = Market::first();
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
            $transaction = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->first();
            $transactions = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->get();
            $diskon = $transaction->subtotal * $transaction->diskon / 100;
            // return view('transaction.receipt_transaction_do', compact('transaction', 'transactions', 'diskon', 'market'));
            $pdf = PDF::loadview('transaction.receipt_transaction_do', compact('transaction', 'transactions', 'diskon', 'market'));
            return $pdf->stream();
        }else{
            return back();
        }
    }

    public function changepocashTransaction($id) {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $transaction = Transaction::where('kode_transaksi', $id)->get();
            $jml_barang = count($transaction);
        	for($i = 0; $i < $jml_barang; $i++){
        		$transaction[$i]->po = 2;
        		$transaction[$i]->save();
        	}

            Session::flash('update_success', 'Data PO berhasil diubah');

            return back();
        }else{
            return back();
        }
    }

    public function changepotransferTransaction($id) {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $transaction = Transaction::where('kode_transaksi', $id)->get();
            $jml_barang = count($transaction);
        	for($i = 0; $i < $jml_barang; $i++){
        		$transaction[$i]->po = 3;
        		$transaction[$i]->save();
        	}

            Session::flash('update_success', 'Data PO berhasil diubah');

            return back();
        }else{
            return back();
        }
    }

    public function returTransaction($id) {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $transaction = Transaction::where('kode_transaksi', $id)->get();
            $jml_barang = count($transaction);
            $jumlah_data = 0;
        	for($i = 0; $i < $jml_barang; $i++){
                $cekkode = explode('-', $transaction[$i]->kode_barang);
                if(count($cekkode) > 1) {
                    $kodeready = $cekkode[0];
                    $sku = $cekkode[1];
                    $product_status = Product::where('kode_barang', $kodeready)
                    ->first();
                    if($product_status->stok > 0){
                        $product_status->keterangan = 'Tersedia';
                        $product_status->save();
                    }
    
                    $supply = new Supply;
                    $supply->kode_barang = $kodeready;
                    $product = Product::where('kode_barang', $kodeready)
                    ->first();
                    $varianget = Varian::where(
                    [
                        ['kode_barang', '=', $kodeready],
                        ['sku_varian', '=', $sku]
                    ])
                    ->first();
                    $supply->nama_barang = $product->nama_barang;
                    $supply->jumlah = ($transaction[$i]->jumlah * $varianget->conv_varian);
                    $supply->harga_beli = $product->harga;
                    $supply->id_pemasok = Auth::id();
                    $supply->pemasok = Auth::user()->nama;
                    $supply->save();
                    $jumlah_data += 1;
                } else {
                    $product_status = Product::where('kode_barang', $transaction[$i]->kode_barang)
                    ->first();
                    if($product_status->stok > 0){
                        $product_status->keterangan = 'Tersedia';
                        $product_status->save();
                    }
    
                    $supply = new Supply;
                    $supply->kode_barang = $transaction[$i]->kode_barang;
                    $product = Product::where('kode_barang', $transaction[$i]->kode_barang)
                    ->first();
                    $supply->nama_barang = $product->nama_barang;
                    $supply->jumlah = $transaction[$i]->jumlah;
                    $supply->harga_beli = $product->harga;
                    $supply->id_pemasok = Auth::id();
                    $supply->pemasok = Auth::user()->nama;
                    $supply->save();
                    $jumlah_data += 1;
                }
                
        	}

            $activity = new Activity;
            $activity->id_user = Auth::id();
            $activity->user = Auth::user()->nama;
            $activity->nama_kegiatan = 'retur';
            $activity->jumlah = $jumlah_data;
            $activity->save();

            Transaction::where('kode_transaksi', $id)->delete();
            Session::flash('delete_success', 'Barang berhasil diretur');

            return back();
        }else{
            return back();
        }
    }
}
