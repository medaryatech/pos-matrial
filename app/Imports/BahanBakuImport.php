<?php

namespace App\Imports;

use App\BahanBaku;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;

class BahanBakuImport implements ToModel, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new BahanBaku([
            'kode_bahan'     => $row[0],
            'jenis_bahan'    => $row[1],
            'nama_bahan'    => $row[2],
            'berat_bahan'    => $row[3],
            'stok'    => $row[4],
            'harga'    => $row[5],
        ]);
    }

    public function rules(): array
    {
        return [
            '0' => function($attribute, $value, $onFailure) {
                if (BahanBaku::where('kode_bahan', '=', $value)->count() > 0) {
                    $onFailure('tersedia');
                }elseif ($value == null || $value == '') {
                    $onFailure('kosong');
                }elseif ($value == 0) {
                    $onFailure('nol');
                }
            },
            '1' => 'required|string',
            '2' => 'required|string',
            '5' => 'required|numeric',
        ];
    }
}
