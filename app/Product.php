<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\UomConversion;
use App\Models\Master\HargaGrosir;
use App\Models\Master\Varian;
class Product extends Model
{
    // Initialize
    protected $fillable = [
        'kode_barang', 'jenis_barang', 'nama_barang', 'berat_barang', 'merek', 'stok', 'hpp', 'harga', 'harga_reseller', 'keterangan',
        'gambar_produk',
        'category_id'
    ];

    public function uom_conversion()
    {
        return $this->hasOne(UomConversion::class, 'kode_barang', 'kode_barang')->select('kode_barang','tipe_satuan','value_uom_sell');
    }

    public function daftarHargaGrosir()
    {
        return $this->hasMany(HargaGrosir::class, 'kode_barang', 'kode_barang')->select('minimal_grosir','harga_grosir');
    }

    public function daftarVarian()
    {
        return $this->hasMany(Varian::class, 'kode_barang', 'kode_barang')->select('nama_varian', 'conv_varian', 'hpp_varian', 'sku_varian', 'harga_varian');
    }
}