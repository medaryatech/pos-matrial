<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandMaster extends Model
{
    protected $table = 'brand_master';

    protected $fillable = [
        'kode_brand',
        'nama'
    ];
}
