<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'master_category';

    protected $fillable = [
        'nama',
        'keterangan',
    ];
}
