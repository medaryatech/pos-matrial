<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Varian extends Model
{
    protected $table = 'product_varian';

    protected $fillable = [
        'kode_barang',
        'nama_varian',
        'conv_varian',
        'hpp_varian',
        'sku_varian',
        'harga_varian'
    ];
}
