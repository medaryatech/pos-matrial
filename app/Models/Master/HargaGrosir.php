<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class HargaGrosir extends Model
{
    protected $table = 'product_harga_grosir';

    protected $fillable = [
        'kode_barang',
        'minimal_grosir',
        'harga_grosir'
    ];
}
