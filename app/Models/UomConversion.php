<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UomConversion extends Model
{
    protected $table = 'uom_conversion';

    protected $fillable = [
        'kode_barang',
        'tipe_satuan',
        'value_uom_sell',
    ];
}
