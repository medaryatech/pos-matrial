<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemBrand extends Model
{
    protected $table = 'item_brand';

    protected $fillable = [
        'kode_item',
        'nama',
        'brand_id',
        'value_satuan',
        'type_volume'
    ];

    public function brand()
    {
        return $this->hasOne(BrandMaster::class, 'id', 'brand_id');
    }
}
