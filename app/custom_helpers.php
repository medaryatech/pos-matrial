<?php

function getOnlyNumber($var) {
    $var = filter_var($var, FILTER_SANITIZE_NUMBER_INT);
    return $var;
}

function formatRupiah($angka){
    $result = "Rp. " . number_format($angka,0,'','.');
    return $result;
}