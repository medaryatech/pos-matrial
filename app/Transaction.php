<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    // Initialize
    protected $fillable = [
        'kode_transaksi', 'kode_barang', 'nama_barang', 'hpp', 'harga', 'jumlah', 'total_barang', 'total_barang_hpp', 'subtotal', 'subtotal_hpp', 'diskon', 'total', 'total_hpp', 'bayar', 'kembali', 'id_kasir', 'kasir', 'add_val', 'value_add_val',
    ];

}
