<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    // Initialize
    protected $fillable = [
        'nama', 'email', 'nama_toko', 'telp', 'alamat', 'no_kontrak'
    ];
}
