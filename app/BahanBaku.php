<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BahanBaku extends Model
{
    // Initialize
    protected $fillable = [
        'kode_bahan', 'jenis_bahan', 'nama_bahan', 'berat_bahan', 'stok', 'harga', 'keterangan',
    ];
}