<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BahanBakuExport implements FromView, ShouldAutoSize
{
    private $bahan_baku;
    private $count;
    public function __construct($bahan_baku, $count)
    {
        $this->bahan_baku = $bahan_baku;
        $this->count = $count;
    }
    public function view(): View
    {
        return view('report.export_report_bahan_baku_excel', [
            'bahan_baku' => $this->bahan_baku,
            'count' => $this->count
        ]);
    }
}
