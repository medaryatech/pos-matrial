<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TransactionExport implements FromView, ShouldAutoSize
{
    private $dates;
    private $tgl_awal;
    private $tgl_akhir;
    private $market;
    private $income;
    private $incomepo;
    private $incomecash;
    private $incometransfer;
    public function __construct($tgl_awal, $tgl_akhir, $market, $transactions, $income, $incomepo, $incomecash, $incometransfer)
    {
        $this->transactions = $transactions;
        $this->tgl_awal = $tgl_awal;
        $this->tgl_akhir = $tgl_akhir;
        $this->market = $market;
        $this->income = $income;
        $this->incomepo = $incomepo;
        $this->incomecash = $incomecash;
        $this->incometransfer = $incometransfer;
    }
    public function view(): View
    {
        return view('report.export_report_transaction_excel', [
            'transactions' => $this->transactions,
            'tgl_awal' => $this->tgl_awal,
            'tgl_akhir' => $this->tgl_akhir,
            'market' => $this->market,
            'income' => $this->income,
            'incomepo' => $this->incomepo,
            'incomecash' => $this->incomecash,
            'incometransfer' => $this->incometransfer
        ]);
    }
}
