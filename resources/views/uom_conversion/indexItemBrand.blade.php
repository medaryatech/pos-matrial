@extends('templates/main')
@section('css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{ asset('css/uom_conversion/style.css') }}">
@endsection
@section('content')
  <div class="row page-title-header">
    <div class="col-12">
      <div class="page-header d-flex justify-content-between align-items-center">
        <h4 class="page-title">Master Item</h4>
        <button type="button" class="btn btn-inverse-primary btn-new btn-sm2" onclick="tambahData()">
          Tambah Data Item
        </button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 mb-4">
      <div class="card card-noborder b-radius">
        <div class="card-body">
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table table-custom">
                <thead>
                  <tr>
                    <th>No</th>            
                    <th>Kode Item</th>
                    <th>Nama Brand</th>
                    <th>Nama Item</th>
                    <th>Tipe Satuan</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data as $key => $val)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td>{{ $val->kode_item }}</td>
                      <td>{{ $val->brand->nama }}</td>
                      <td>{{ $val->nama }}</td>
                      <td>{{ $val->value_satuan.' '.$val->type_volume }}</td>
                      <td>
                        <button type="button" class="btn btn-sm btn-warning" onclick="editData({{ $val->id }})">Edit Data</button>
                        <button type="button" class="btn btn-sm btn-danger btnDelete" data-id="{{ $val->id }}">Hapus Data</button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalTambah" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">Tambah Data</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="form-create" action="{{ route('itembrand_store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="modal-body pr-5 pl-5">
            <div class="form-group">
                <label for="brand_id" class="col-form-label">Pilih Merek</label>
                <select class="form-control select2" name="brand_id" id="brand_id" class="form-control" required style="width: 100%;">
                    <option value="" disabled selected>Pilih Merek</option>
                    @foreach ($brand as $val)
                        <option value="{{ $val->id }}">{{ $val->nama }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="nama_item" class="col-form-label">Nama Item <span class="text-danger">*</span></label>
                <input type="text" name="nama" class="form-control" id="nama_item" required placeholder="Masukkan Nama Item">
            </div>
            <div class="form-group">
                <label for="kode_item" class="col-form-label">Kode Item</label>
                <input type="text" name="kode_item" class="form-control" id="kode_item" placeholder="Masukkan Kode Item">
            </div>
            <div class="form-group row">
					  	<div class="col-lg-12 col-md-12 col-sm-12 space-bottom">
					  		<div class="row">
					  			<label class="col-12 font-weight-bold col-form-label">Tipe Satuan <span class="text-danger">*</span></label>
							  	<div class="col-12">
							  		<div class="input-group">
							  			<input type="number" class="form-control" id="value_satuan" name="value_satuan" placeholder="Masukkan Tipe Satuan" required>
							  			<div class="input-group-append">
							  				<select class="form-control select2" name="type_volume" id="type_volume" style="width: 100px">
							  					<option value="m">Meter</option>
							  					<option value="m3">Meter Kubik</option>
							  					<option value="satuan">Satuan</option>
							  					<option value="kol">Kol</option>
							  				</select>
							  			</div>
							  		</div>
							  	</div>
					  		</div>
					  	</div>
            </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary btn-new" id="btnSubmit">Tambah Data</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Brand</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="#" method="POST" id="formDeleteBr">
          @method('delete')
          @csrf
          <div class="modal-body pr-5 pl-5">
            <p>Apakaah anda yakin ingin menghapus data ini ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Hapus</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('script')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
      $('.select2').select2();
  });
    function tambahData() {
      $('#modalTitle').html('Tambah Data Item');
      $('#brand_id, #nama_item, #kode_item, #value_satuan').val('');
      $('#type_volume').val('m');
      $('#btnSubmit').html('Tambah Data');
      $('div #modalTambah').modal('show');
    }

    function editData(id) {
      $('#modalTitle').html('Edit Item');
      let url = "{{ route('itembrand_json_getData',':id') }}";
      url = url.replace(':id', id);

      let url_update = "{{ route('itembrand_update',':id') }}";
      url_update = url_update.replace(':id', id);

      $.ajax({
        url : url,
        type: 'get',
        success:function(res) {
          $('#brand_id').val(res.brand_id);
          $("#kode_item").val(res.kode_item);
          $('#nama_item').val(res.nama);
          $('#value_satuan').val(res.value_satuan);
          $('#type_volume').val(res.type_volume);
          
          $('#form-create').attr('action', url_update);
          $('#btnSubmit').html('Update Item');
          $('.select2').select2();
          $('div #modalTambah').modal('show');
        },
        error:function(res,status) {
          swal(
            status,
            res.responseText,
            "error"
          )
        }
      });
    }

    $('.btnDelete').on('click', function() {
      let id = $(this).data('id');
      let url = "{{ route('itembrand_delete',':id') }}";
      url = url.replace(':id', id);
      $('#formDeleteBr').attr('action', url);
      $('#modalDelete').modal('show');
    });
  </script>
@endsection