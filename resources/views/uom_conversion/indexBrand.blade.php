@extends('templates/main')
@section('css')
  <link rel="stylesheet" href="{{ asset('css/uom_conversion/style.css') }}">
@endsection
@section('content')
  <div class="row page-title-header">
    <div class="col-12">
      <div class="page-header d-flex justify-content-between align-items-center">
        <h4 class="page-title">Master Merek</h4>
        <button type="button" class="btn btn-inverse-primary btn-new btn-sm2" data-toggle="modal" data-target="#modalTambah">
          Tambah Data
        </button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 mb-4">
      <div class="card card-noborder b-radius">
        <div class="card-body">
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table table-custom">
                <thead>
                  <tr>
                    <th>No</th>            
                    <th>Kode Merk</th>
                    <th>Nama Merk</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data as $key => $val)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td>{{ $val->kode_brand }}</td>
                      <td>{{ $val->nama }}</td>
                      <td>
                        <button type="button" class="btn btn-sm btn-warning" onclick="editData({{ $val->id }})">Edit Data</button>
                        <button type="button" class="btn btn-sm btn-danger btnDelete" data-id="{{ $val->id }}">Hapus Data</button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalTambah" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">Tambah Data</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="form-create" action="{{ route('brand_store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="modal-body pr-5 pl-5">
            <div class="form-group">
              <label for="nama_brand" class="col-form-label">Nama Merek</label>
              <input type="text" name="nama" class="form-control" id="nama_brand" required>
            </div>

            <div class="form-group">
              <label for="kode_brand" class="col-form-label">Kode Merek</label>
              <input type="text" name="kode_brand" class="form-control" id="kode_brand" required>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary btn-new" id="btnSubmit">Tambah Data</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Brand</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="#" method="POST" id="formDeleteBr">
          @method('delete')
          @csrf
          <div class="modal-body pr-5 pl-5">
            <p>Apakaah anda yakin ingin menghapus data ini ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Hapus</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('script')
  <script type="text/javascript">
    function editData(id) {
      $('#modalTitle').html('Edit Brand');
      let url = "{{ route('brand_json_getData',':id') }}";
      url = url.replace(':id', id);

      let url_update = "{{ route('brand_update',':id') }}";
      url_update = url_update.replace(':id', id);

      $.ajax({
        url : url,
        type: 'get',
        success:function(res) {
          $('#nama_brand').val(res.nama);
          $("#kode_brand").val(res.kode_brand);
          
          $('#form-create').attr('action', url_update);
          $('#btnSubmit').html('Update Brand');
          $('div #modalTambah').modal('show');
        },
        error:function(res,status) {
          swal(
            status,
            res.responseText,
            "error"
          )
        }
      });
    }

    $('.btnDelete').on('click', function() {
      let id = $(this).data('id');
      let url = "{{ route('brand_delete',':id') }}";
      url = url.replace(':id', id);
      $('#formDeleteBr').attr('action', url);
      $('#modalDelete').modal('show');
    });
  </script>
@endsection