@extends('templates/main')
@section('css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{ asset('css/uom_conversion/style.css') }}">
@endsection
@section('content')
  <div class="row page-title-header">
    <div class="col-12">
      <div class="page-header d-flex justify-content-between align-items-center">
        <h4 class="page-title">Master UOM</h4>
        <button class="btn btn-inverse-primary btn-new btn-sm2" type="button" onclick="tambahData()">
          Tambah Data
        </button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 mb-4">
      <div class="card card-noborder b-radius">
        <div class="card-body">
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table table-custom">
                <thead>
                  <tr>
                    <th>No</th>     
                    <th>Nama Merk</th>       
                    <th>Nama Barang</th>
                    <th>Tipe Satuan</th>
                    <th>UOM Sell</th>
                    <th>Harga Beli</th>
                    <th>Harga Jual</th>
                    <th>Quantity</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data as $key => $val)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $val->brand->nama ?? '' }}</td>
                        <td>{{ $val->item->nama ?? '' }}</td>
                        <td>{{ ($val->item->value_satuan.' '.$val->item->type_volume) ?? '' }}</td>
                        <td>{{ $val->tipe_uom_sell }}</td>
                        <td>{{ formatRupiah($val->harga_beli) }}</td>
                        <td>{{ formatRupiah($val->harga_jual) }}</td>
                        <td>{{ $val->quantity }}</td>
                        <td>
                          <button type="button" class="btn btn-sm btn-warning" onclick="editData({{ $val->id }})">Edit Data</button>
                          <button type="button" class="btn btn-sm btn-danger btnDelete" data-id="{{ $val->id }}">Hapus Data</button>
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalTambah" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">Tambah Data</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="form-create" action="{{ route('uom_store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="modal-body pr-5 pl-5">
            <div class="form-group">
              <label for="brand_id" class="col-form-label">Nama Brand</label>
              <select name="brand_id" id="brand_id" class="form-control" required>
                <option value="" disabled selected>Pilih Brand</option>
                @foreach ($brand as $val)
                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                @endforeach
              </select>
            </div> 

            <div class="form-group">
              <label for="item_id" class="col-form-label">Nama Item</label>
                <select name="item_id[]" class="form-control select2-item" id="item_id" multiple="multiple">
                </select>
            </div>
            
            <div class="form-group">
              <label for="tipe_uom_sell" class="col-form-label">Value Uom Sell</label>
              <input type="text" name="tipe_uom_sell" class="form-control" id="tipe_uom_sell" placeholder="Masukkan Value Uom Sell">
            </div>
            <div class="form-group">
              <label for="harga_jual" class="col-form-label">Harga Jual</label>
              <input type="text" name="harga_jual" class="form-control idr" id="harga_jual" placeholder="Masukkan Harga Jual">
            </div>
            <div class="form-group">
              <label for="harga_beli" class="col-form-label">Harga Beli</label>
              <input type="text" name="harga_beli" class="form-control idr" id="harga_beli" placeholder="Masukkan Harga Beli">
            </div>
            <div class="form-group">
              <label for="nilai_hpp" class="col-form-label">Harga HPP ( Harga Pokok Produksi )</label>
              <input type="text" name="nilai_hpp" class="form-control idr" id="nilai_hpp">
            </div>
            <div class="form-group">
              <label for="quantity" class="col-form-label">Quantity</label>
              <input type="number" min="1" name="quantity" class="form-control" id="quantity" placeholder="Masukkan Quantity">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary btn-new" id="btnSubmit">Tambah Data</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Hapus Data</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="#" method="POST" id="formDeleteBr" enctype="multipart/form-data">
          @method('delete')
          @csrf
          <div class="modal-body">
            <p>Apakaah anda yakin ingin menghapus data ini ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Hapus Data</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('script')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="{{ asset('js/manage_bahan_baku/product/script.js') }}"></script>
  <script type="text/javascript">
    @if ($message = Session::get('create_success'))
      swal(
          "Berhasil!",
          "{{ $message }}",
          "success"
      );
    @endif

    @if ($message = Session::get('update_success'))
      swal(
          "Berhasil!",
          "{{ $message }}",
          "success"
      );
    @endif

    @if ($message = Session::get('delete_success'))
      swal(
          "Berhasil!",
          "{{ $message }}",
          "success"
      );
    @endif

    $('body').on('shown.bs.modal', '.modal', function() {
      $(this).find('select').each(function() {
        var dropdownParent = $(document.body);
        if ($(this).parents('.modal.in:first').length !== 0)
          dropdownParent = $(this).parents('.modal.in:first');
        $(this).select2({
          dropdownParent: dropdownParent,
          placeholder: `Pilih ${$(this).parent().find('label').text()}`
        });
      });
    });

    $("#brand_id").on('change', function() {
      var id = $(this).val();
      var url = "{{ route('getItemBrand',':id') }}";
      url = url.replace(':id', id);

      $.ajax({
        url : url,
        type : 'get',
        success:function(res) {
          $('.select2-item').empty();
          var html = '', satuan = '';
          res.forEach( function(data) {
            if ( data.type_volume == 'satuan' ) {
              satuan = data.type_volume;
            } else {
              satuan = data.value_satuan+' '+data.type_volume;
            }
            html += '<option value="'+data.id+'">'+data.nama+' | '+satuan+'</option>';
          });
          $('.select2-item').append(html);
        }
      })
    });

    function tambahData() {
      $('#modalTitle').html('Tambah Data');
      $('#brand_id, #tipe_uom_sell, #harga_jual, #harga_beli, #quantity').val('');
      $('.select2-item').empty();
      document.getElementById('item_id').setAttribute('multiple','');
      // $("#tipe_volume").val('');
      // $('#hitung_satuan').val('');
      $('#btnSubmit').html('Tambah Data');
      $('div #modalTambah').modal('show');
    }

    function editData(id) {
      $('#modalTitle').html('Edit Data');
      let url = "{{ route('uom_json_getData',':id') }}";
      url = url.replace(':id', id);

      let url_update = "{{ route('uom_update',':id') }}";
      url_update = url_update.replace(':id', id);

      $.ajax({
        url : url,
        type: 'get',
        success:function(res) {
          $('#brand_id').val(res.data.brand_id);
          if ( res.itemBrand.length > 0 ) {
            $('.select2-item').empty().removeAttr('multiple');
            html = '', satuan = '';
            res.itemBrand.forEach( function(data) {
              if ( data.type_volume == 'satuan' ) {
                satuan = data.type_volume;
              } else {
                satuan = data.value_satuan+' '+data.type_volume;
              }
              html += '<option value="'+data.id+'" >'+data.nama+' | '+satuan+'</option>';
            });
            $('.select2-item').append(html);
            $('#item_id').val(res.data.item_id);
          }
          $("#tipe_volume").val(res.data.tipe_volume);
          $('#tipe_uom_sell').val(res.data.tipe_uom_sell);
          $('#harga_jual').val(formatRupiah(res.data.harga_jual.toString(), 'Rp. '));
          $('#harga_beli').val(formatRupiah(res.data.harga_beli.toString(), 'Rp. '));
          $('#nilai_hpp').val(formatRupiah(res.data.nilai_hpp.toString(), 'Rp. '));
          $('#quantity').val(res.data.quantity);
          $('#hitung_satuan').val(res.data.hitung_satuan);
          
          $('#form-create').attr('action', url_update);
          $('#btnSubmit').html('Update Data');
          $('div #modalTambah').modal('show');
        }
      });
    }
    
    $('.btnDelete').on('click', function() {
      let id = $(this).data('id');
      let url = "{{ route('uom_delete',':id') }}";
      url = url.replace(':id', id);
      $('#formDeleteBr').attr('action', url);
      $('#modalDelete').modal('show');
    });
  </script>
@endsection