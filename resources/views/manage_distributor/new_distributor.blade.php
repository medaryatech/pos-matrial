@extends('templates/main')
@section('css')
<link rel="stylesheet" href="{{ asset('css/manage_account/new_account/style.css') }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
@endsection
@section('content')
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header d-flex justify-content-start align-items-center">
            <div class="quick-link-wrapper d-md-flex flex-md-wrap">
                <ul class="quick-links">
                    <li><a href="{{ url('distributor') }}">Daftar Akun</a></li>
                    <li><a href="{{ url('distributor/new') }}">Akun Baru</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card card-noborder b-radius">
            <div class="card-body">
                <form action="{{ url('distributor/create') }}" method="post" name="create_dist_form"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-12 font-weight-bold col-form-label">Nama <span
                                class="text-danger">*</span></label>
                        <div class="col-12">
                            <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
                        </div>
                        <div class="col-12 error-notice" id="nama_error"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 font-weight-bold col-form-label">Email <span
                                class="text-danger">*</span></label>
                        <div class="col-12">
                            <input type="email" class="form-control" name="email" placeholder="Masukkan Email">
                        </div>
                        <div class="col-12 error-notice" id="email_error"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 font-weight-bold col-form-label">Nama Toko <span
                                class="text-danger">*</span></label>
                        <div class="col-12">
                            <input type="text" class="form-control" name="nama_toko" placeholder="Masukkan Nama Toko">
                        </div>
                        <div class="col-12 error-notice" id="nama_toko_error"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 font-weight-bold col-form-label">Tanggal Join</span></label>
                        <div class="col-12">
                            <input type="text" class="form-control" name="tanggal_join"
                                placeholder="Masukkan Tanggal Join" id="datepicker">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 font-weight-bold col-form-label">No. Telp <span
                                class="text-danger">*</span></label>
                        <div class="col-12">
                            <input type="tel" class="form-control" name="telp" placeholder="Masukkan Telp">
                        </div>
                        <div class="col-12 error-notice" id="telp_error"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 font-weight-bold col-form-label">Alamat <span
                                class="text-danger">*</span></label>
                        <div class="col-12">
                            <textarea type="text" class="form-control" name="alamat"
                                placeholder="Masukkan Alamat"></textarea>
                        </div>
                        <div class="col-12 error-notice" id="alamat_error"></div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12 d-flex justify-content-end">
                            <button class="btn simpan-btn btn-sm" type="submit"><i class="mdi mdi-content-save"></i>
                                Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/manage_distributor/new_distributor/script.js') }}"></script>
{{-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> --}}
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<script type="text/javascript">
    @if($message = Session::get('both_error'))
    swal(
        "",
        "{{ $message }}",
        "error"
    );
    @endif

    @if($message = Session::get('email_error'))
    swal(
        "",
        "{{ $message }}",
        "error"
    );
    @endif

    $(function () {
        $("#datepicker").datepicker();
    });
</script>
@endsection