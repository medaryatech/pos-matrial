@foreach($bahan_baku as $product)
<tr>
  <td>
    <span class="kd-barang-field">{{ $product->kode_bahan }}</span><br><br>
    <span class="nama-barang-field">{{ $product->nama_bahan }}</span>
  </td>
  <td>{{ $product->jenis_bahan }}</td>
  @if($product->berat_bahan === 'kg')         
  <td>Kilogram</td>         
  @elseif ($product->berat_bahan === 'g')
  <td>Gram</td>
  @elseif ($product->berat_bahan === 'ml')
  <td>Miligram</td>
  @elseif ($product->berat_bahan === 'oz')
  <td>Ons</td>
  @elseif ($product->berat_bahan === 'l')
  <td>Liter</td>
  @elseif ($product->berat_bahan === 'ml')
  <td>Mililiter</td>
  @else
  <td>Pcs</td>
  @endif
  <td><span class="ammount-box bg-secondary"><i class="mdi mdi-cube-outline"></i></span>{{ $product->stok }}</td>
  <td><span class="ammount-box bg-green"><i class="mdi mdi-coin"></i></span>Rp. {{ number_format($product->harga,2,',','.') }}</td>
  <td>
    @if($product->keterangan == 'Tersedia')
    <span class="btn tersedia-span">{{ $product->keterangan }}</span>
    @else
    <span class="btn habis-span">{{ $product->keterangan }}</span>
    @endif
  </td>
  <td>
    <button type="button" class="btn btn-edit btn-icons btn-rounded btn-secondary" data-toggle="modal" data-target="#editModal" data-edit="{{ $product->id }}">
        <i class="mdi mdi-pencil"></i>
    </button>
    <button type="button" class="btn btn-icons btn-rounded btn-secondary ml-1 btn-delete" data-delete="{{ $product->id }}">
        <i class="mdi mdi-close"></i>
    </button>
  </td>
</tr>
@endforeach