@extends('templates/main')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css">
<link rel="stylesheet" href="{{ asset('css/manage_product/product/style.css') }}">
@endsection
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header d-flex justify-content-between align-items-center">
      <h4 class="page-title">Stok</h4>
      <div class="d-flex justify-content-start">
      	<div class="dropdown">
          <form action="{{ url('/product/stock/exportexcel') }}" name="export_form" method="POST" target="_blank" style="position: absolute;left: -48px;">
            @csrf
            <input id="searchkey" type="text" name="search_key" hidden="">
            <button class="btn btn-icons btn-inverse-primary btn-filter shadow-sm" type="submit" style="margin-right: 4px; padding-top: 12px">
              <i class="mdi mdi-file-excel print-icon"></i>
            </button>
          </form>
          <form action="{{ url('/product/stock/export') }}" name="export_form" method="POST" target="_blank">
            @csrf
            <input id="searchkeypdf" type="text" name="search_key" hidden="">
            <button class="btn btn-icons btn-inverse-primary btn-filter shadow-sm" type="submit" style=" padding-top: 12px">
              <i class="mdi mdi-file-pdf print-icon"></i>
            </button>
            <button class="btn btn-icons btn-inverse-primary btn-filter shadow-sm" type="button" id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: none">
              <i class="mdi mdi-filter-variant"></i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1">
              <h6 class="dropdown-header">Urut Berdasarkan :</h6>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item filter-btn" data-filter="kode_barang">Kode Barang</a>
              <a href="#" class="dropdown-item filter-btn" data-filter="jenis_barang">Jenis Barang</a>
              <a href="#" class="dropdown-item filter-btn" data-filter="nama_barang">Nama Barang</a>
              <a href="#" class="dropdown-item filter-btn" data-filter="merek">Merek Barang</a>
              @if($supply_system->status == true)
              <a href="#" class="dropdown-item filter-btn" data-filter="stok">Stok Barang</a>
              @endif
              <a href="#" class="dropdown-item filter-btn" data-filter="harga">Harga Barang</a>
            </div>
          </form>
	      </div>
        <div class="dropdown dropdown-search">
          <button class="btn btn-icons btn-inverse-primary btn-filter shadow-sm ml-2" type="button" id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="mdi mdi-magnify"></i>
          </button>
          <div class="dropdown-menu search-dropdown" aria-labelledby="dropdownMenuIconButton1">
            <div class="row">
              <div class="col-11">
                <form action="{{ url('/product/search') }}" method="get" id="form_search_product" >
                  <input id="search_product" type="text" class="form-control" name="search" placeholder="Cari barang">
                </form>
              </div>
            </div>
          </div>
        </div>
	      <a href="{{ url('/product/new') }}" class="btn btn-icons btn-inverse-primary btn-new ml-2">
	      	<i class="mdi mdi-plus"></i>
	      </a>
      </div>
    </div>
  </div>
</div>
<div class="row modal-group">
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ url('/product/update') }}" method="post" name="update_form" enctype="multipart/form-data">
          <div class="modal-header">
            <h5 class="modal-title" id="editModalLabel">Edit Barang</h5>
            <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="edit-modal-body">
              @csrf
              <div class="row" hidden="">
                <div class="col-12">
                  <input type="text" name="id">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">Kode Barang</label>
                <div class="col-lg-7 col-md-7 col-sm-10 col-10">
                  <input type="number" class="form-control" name="kode_barang">
                </div>
                <style>
                  @media (max-width: 991px) {
                    .mobnopadleft {
                      padding-left: unset
                    }
                  }
                </style>
                <div class="col-lg-2 col-md-2 col-sm-2 col-2 mobnopadleft">
                  <button class="btn btn-inverse-primary btn-sm btn-scan shadow-sm" type="button"><i class="mdi mdi-crop-free"></i></button>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 offset-lg-3 offset-md-3 error-notice" id="kode_barang_error"></div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">Jenis Barang</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <select class="form-control" name="jenis_barang">
                    <option value="Produksi">Produksi</option>
                    <option value="Konsumsi">Konsumsi</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">Nama Barang</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <input type="text" class="form-control" name="nama_barang">
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 offset-lg-3 offset-md-3 error-notice" id="nama_barang_error"></div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">Merek Barang</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <input type="text" class="form-control" name="merek">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">Kategori Barang</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <select class="form-control" name="kategori_id" id="kategori_id">
										@forelse ($category as $key => $val)
											<option value="{{ $val->id }}">{{ $val->nama }}</option>
										@empty
											<option value="" disabled selected>-- Kategori Kosong --</option>
										@endforelse
									</select>
                </div>
              </div>
              <div class="form-group row" style="display: none">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold mb-3 font-weight-bold col-form-label">Atur UOM Conversion
                  <div class="custom-checkbox" style="top: 15px; z-index: 1;">
                    <input id="uom_check" type="checkbox" name="isSetConversion" class="switch-mode">
                    <label for="uom_check">
                        <div class="status-switch" data-unchecked="Off" data-checked="On">
                        </div>
                    </label>
                  </div>
              </label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <table class="table table-responsive table-borderless tbl_uom mt-5">
										<tbody>
											<tr>
												<th>Tipe Satuan Barang</th>
												<td class="pl-3">
													<select class="form-control" name="tipe_satuan" id="tipe_satuan">
														<option value="pcs">Pcs</option>
														<option value="lembar">Lembar</option>
														<option value="zak">Zak</option>
													</select>
												</td>
											</tr>
											<tr>
												<th>Nilai UOM Sell</th>
												<td class="pl-3">
													<input type="number" name="value_uom_sell" class="form-control" id="value_uom_sell">
												</td>
											</tr>
										</tbody>
									</table>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">HPP</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <div class="input-group">
                      <input type="text" class="form-control idr input-notzero" name="hpp">
                  </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 offset-lg-3 offset-md-3 error-notice" id="hpp_error"></div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">Harga Barang</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <div class="input-group">
                      <input type="text" class="form-control idr input-notzero" name="harga">
                  </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 offset-lg-3 offset-md-3 error-notice" id="harga_error"></div>
              </div>
              {{-- <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">Harga Reseller</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Rp. </span>
                      </div>
                      <input type="number" class="form-control number-input input-notzero" name="harga_reseller">
                  </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 offset-lg-3 offset-md-3 error-notice" id="harga_reseller_error"></div>
              </div> --}}
              <div class="form-group row" @if($supply_system->status == false) hidden="" @endif>
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">Stok Barang</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <input type="number" class="form-control number-input" name="stok">
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 offset-lg-3 offset-md-3 error-notice" id="stok_error"></div>
              </div>
              
              <div class="form-group row">
                  <div class="col-12">
                      <div class="row" id="harga_grosir">
                          <label class="col-12 font-weight-bold col-form-label" id="label_grosir">Harga Grosir
                              <div class="custom-checkbox">
                                  <input id="status" type="checkbox" name="status" value="0" class="switch-mode">
                                  <label for="status">
                                      <div class="status-switch" data-unchecked="Off" data-checked="On"></div>
                                  </label>
                              </div>
                          </label>
                          <div class="container-gross">
                              <div style="display: flex" class="gross">
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 offset-lg-3 offset-md-3 error-notice">
                    <label class="col-12 error-notice error" id="harga_grosir_error" style="padding: 0;margin-top: -7px !important;"></label>
                  </div>
              </div>

              <div class="form-group row">
                  <div class="col-12">
                      <div class="row" id="harga_varians">
                          <label class="col-12 font-weight-bold col-form-label" id="label_varians">Varians
                              <div class="custom-checkbox" style="margin-left: 77px;">
                                  <input id="varians" type="checkbox" name="varians" value="0" class="switch-mode">
                                  <label for="varians">
                                      <div class="status-switch" data-unchecked="Off" data-checked="On"></div>
                                  </label>
                              </div>
                          </label>
                          <div class="container-varians">
                              <div style="display: flex" class="varians">
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 offset-lg-3 offset-md-3 error-notice">
                    <label class="col-12 error-notice error" id="harga_varians_error" style="padding: 0;margin-top: -7px !important;"></label>
                  </div>
              </div>


              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-sm-12 col-form-label font-weight-bold">Gambar Barang</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                  <input type="file" class="dropify" name="file_gambar"/>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 offset-lg-3 offset-md-3 error-notice" id="file_gambar_error"></div>
              </div>
          </div>
          <div class="modal-body" id="scan-modal-body" hidden="">
            <div class="row">
              <div class="col-12 text-center" id="area-scan">
              </div>
              <div class="col-12 barcode-result" hidden="">
                <h5 class="font-weight-bold">Hasil</h5>
                <div class="form-border">
                  <p class="barcode-result-text"></p>
                </div>
              </div>
            </div>
          </div>
          <input id="urle" type="text" name="url" hidden="">
          <input id="urles" type="text" name="urlsearch" hidden="">
          <div class="modal-footer" id="edit-modal-footer">
            <button type="submit" class="btn btn-update"><i class="mdi mdi-content-save"></i> Simpan</button>
          </div>
          <div class="modal-footer" id="scan-modal-footer" hidden="">
            <button type="button" class="btn btn-primary btn-sm font-weight-bold rounded-0 btn-continue">Lanjutkan</button>
            <button type="button" class="btn btn-outline-secondary btn-sm font-weight-bold rounded-0 btn-repeat">Ulangi</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <div class="alert alert-primary d-flex justify-content-between align-items-center" role="alert">
      @if($supply_system->status == false)
      <div class="text-instruction">
        <i class="mdi mdi-information-outline mr-2"></i> Aktifkan sistem stok dan pasok barang dengan klik tombol disamping
      </div>
      <a href="{{ url('/supply/system/active') }}" class="btn stok-btn">Aktifkan</a>
      @else
      <div class="text-instruction">
        <i class="mdi mdi-information-outline mr-2"></i> Nonaktifkan sistem stok dan pasok barang dengan klik tombol disamping
      </div>
      <a href="{{ url('/supply/system/nonactive') }}" class="btn stok-btn">Nonaktifkan</a>
      @endif
    </div>
  </div>
  <div class="col-12 grid-margin">
    <div class="card card-noborder b-radius">
      <div class="card-body">
        <div class="row">
        	<div class="col-12 table-responsive">
        		<table class="table table-custom">
              <thead>
                <tr>
                  <th>Barang</th>
                  <th>Jenis</th>
                  {{-- <th>Berat</th> --}}
                  <th>Merk</th>
                  @if($supply_system->status == true)
                  <th>Stok</th>
                  @endif
                  <th>Harga</th>
                  <th>Keterangan</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($products as $product)
                @php
                $pricevar = array();
                $countvar=0;
                $firstprice = 0;
                $lastprice = 0;
                @endphp
              	<tr>
                  @foreach($varians as $varian)
                  @php
                  if($varian->kode_barang === $product->kode_barang) {
                    $countvar += 1;
                    array_push($pricevar, $varian->harga_varian);
                    sort($pricevar);
                  }
                  @endphp
                  @endforeach
                  @foreach ($pricevar as $index => $valuesdfa)
                    @php
                      if($index === array_key_first($pricevar)) {
                        $firstprice = $valuesdfa;
                      }
                      if($index === array_key_last($pricevar)) {
                        $lastprice = $valuesdfa;
                      }
                    @endphp
                  @endforeach
                  <td>
                    <span class="kd-barang-field">{{ $product->kode_barang }}</span><br><br>
                    <span class="nama-barang-field">{{ $product->nama_barang }}</span>
                    @if($countvar > 0)
                    <div style="display: initial;color: #2355ed;font-size: 10px;border: 1px solid #2355ed;border-radius: 7px;padding-top: 1px;padding-left: 4px;padding-right: 4px;font-weight: 900;">
                      {{$countvar}} VARIAN
                    </div>
                    @endif
                  </td>
                  <td>{{ $product->jenis_barang }}</td>
                  {{-- <td>{{ $product->berat_barang }}</td> --}}
                  <td>{{ $product->merek }}</td>
                  @if($supply_system->status == true)
                  <td><span class="ammount-box bg-secondary"><i class="mdi mdi-cube-outline"></i></span>{{ $product->stok }}</td>
                  @endif
                  @if($firstprice)
                    @if($firstprice && $lastprice)
                    <td><span class="ammount-box bg-green"><i class="mdi mdi-coin"></i></span>Rp. {{ number_format($firstprice,2,',','.') }} - {{ number_format($lastprice,2,',','.') }}</td>
                    @else
                    <td><span class="ammount-box bg-green"><i class="mdi mdi-coin"></i></span>Rp. {{ number_format($firstprice,2,',','.') }}</td>
                    @endif
                  @else
                  <td><span class="ammount-box bg-green"><i class="mdi mdi-coin"></i></span>Rp. {{ number_format($product->harga,2,',','.') }}</td>
                  @endif
                  @if($supply_system->status == true)
                  <td>
                    @if($product->keterangan == 'Tersedia')
                    <span class="btn tersedia-span">{{ $product->keterangan }}</span>
                    @else
                    <span class="btn habis-span">{{ $product->keterangan }}</span>
                    @endif
                  </td>
                  @endif
                  <td>
                    <button type="button" class="btn btn-edit btn-icons btn-rounded btn-secondary" data-toggle="modal" data-target="#editModal" data-edit="{{ $product->id }}">
                        <i class="mdi mdi-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-icons btn-rounded btn-secondary ml-1 btn-delete" data-delete="{{ $product->id }}">
                        <i class="mdi mdi-close"></i>
                    </button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        	</div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="d-flex col-12 justify-content-end mt-2">
  {{ $products->links() }}
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script src="{{ asset('plugins/js/quagga.min.js') }}"></script>
<script src="{{ asset('js/manage_product/product/script.js') }}"></script>
<script type="text/javascript">
  @if ($message = Session::get('create_success'))
    swal(
        "Berhasil!",
        "{{ $message }}",
        "success"
    );
  @endif

  @if ($message = Session::get('update_success'))
    swal(
        "Berhasil!",
        "{{ $message }}",
        "success"
    );
  @endif

  @if ($message = Session::get('delete_success'))
    swal(
        "Berhasil!",
        "{{ $message }}",
        "success"
    );
  @endif  

  @if ($message = Session::get('import_success'))
    swal(
        "Berhasil!",
        "{{ $message }}",
        "success"
    );
  @endif

  @if ($message = Session::get('update_failed'))
    swal(
        "",
        "{{ $message }}",
        "error"
    );
  @endif

  @if ($message = Session::get('supply_system_status'))
    swal(
        "",
        "{{ $message }}",
        "success"
    );
  @endif

  $( document ).ready(function() {
    var seadsa = getUrlParameter('search');
    $('#searchkey').val(seadsa);
    $('#searchkeypdf').val(seadsa);
    if(seadsa) {
      $(".dropdown.dropdown-search button").click();
      $('#search_product').val(seadsa.replace(/\+/g, ' '));
      // $('#search_product').focus();
    }
  });

  // $(document).on('click', '.filter-btn', function(e){
  //   e.preventDefault();
  //   var data_filter = $(this).attr('data-filter');
  //   $.ajax({
  //     method: "GET",
  //     url: "{{ url('/product/filter') }}/" + data_filter,
  //     success:function(data)
  //     {
  //       $('tbody').html(data);
  //     }
  //   });
  // });

  getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    if(sParam === 'page') {
      return 1;
    } else {
      return false;
    }
    
};

  $(document).on('keypress', '#search_product',function(e) {
      if(e.which == 13) {
        $('#form_search_product').submit();
      }
  });

  $(document).on('click', '.btn-edit', function(){
    var data_edit = $(this).attr('data-edit');
    $.ajax({
      method: "GET",
      url: "{{ url('/product/edit') }}/" + data_edit,
      success:function(response)
      {
        $('input[name=id]').val(response.product.id);
        $('input[name=kode_barang]').val(response.product.kode_barang);
        $('input[name=nama_barang]').val(response.product.nama_barang);
        $('input[name=merek]').val(response.product.merek);
        $('input[name=stok]').val(response.product.stok);
        $('input[name=hpp]').val(formatRupiah(response.product.hpp.toString(), 'Rp. '));
        $('input[name=harga]').val(formatRupiah(response.product.harga.toString(), 'Rp. '));
        $('#kategori_id').val(response.product.category_id);
        // $('input[name=harga_reseller]').val(response.product.harga_reseller);
        // if (response.product.berat_barang) {
        //   var berat_barang = response.product.berat_barang.split(" ");
        //   $('input[name=berat_barang]').val(berat_barang[0]);
        //   $('select[name=satuan_berat] option[value="'+ berat_barang[1] +'"]').prop('selected', true);
        // }
        $('select[name=jenis_barang] option[value="'+ response.product.jenis_barang +'"]').prop('selected', true);
        var urlen = getUrlParameter('page');
        var urlens = getUrlParameter('search');
        $('#urle').val(urlen);
        $('#urles').val(urlens);
        validator.resetForm();

        let dropify = $('.dropify').dropify();
        dropify = dropify.data('dropify');
        dropify.resetPreview();
        dropify.clearElement();
        dropify.destroy();
        if (response.product.gambar_produk) {
          const gambar_produk = `storage/${response.product.gambar_produk.split('/').slice(1).join('/')}`;
          dropify.settings.defaultFile = gambar_produk;
        } else {
          dropify.settings.defaultFile = '';
        }
        dropify.settings.maxFileSize = '2M';
        dropify.settings.messages = {
          'default': 'Masukkan Gambar Barang',
        };
        dropify.init();

        if (response.uom !== null) {
          $('.tbl_uom').show();
          $('#uom_check').prop('checked', true);
          $('input[name=value_uom_sell]').val(response.uom.value_uom_sell.slice(0, response.uom.value_uom_sell.indexOf('.')));
          $('select[name=tipe_satuan]').val(response.uom.tipe_satuan);
        } else {
          $('.tbl_uom').hide();
          $('#uom_check').prop('checked', false)
        }

        if (response.harga_grosir !== null) {
          if ($('#status').val() === '0') {
            $('#status').click();
          }
          let grosir_dom = '';
          response.harga_grosir.forEach(function(e) {
            grosir_dom += `<div style="display: flex;margin-bottom: 10px" class="gross">
              <div class="col-5" style="display: flex; padding-right: unset">
                <div class="input-group-prepend">
                  <span class="input-group-text paddmob10" style="border-bottom-right-radius: unset; border-top-right-radius: unset">≥ </span>
                </div>
                <input type="number" class="form-control number-input min-grosir" name="minimal_grosir[]" placeholder="Min" style="border-radius: unset" value="${e.minimal_grosir}">
              </div>
              <div class="col-7" style="display: flex; padding-left: unset">
                <div class="input-group-prepend">
                  <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
                </div>
                <input type="number" class="form-control number-input harga-grosir" name="harga_grosir[]" placeholder="Harga Grosir" style="border-bottom-left-radius: unset; border-top-left-radius: unset;" value="${e.harga_grosir}">
              </div>
              <div id="remove_gross" style="color: red; cursor: pointer; padding-top: 3px; user-select: unset">x</div>
              <input type="hidden" name="grosir_id[]" value="${e.id}">
            </div>`
          })
          $('.container-gross').html(grosir_dom);
        } else {
          if ($('#status').val() === '1') {
            $('#status').click();
          }
        }
        if (response.varian && response.varian !== null) {
          if ($('#varians').val() === '0') {
            $('#varians').click();
          }
          let varians_dom = '';
          response.varian.forEach(function(e) {
            varians_dom += `<div style="display: flex;margin-bottom: 20px;" class="varians">
          <div class="col-12 row">
            <div class="col-12" style="display: flex;margin-bottom: 8px;width: 108%;max-width: 108%;flex: 0 0 108%;"> 
              <input type="text" class="form-control nama-varian" name="nama_varian[]" placeholder="Nama Varian" value="${e.nama_varian}">
            </div>
            <div class="col-5" style="display: flex; padding-right: unset;margin-bottom: 8px;">
              <input type="number" class="form-control number-input conv-varian" name="conv_varian[]" placeholder="Conversion" style="border-bottom-right-radius: unset; border-top-right-radius: unset" value="${e.conv_varian}">
            </div>
            <div class="col-7" style="display: flex; padding-left: unset">
              <div class="input-group-prepend" style="height: 32px;">
                <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
              </div>
              <input type="number" class="form-control number-input hpp-varian" name="hpp_varian[]" placeholder="HPP Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;width: 138px;max-width: 138px;" value="${e.hpp_varian}">
            </div>
            <div class="col-5" style="display: flex; padding-right: unset">
              <input type="number" class="form-control number-input sku-varian" name="sku_varian[]" placeholder="SKU" style="border-bottom-right-radius: unset; border-top-right-radius: unset" value="${e.sku_varian}">
            </div>
            <div class="col-7" style="display: flex; padding-left: unset">
              <div class="input-group-prepend">
                <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
              </div>
              <input type="number" class="form-control number-input harga-varian" name="harga_varian[]" placeholder="Harga Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;width: 138px;max-width: 138px;" value="${e.harga_varian}">
            </div>
          </div>
          <div id="remove_varians" style="color: red; cursor: pointer; user-select: unset;align-self: center; margin-left: 25px;">x</div>
          <input type="hidden" name="varian_id[]" value="${e.id}">
        </div>`
          })
          $('.container-varians').html(varians_dom);
        } else {
          if ($('#varians').val() === '1') {
            $('#varians').click();
          }
        }
      }
    });
  });

  $(document).on('click', '.btn-delete', function(e){
    e.preventDefault();
    var data_delete = $(this).attr('data-delete');
    swal({
      title: "Apa Anda Yakin?",
      text: "Data barang akan terhapus, klik oke untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.open("{{ url('/product/delete') }}/" + data_delete, "_self");
      }
    });
  });

  $(document).on('click', '#tambah_harga_grosir', function(e){
	e.preventDefault();
	const newform = `<div style="display: flex;margin-bottom: 10px" class="gross">
									<div class="col-5" style="display: flex; padding-right: unset">
										<div class="input-group-prepend">
											<span class="input-group-text paddmob10" style="border-bottom-right-radius: unset; border-top-right-radius: unset">≥ </span>
										</div>
										<input type="number" class="form-control number-input min-grosir" name="minimal_grosir[]" placeholder="Min" style="border-radius: unset">
									</div>
									<div class="col-7" style="display: flex; padding-left: unset">
										<div class="input-group-prepend">
											<span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
										</div>
										<input type="number" class="form-control number-input harga-grosir" name="harga_grosir[]" placeholder="Harga Grosir" style="border-bottom-left-radius: unset; border-top-left-radius: unset;">
									</div>
									<div id="remove_gross" style="color: red; cursor: pointer; padding-top: 3px; user-select: unset">x</div>
								</div>`;
	$('.container-gross').append(newform);
  });

  $(document).on('blur', '.min-grosir', function(e){
	e.preventDefault();
	if(e.target.value === '1') {
		alert("Minimal Grosir Harus Lebih dari 1 ");
		$(e.target).val(2);
	}
  });

  $(document).on('click', '#remove_gross', function(e){
    resetGrosirError()
    // e.preventDefault();
    const length_gross = $('#harga_grosir .gross').length;
    if(length_gross > 1) {
      $(this).parents()[0].remove();
    } else {
      $('#status').click();
      $('.container-gross').html(``);
      $('.tambah_grosir').remove();
    }
  });

  $(document).on('click', '#status', function(e){
    if($('#varians').val() === '1') {
		e.preventDefault();
		alert('Tidak bisa mengaktifkan Harga Grosir jika Varian Aktif');
	} else {
    resetGrosirError()
	  if($(this).val() === '0') {
      $(this).val('1');
	  } else {
      $(this).val('0');
	  }
		if($(this).val() === '0') {
      $('.container-gross').html(``);
      $('.tambah_grosir').remove();
	  } else {
      $('#label_grosir').append(`<small class="tambah_grosir" style="float: right; cursor: pointer; color: #2449f0; user-select: none" id="tambah_harga_grosir">+Tambah Harga</small>`);
      $('.container-gross').html(`
        <div style="display: flex;margin-bottom: 10px" class="gross">
          <div class="col-5" style="display: flex; padding-right: unset">
            <div class="input-group-prepend">
              <span class="input-group-text paddmob10" style="border-bottom-right-radius: unset; border-top-right-radius: unset">≥ </span>
            </div>
            <input type="number" class="form-control number-input min-grosir" name="minimal_grosir[]" placeholder="Min" style="border-radius: unset">
          </div>
          <div class="col-7" style="display: flex; padding-left: unset">
            <div class="input-group-prepend">
              <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
            </div>
            <input type="number" class="form-control number-input harga-grosir" name="harga_grosir[]" placeholder="Harga Grosir" style="border-bottom-left-radius: unset; border-top-left-radius: unset;">
          </div>
          <div id="remove_gross" style="color: red; cursor: pointer; padding-top: 3px; user-select: unset">x</div>
        </div>`);
	  }
  }
  });

  $('#uom_check').on('click', function() {
		if ( $(this).is(':checked') == true ) {
			$('.tbl_uom').show();
		} else {
			$('.tbl_uom').hide();
		}
  });

  $(document).on('click', '#tambah_harga_varians', function(e){
	e.preventDefault();
	const newform = `<div style="display: flex;margin-bottom: 20px;" class="varians">
          <div class="col-12 row">
            <div class="col-12" style="display: flex;margin-bottom: 8px;width: 108%;max-width: 108%;flex: 0 0 108%;"> 
              <input type="text" class="form-control nama-varian" name="nama_varian[]" placeholder="Nama Varian">
            </div>
            <div class="col-5" style="display: flex; padding-right: unset;margin-bottom: 8px;">
              <input type="number" class="form-control number-input conv-varian" name="conv_varian[]" placeholder="Conversion" style="border-bottom-right-radius: unset; border-top-right-radius: unset">
            </div>
            <div class="col-7" style="display: flex; padding-left: unset">
              <div class="input-group-prepend" style="height: 32px;">
                <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
              </div>
              <input type="number" class="form-control number-input hpp-varian" name="hpp_varian[]" placeholder="HPP Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;width: 138px;max-width: 138px;">
            </div>
            <div class="col-5" style="display: flex; padding-right: unset">
              <input type="number" class="form-control number-input sku-varian" name="sku_varian[]" placeholder="SKU" style="border-bottom-right-radius: unset; border-top-right-radius: unset">
            </div>
            <div class="col-7" style="display: flex; padding-left: unset">
              <div class="input-group-prepend">
                <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
              </div>
              <input type="number" class="form-control number-input harga-varian" name="harga_varian[]" placeholder="Harga Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;width: 138px;max-width: 138px;">
            </div>
          </div>
          <div id="remove_varians" style="color: red; cursor: pointer; user-select: unset;align-self: center; margin-left: 25px;">x</div>
          <input type="hidden" name="varian_id[]" value="0">
        </div>`;
	$('.container-varians').append(newform);
  });

  $(document).on('click', '#remove_varians', function(e){
    e.preventDefault();
    const length_gross_varians = $('#harga_varians .varians').length;
    if(length_gross_varians > 1) {
      $(this).parents()[0].remove();
    } else {
      $('#varians').click();
      $('.container-varians').html(``);
      $('.tambah_varians').remove();
    }
  });
  
  $(document).on('click', '#varians', function(e){
    if($('#status').val() === '1') {
		e.preventDefault();
		alert('Tidak bisa mengaktifkan Varians jika Harga Grosir Aktif');
	} else {
    if($(this).val() === '0') {
      $(this).val('1');
      $('input[name=hpp]').addClass('noned').prop('readonly', true);
      $('input[name=harga]').addClass('noned').prop('readonly', true);
	  } else {
      $(this).val('0');
      $('input[name=hpp]').removeClass('noned').prop('readonly', false);
      $('input[name=harga]').removeClass('noned').prop('readonly', false);
	  }
		if($(this).val() === '0') {
      $('.container-varians').html(``);
      $('.tambah_varians').remove();
	  } else {
      $('#label_varians').append(`<small class="tambah_varians" style="float: right; cursor: pointer; color: #2449f0; user-select: none" id="tambah_harga_varians">+Tambah Varian</small>`);
      $('.container-varians').html(`
        <div style="display: flex;margin-bottom: 20px;" class="varians">
          <div class="col-12 row">
            <div class="col-12" style="display: flex;margin-bottom: 8px;width: 108%;max-width: 108%;flex: 0 0 108%;"> 
              <input type="text" class="form-control nama-varian" name="nama_varian[]" placeholder="Nama Varian">
            </div>
            <div class="col-5" style="display: flex; padding-right: unset;margin-bottom: 8px;">
              <input type="number" class="form-control number-input conv-varian" name="conv_varian[]" placeholder="Conversion" style="border-bottom-right-radius: unset; border-top-right-radius: unset">
            </div>
            <div class="col-7" style="display: flex; padding-left: unset">
              <div class="input-group-prepend" style="height: 32px;">
                <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
              </div>
              <input type="number" class="form-control number-input hpp-varian" name="hpp_varian[]" placeholder="HPP Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;width: 138px;max-width: 138px;">
            </div>
            <div class="col-5" style="display: flex; padding-right: unset">
              <input type="number" class="form-control number-input sku-varian" name="sku_varian[]" placeholder="SKU" style="border-bottom-right-radius: unset; border-top-right-radius: unset">
            </div>
            <div class="col-7" style="display: flex; padding-left: unset">
              <div class="input-group-prepend">
                <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
              </div>
              <input type="number" class="form-control number-input harga-varian" name="harga_varian[]" placeholder="Harga Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;width: 138px;max-width: 138px;">
            </div>
          </div>
          <div id="remove_varians" style="color: red; cursor: pointer; user-select: unset;align-self: center; margin-left: 25px;">x</div>
          <input type="hidden" name="varian_id[]" value="0">
        </div>`);
	  }
  }
  });
</script>
@endsection