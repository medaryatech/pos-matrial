@extends('templates/main')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css">
<link rel="stylesheet" href="{{ asset('css/manage_product/new_product/style.css') }}">
@endsection
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header d-flex justify-content-start align-items-center">
      <div class="quick-link-wrapper d-md-flex flex-md-wrap">
        <ul class="quick-links">
          <li><a href="{{ url('product') }}">Daftar Barang</a></li>
          <li><a href="{{ url('product/new') }}">Barang Baru</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="row modal-group">
  <div class="modal fade" id="scanModal" tabindex="-1" role="dialog" aria-labelledby="scanModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="scanModalLabel">Scan Barcode</h5>
	        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	          <div class="row">
	          	<div class="col-12 text-center" id="area-scan">
	          	</div>
	          	<div class="col-12 barcode-result" hidden="">
	          		<h5 class="font-weight-bold">Hasil</h5>
	          		<div class="form-border">
	          			<p class="barcode-result-text"></p>
	          		</div>
	          	</div>
	          </div>
	      </div>
	      <div class="modal-footer" id="btn-scan-action" hidden="">
	        <button type="button" class="btn btn-primary btn-sm font-weight-bold rounded-0 btn-continue">Lanjutkan</button>
	        <button type="button" class="btn btn-outline-secondary btn-sm font-weight-bold rounded-0 btn-repeat">Ulangi</button>
	      </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="formatModal" tabindex="-1" role="dialog" aria-labelledby="formatModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      	<div class="modal-header">
	        <h5 class="modal-title" id="formatModalLabel">Format Upload</h5>
	        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	    </div>
	    <div class="modal-body">
	    	<div class="row">
	    		<div class="col-12 img-import-area">
	    			@if($supply_system->status == true)
	    			<img src="{{ asset('images/instructions/ImportProduct.jpg') }}" class="img-import">
	    			@else
	    			<img src="{{ asset('images/instructions/ImportProduct2.jpg') }}" class="img-import">
	    			@endif
	    		</div>
	    	</div>
	    </div>
      </div>
	</div>
  </div>
</div>
<div class="row">
	<div class="col-lg-8 col-md-12 col-sm-12 mb-4">
		<div class="card card-noborder b-radius">
			<div class="card-body">
				<form action="{{ url('/product/create') }}" method="post" name="create_form" enctype="multipart/form-data">
					@csrf
					<div class="form-group row">
			  			<label class="col-12 font-weight-bold col-form-label">Kode Barang <span class="text-danger">*</span></label>
					  	<div class="col-12">
					  		<div class="input-group">
					  			<input type="number" class="form-control number-input" name="kode_barang" placeholder="Masukkan Kode Barang">
					  			<div class="inpu-group-prepend">
					  				<button class="btn btn-inverse-primary btn-sm btn-scan shadow-sm ml-2" type="button" data-toggle="modal" data-target="#scanModal"><i class="mdi mdi-crop-free"></i></button>
					  			</div>
					  		</div>
					  	</div>
						<div class="col-12 error-notice" id="kode_barang_error"></div>
					</div>
					<div class="form-group row">
					  	<div class="col-lg-6 col-md-6 col-sm-12 space-bottom">
					  		<div class="row">
					  			<label class="col-12 font-weight-bold col-form-label">Nama Barang <span class="text-danger">*</span></label>
							  	<div class="col-12">
							  		<input type="text" class="form-control" name="nama_barang" placeholder="Masukkan Nama Barang">
							  	</div>
								<div class="col-12 error-notice" id="nama_barang_error"></div>
					  		</div>
					  	</div>
					  	<div class="col-lg-6 col-md-6 col-sm-12">
					  		<div class="row">
					  			<label class="col-12 font-weight-bold col-form-label">Jenis Barang <span class="text-danger">*</span></label>
							  	<div class="col-12">
							  		<select class="form-control" name="jenis_barang">
							  			<option value="">-- Pilih Jenis Barang --</option>
							  			<option value="Produksi">Produksi</option>
							  			<option value="Konsumsi">Konsumsi</option>
							  		</select>
							  	</div>
								<div class="col-12 error-notice" id="jenis_barang_error"></div>
					  		</div>
					  	</div>
					</div>
					<div class="form-group row">
					  	<div class="col-lg-6 col-md-6 col-sm-12">
					  		<div class="row">
					  			<label class="col-12 font-weight-bold col-form-label">Merek Barang</label>
							  	<div class="col-12">
							  		<input type="text" class="form-control" name="merek" placeholder="Masukkan Merek Barang">
							  	</div>
					  		</div>
					  	</div>
						@if($supply_system->status == true)
							<div class="col-lg-6 col-md-6 col-sm-12">
								<div class="row">
									<label class="col-12 font-weight-bold col-form-label">Stok Barang <span class="text-danger">*</span></label>
									<div class="col-12">
										<input type="number" class="form-control number-input" name="stok" placeholder="Masukkan Stok Barang">
									</div>
									<div class="col-12 error-notice" id="stok_error"></div>
								</div>
							</div>
						@endif
					</div>
					<div class="form-group row">
						<div class="col-lg-6 col-md-6 col-sm-12 space-bottom">
							<div class="row">
								<label class="col-12 font-weight-bold col-form-label">Kategori Barang <span class="text-danger">*</span></label>
								<div class="col-12">
									<select class="form-control" name="kategori_id" id="kategori_id">
										@forelse ($category as $key => $val)
											<option value="{{ $val->id }}">{{ $val->nama }}</option>
										@empty
											<option value="" disabled selected>-- Kategori Kosong --</option>
										@endforelse
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 space-bottom" style="display: none">
							<div class="row" id="harga_grosir">
								<label class="col-12 font-weight-bold col-form-label" id="label_uom_check">Atur UOM Conversion
								    <div class="custom-checkbox">
								        <input id="uom_check" type="checkbox" name="isSetConversion" class="switch-mode">
								        <label for="uom_check">
								            <div class="status-switch" data-unchecked="Off" data-checked="On">
								            </div>
								        </label>
								    </div>
								</label>
						    </div>
							<div class="row tbl_uom">
								<div class="col">
									<table class="table table-responsive table-borderless">
										<tbody>
											<tr>
												<th>Tipe Satuan Barang</th>
												<td>
													<select class="form-control" name="tipe_satuan" id="tipe_satuan">
														<option value="pcs">Pcs</option>
														<option value="lembar">Lembar</option>
														<option value="zak">Zak</option>
													</select>
												</td>
											</tr>
											<tr>
												<th>Nilai UOM Sell</th>
												<td>
													<input type="number" name="value_uom_sell" class="form-control" id="value_uom_sell">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group row">
					  	<div class="col-lg-6 col-md-6 col-sm-12 space-bottom">
					  		<div class="row">
					  			<label class="col-12 font-weight-bold col-form-label">HPP <span class="text-danger">*</span></label>
							  	<div class="col-12">
							  		<div class="input-group">
							  			<input type="text" class="form-control idr" name="hpp" placeholder="Masukkan HPP">
							  		</div>
							  	</div>
								<div class="col-12 error-notice" id="hpp_error"></div>
					  		</div>
					  	</div>
						  <div class="col-lg-6 col-md-6 col-sm-12">
							<div class="row">
								<label class="col-12 font-weight-bold col-form-label">Harga Jual <span class="text-danger">*</span></label>
								<div class="col-12">
									<div class="input-group">
										<input type="text" class="form-control idr" name="harga" placeholder="Masukkan Harga Jual">
									</div>
								</div>
							  <div class="col-12 error-notice" id="harga_error"></div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						{{-- @if($supply_system->status == true)
							<div class="col-lg-6 col-md-6 col-sm-12 space-bottom" style="margin-bottom: 10px;">
								<div class="row">
									<label class="col-12 font-weight-bold col-form-label">Stok Barang <span class="text-danger">*</span></label>
									<div class="col-12">
										<input type="number" class="form-control number-input" name="stok" placeholder="Masukkan Stok Barang">
									</div>
									<div class="col-12 error-notice" id="stok_error"></div>
								</div>
							</div>
					  	@endif --}}
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="row" id="harga_grosir">
								<label class="col-12 font-weight-bold col-form-label" id="label_grosir">Harga Grosir
								    <div class="custom-checkbox">
								        <input id="status" type="checkbox" name="status" value="0" class="switch-mode">
								        <label for="status">
								            <div class="status-switch" data-unchecked="Off" data-checked="On">
								            </div>
								        </label>
								    </div>
								</label>
								<div class="container-gross">
									<div style="display: flex" class="gross">
									</div>
								</div>
								
						    </div>
							<label class="col-12 error-notice error" id="harga_grosir_error" style="padding: 0;margin-top: -7px !important;"></label>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="row" id="harga_varians">
								<label class="col-12 font-weight-bold col-form-label" id="label_varians">Varians
								    <div class="custom-checkbox">
								        <input id="varians" type="checkbox" name="varians" value="0" class="switch-mode">
								        <label for="varians">
								            <div class="status-switch" data-unchecked="Off" data-checked="On">
								            </div>
								        </label>
								    </div>
								</label>
								<div class="container-varians">
									<div style="display: flex" class="varians">
									</div>
								</div>
								
						    </div>
							<label class="col-12 error-notice error" id="harga_varians_error" style="padding: 0;margin-top: -7px !important;"></label>
						</div>
					</div>
					<div class="form-group row">
					  	<div class="col-lg-12 col-md-12 col-sm-12 space-bottom">
					  		<div class="row">
					  			<label class="col-12 font-weight-bold col-form-label">Gambar Barang</label>
							  	<div class="col-12">
							  		<input type="file" class="dropify" name="file_gambar" />
							  	</div>
								<div class="col-12 error-notice" id="file_gambar_error"></div>
					  		</div>
					  	</div>
					</div>
					<div class="row">
						<div class="col-12 mt-2 d-flex justify-content-end">
					  		<button class="btn btn-simpan btn-sm" type="submit"><i class="mdi mdi-content-save"></i> Simpan</button>
					  	</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-12 col-sm-12">
		<div class="row">
			<div class="col-12 stretch-card bg-dark-blue">
				<div class="card text-white card-noborder b-radius">
					<div class="card-body">
						<form action="{{ url('/product/import') }}" method="post" enctype="multipart/form-data">
							@csrf
							<div class="d-flex justify-content-between pb-2 align-items-center">
			                  <h2 class="font-weight-semibold mb-0">Import</h2>
			                  <input type="file" name="excel_file" hidden="" accept=".xls, .xlsx">
			                  <a href="#" class="excel-file">
			                  	<div class="icon-holder">
				                   <i class="mdi mdi-upload"></i>
				                </div>
			                  </a>
			                </div>
			                <div class="d-flex justify-content-between">
			                  <h5 class="font-weight-semibold mb-0">Upload file excel</h5>
			                  <p class="text-white excel-name">Pilih File</p>
			                </div>
			                <button class="btn btn-block mt-3 btn-upload" type="submit" hidden="">Import Data</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-12 mt-4">
				<div class="card card-noborder b-radius">
					<div class="card-body">
						<h4 class="card-title mb-1">Langkah - Langkah Import</h4>
	                    <div class="d-flex py-2 border-bottom">
	                      <div class="wrapper">
	                        <p class="font-weight-semibold text-gray mb-0">1. Siapkan data dengan format Excel (.xls atau .xlsx)</p>
	                        <small class="text-muted">
	                        	<a href="" role="button" class="link-how" data-toggle="modal" data-target="#formatModal">Selengkapnya</a>
	                    	</small>
	                      </div>
	                    </div>
	                    <div class="d-flex py-2 border-bottom">
	                      <div class="wrapper">
	                        <p class="font-weight-semibold text-gray mb-0">2. Jika sudah sesuai pilih file</p>
	                      </div>
	                    </div>
	                    <div class="d-flex py-2">
	                      <div class="wrapper">
	                        <p class="font-weight-semibold text-gray mb-0">3. Klik simpan, maka data otomatis tersimpan</p>
	                      </div>
	                    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script src="{{ asset('plugins/js/quagga.min.js') }}"></script>
<script src="{{ asset('js/manage_product/new_product/script.js') }}"></script>
<script type="text/javascript">
  @if ($message = Session::get('create_failed'))
    swal(
        "",
        "{{ $message }}",
        "error"
    );
  @endif

  @if ($message = Session::get('import_failed'))
    swal(
        "",
        "{{ $message }}",
        "error"
    );
  @endif

  $('.dropify').dropify({
	maxFileSize: '2M',
    messages: {
    	'default': 'Masukkan Gambar Barang',
    }
  });

  
  $(document).on('click', '#tambah_harga_grosir', function(e){
	e.preventDefault();
	const length = $('#harga_grosir .gross').length+1;
	const newform = `<div style="display: flex;margin-bottom: 10px" class="gross">
						<div class="col-5" style="display: flex; padding-right: unset">
							<div class="input-group-prepend">
								<span class="input-group-text paddmob10" style="border-bottom-right-radius: unset; border-top-right-radius: unset">≥ </span>
							</div>
							<input type="number" class="form-control number-input min-grosir" name="minimal_grosir[]" placeholder="Min" style="border-radius: unset">
						</div>
						<div class="col-7" style="display: flex; padding-left: unset">
							<div class="input-group-prepend">
								<span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
							</div>
							<input type="number" class="form-control number-input harga-grosir" name="harga_grosir[]" placeholder="Harga Grosir" style="border-bottom-left-radius: unset; border-top-left-radius: unset;">
						</div>
						<div id="remove_gross" style="color: red; cursor: pointer; padding-top: 3px; user-select: unset">x</div>
					</div>`;
	$('.container-gross').append(newform);
  });

  $(document).on('click', '#remove_gross', function(e){
	resetGrosirError()
	// e.preventDefault();
	const length_gross = $('#harga_grosir .gross').length;
	if(length_gross > 1) {
		$(this).parents()[0].remove();
	} else {
		$('#status').click();
		$('.container-gross').html(``);
		$('.tambah_grosir').remove();
	}
  });

  $(document).on('click', '#status', function(e){
	if($('#varians').val() === '1') {
		e.preventDefault();
		alert('Tidak bisa mengaktifkan Harga Grosir jika Varian Aktif');
	} else {
		resetGrosirError()
		if($(this).val() === '0') {
			$(this).val('1');
		} else {
			$(this).val('0');
		}
		if($(this).val() === '0') {
			$('.container-gross').html(``);
			$('.tambah_grosir').remove();
		} else {
			$('#label_grosir').append(`<small class="tambah_grosir" style="float: right; cursor: pointer; color: #2449f0; user-select: none" id="tambah_harga_grosir">+Tambah Harga</small>`);
			$('.container-gross').html(`
			<div style="display: flex;margin-bottom: 10px" class="gross">
				<div class="col-5" style="display: flex; padding-right: unset">
					<div class="input-group-prepend">
						<span class="input-group-text paddmob10" style="border-bottom-right-radius: unset; border-top-right-radius: unset">≥ </span>
					</div>
					<input type="number" class="form-control number-input min-grosir" name="minimal_grosir[]" placeholder="Min" style="border-radius: unset">
				</div>
				<div class="col-7" style="display: flex; padding-left: unset">
					<div class="input-group-prepend">
						<span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
					</div>
					<input type="number" class="form-control number-input harga-grosir" name="harga_grosir[]" placeholder="Harga Grosir" style="border-bottom-left-radius: unset; border-top-left-radius: unset;">
				</div>
				<div id="remove_gross" style="color: red; cursor: pointer; padding-top: 3px; user-select: unset">x</div>
			</div>`);
		}
	}
  });

  $(document).on('blur', '.min-grosir', function(e){
	e.preventDefault();
	if(e.target.value === '1') {
		alert("Minimal Grosir Harus Lebih dari 1 ");
		$(e.target).val(2);
	}
  });
  
  $('.tbl_uom').hide();
    $('#uom_check').on('click', function() {
		if ( $(this).is(':checked') == true ) {
			$('.tbl_uom').show();
		} else {
			$('.tbl_uom').hide();
		}
  	});

	  $(document).on('click', '#tambah_harga_varians', function(e){
	e.preventDefault();
	const newform = `<div style="display: flex;margin-bottom: 20px;" class="varians">
          <div class="col-12 row">
            <div class="col-12" style="display: flex;margin-bottom: 8px;"> 
              <input type="text" class="form-control nama-varian" name="nama_varian[]" placeholder="Nama Varian">
            </div>
            <div class="col-5" style="display: flex; padding-right: unset;margin-bottom: 8px;">
              <input type="number" class="form-control number-input conv-varian" name="conv_varian[]" placeholder="Conversion" style="border-bottom-right-radius: unset; border-top-right-radius: unset">
            </div>
            <div class="col-7" style="display: flex; padding-left: unset">
              <div class="input-group-prepend" style="height: 32px;">
                <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
              </div>
              <input type="number" class="form-control number-input hpp-varian" name="hpp_varian[]" placeholder="HPP Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;">
            </div>
            <div class="col-5" style="display: flex; padding-right: unset">
              <input type="number" class="form-control number-input sku-varian" name="sku_varian[]" placeholder="SKU" style="border-bottom-right-radius: unset; border-top-right-radius: unset">
            </div>
            <div class="col-7" style="display: flex; padding-left: unset">
              <div class="input-group-prepend">
                <span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
              </div>
              <input type="number" class="form-control number-input harga-varian" name="harga_varian[]" placeholder="Harga Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;">
            </div>
          </div>
          <div id="remove_varians" style="color: red; cursor: pointer; user-select: unset;align-self: center;">x</div>
          <input type="hidden" name="varian_id[]" value="0">
        </div>`;
	$('.container-varians').append(newform);
  });

  $(document).on('click', '#remove_varians', function(e){
    e.preventDefault();
    const length_gross_varians = $('#harga_varians .varians').length;
    if(length_gross_varians > 1) {
      $(this).parents()[0].remove();
    } else {
      $('#varians').click();
      $('.container-varians').html(``);
      $('.tambah_varians').remove();
    }
  });
  
  $(document).on('click', '#varians', function(e){
	if($('#status').val() === '1') {
		e.preventDefault();
		alert('Tidak bisa mengaktifkan Varians jika Harga Grosir Aktif');
	} else {
		if($(this).val() === '0') {
		$(this).val('1');
		$('input[name=hpp]').val(0);
		$('input[name=harga]').val(0);
		$('input[name=hpp]').addClass('noned').prop('readonly', true);
		$('input[name=harga]').addClass('noned').prop('readonly', true);
		} else {
		$(this).val('0');
		$('input[name=hpp]').val('');
		$('input[name=harga]').val('');
		$('input[name=hpp]').removeClass('noned').prop('readonly', false);
		$('input[name=harga]').removeClass('noned').prop('readonly', false);
		}
			if($(this).val() === '0') {
		$('.container-varians').html(``);
		$('.tambah_varians').remove();
		} else {
		$('#label_varians').append(`<small class="tambah_varians" style="float: right; cursor: pointer; color: #2449f0; user-select: none" id="tambah_harga_varians">+Tambah Varian</small>`);
		$('.container-varians').html(`
			<div style="display: flex;margin-bottom: 20px;" class="varians">
			<div class="col-12 row">
				<div class="col-12" style="display: flex;margin-bottom: 8px;"> 
				<input type="text" class="form-control nama-varian" name="nama_varian[]" placeholder="Nama Varian">
				</div>
				<div class="col-5" style="display: flex; padding-right: unset;margin-bottom: 8px;">
				<input type="number" class="form-control number-input conv-varian" name="conv_varian[]" placeholder="Conversion" style="border-bottom-right-radius: unset; border-top-right-radius: unset">
				</div>
				<div class="col-7" style="display: flex; padding-left: unset">
				<div class="input-group-prepend" style="height: 32px;">
					<span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
				</div>
				<input type="number" class="form-control number-input hpp-varian" name="hpp_varian[]" placeholder="HPP Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;">
				</div>
				<div class="col-5" style="display: flex; padding-right: unset">
				<input type="number" class="form-control number-input sku-varian" name="sku_varian[]" placeholder="SKU" style="border-bottom-right-radius: unset; border-top-right-radius: unset">
				</div>
				<div class="col-7" style="display: flex; padding-left: unset">
				<div class="input-group-prepend">
					<span class="input-group-text paddmob10" style="border-left: unset;border-radius: unset">Rp. </span>
				</div>
				<input type="number" class="form-control number-input harga-varian" name="harga_varian[]" placeholder="Harga Varian" style="border-bottom-left-radius: unset; border-top-left-radius: unset;">
				</div>
			</div>
			<div id="remove_varians" style="color: red; cursor: pointer; user-select: unset;align-self: center;">x</div>
			<input type="hidden" name="varian_id[]" value="0">
			</div>`);
		}
	}
  });
</script>
@endsection