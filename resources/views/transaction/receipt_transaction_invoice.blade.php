<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		*{
			font-family: Arial, sans-serif;
		}

		.center{
			text-align: center;
		}

		.right{
			text-align: right;
		}

		.left{
			text-align: left;
		}

		p{
			font-size: 10px;
		}

		.top-min{
			margin-top: -10px;
		}

		.uppercase{
			text-transform: uppercase;
		}

		.bold{
			font-weight: bold;
		}

		.d-block{
			display: block;
		}

		hr{
			border: 0;
			border-top: 1px solid #000;
		}

		.hr-dash{
			border-style: solid none none none;
		}

		table{
			font-size: 12px;
		}

		table thead tr td{
			padding: 5px;
		}

		.w-100{
			width: 100%;
		}

		.line{
			border: 0;
			border-top: 1px solid #000;
			border-style: solid none none none;
		}

		.body{
			margin-top: -10px;
		}

		.b-p{
			font-size: 12px !important;
		}

		.w-long{
			width: 80px;
		}
	</style>
</head>
<body>
	@if(count($transactions) <= 10)
	<div class="header">
		<div style="width: 100%; padding: 0; margin: 0;height: 50px; margin-top: -10px">
			{{-- <div style="display: inline-block; width: 7%; padding: 0; margin: 0;">
				<img src="./images/cmi_bw.png" style="width: 100%">
				<span style="height: 4px; display: block"></span>
			</div> --}}
			<div style="display: inline-block; width: 53%; padding: 0; margin: 0;">
				<p class="uppercase bold" style="white-space: nowrap;">{{ $market->nama_toko }}</p>
				<p class="top-min d-block">{{ $market->alamat }}</p>
				<p class="top-min d-block">{{ $market->no_telp }}</p>
			</div>
			<div style="display: inline-block; width: 47%; padding: 0; margin: 0; padding-left: 15px; text-align: right;">
				<p class="uppercase bold" style="font-size: 20px !important; margin-top: 5px; padding-right: 25px">INVOICE</p>
			</div>
		</div>
		<table class="w-100">
			<tr>
				<td class="left" style="width: 43px">Kasir : </td>
				<td class="left">{{ $transaction->kasir }}</td>
				<td class="right">Cust : </td>
				<td class="right">{{ $transaction->pelanggan }}</td>
			</tr>
			<tr>
				<td class="left" style="width: 43px">Kode : </td>
				<td class="left">{{ $transaction->kode_transaksi }}</td>
				<td class="right">Tanggal : </td>
				<td class="right" style="width: 115px">{{ date('d M, Y', strtotime($transaction->created_at)) . ' ' . date('H:i', strtotime($transaction->created_at)) }}</td>
			</tr>
		</table>
		<hr class="hr-dash">
	</div>
	<div class="body">
		@php
		$counttotalbarang = 0;
		@endphp
		<table class="w-100" style="border-collapse: collapse;margin: 0; padding: 0">
			<thead>
				{{-- <tr>
					<td>Nama Barang</td>
					<td style="text-align: right">Qty</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr> --}}
				<tr>
					<td style="font-weight: bold">Nama Barang</td>
					<td style="font-weight: bold">Qty</td>
					<td style="text-align: right; font-weight: bold">Harga</td>
					<td style="text-align: right; font-weight: bold">Jumlah</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaksi)
				@php
				$counttotalbarang += $transaksi->jumlah;
				@endphp
				{{-- <tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td style="text-align: right">{{ $transaksi->jumlah }}&nbsp;&nbsp;</td>
				</tr> --}}
				<tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td>{{ $transaksi->jumlah }} pcs</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->harga,2,',','.') }}</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->total_barang,2,',','.') }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<hr class="hr-dash">
		<table class="w-100" style="border-collapse: collapse;margin-bottom: 7px">
			<tr>
				<td class="left">Subtotal (Jumlah : {{ $counttotalbarang }})</td>
				<td class="right">{{ number_format($transaction->subtotal,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Diskon ({{ $transaction->diskon }}%)</td>
				<td class="right">{{ number_format($diskon?'-'.$diskon:$diskon,2,',','.') }}</td>
			</tr>
			@if($transaction->add_val)
			<tr>
				<td class="left">{{ $transaction->add_val }}</td>
				<td class="right">{{ number_format($transaction->value_add_val,2,',','.') }}</td>
			</tr>
			@endif
			<tr>
				<td class="left" style="font-weight: bold;padding: 0; margin: 0">Total</td>
				<td class="right" style="font-weight: bold;padding: 0; margin: 0">{{ number_format($transaction->total,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left"></td>
				<td class="right"></td>
			</tr>
			<tr>
				<td class="left" style="font-weight: bold;padding: 0; margin: 0">Pembayaran<style>
					.clsd {
					  padding-left: 4px !important;
					  padding-right: 2px !important;
					  padding-bottom: 1px !important;
					  border: 1px solid #495ff1;
					  border-left: 1px solid #495ff1 !important;
					  font-size: 12px;
					  color: #495ff1;
					  font-weight: bold;
					}
					.clsd.po {
					  border-color: #eb5454;
					  color: #eb5454;
					}
				  </style>
				  @if($transaction->po === 1)
				  <span class="clsd po">PO</span>
				  @elseif($transaction->po === 2)
				  <span class="clsd">CASH</span>
				  @else
				  <span class="clsd">TRANSFER</span>
				  @endif</td>
				<td class="right" style="font-weight: bold;padding: 0; margin: 0">{{ number_format($transaction->bayar,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Kembali</td>
				<td class="right">{{ number_format($transaction->kembali,2,',','.') }}</td>
			</tr>
		</table>
		{{-- <table style="border-collapse: collapse;margin-top: 30px;width: 100%; margin-bottom: 5px; border-bottom: 1px solid">
			<tr>
				<td><b>Rekening Perusahan</b></td>
			</tr>
			<tr>
				<td style="width: 42%">NAMA BANK:</td>
				<td style="width: 33%">NOMOR REKENING:</td>
				<td style="width: 25%">NAMA REKENING:</td>
			</tr>
			<tr>
				<td style="width: 42%;">BANK CENTRAL ASIA</td>
				<td style="width: 33%;">8691990443</td>
				<td style="width: 25%;">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
			<tr>
				<td style="width: 42%; padding-bottom: 9px">BANK MANDIRI</td>
				<td style="width: 33%; padding-bottom: 9px">1570003487387</td>
				<td style="width: 25%; padding-bottom: 9px">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
		</table>
		<table style="border-collapse: collapse;width: 100%; font-weight: bold;margin-top: 9px">
			<tr>
				<td style="font-size: 9.5px; text-align: center">
				SIMA BANGUNAN PRODUK TERLENGKAP DENGAN HARGA TERBAIK | MENJUAL BERBAGAI KEBUTUHAN BANGUNAN RUMAH | BERDIRI SEJAK 1988
			</td>
			</tr>
		</table>
		<div style="font-size: 14px; width: 100%;text-align: center; margin-top: 3px; font-weight: bold">BEST CHOICE BUILDING MATERIAL</div>
		<div style="font-size: 9px; width: 100%;text-align: center; margin-top: 1px">www.simabangunan.com</div> --}}
	</div>
	@elseif (count($transactions) > 10 && count($transactions) <= 20)
	@php
	$counttotalbarang1 = 0;
	$counttotalbarang2 = 0;
	$countjumlahbarang1 = 0;
	$countjumlahbarang2 = 0;
	@endphp
	<div class="header">
		<div style="width: 100%; padding: 0; margin: 0;height: 50px; margin-top: -10px">
			{{-- <div style="display: inline-block; width: 7%; padding: 0; margin: 0;">
				<img src="./images/cmi_bw.png" style="width: 100%">
				<span style="height: 4px; display: block"></span>
			</div> --}}
			<div style="display: inline-block; width: 53%; padding: 0; margin: 0;">
				<p class="uppercase bold" style="white-space: nowrap;">{{ $market->nama_toko }}</p>
				<p class="top-min d-block">{{ $market->alamat }}</p>
				<p class="top-min d-block">{{ $market->no_telp }}</p>
			</div>
			<div style="display: inline-block; width: 47%; padding: 0; margin: 0; padding-left: 15px; text-align: right;">
				<p class="uppercase bold" style="font-size: 20px; margin-top: 5px; padding-right: 25px">INVOICE <small style="font-size: 14px !imporant;">1/2</small></p>
			</div>
		</div>
		<table class="w-100">
			<tr>
				<td class="left" style="width: 43px">Kasir : </td>
				<td class="left">{{ $transaction->kasir }}</td>
				<td class="right">Cust : </td>
				<td class="right">{{ $transaction->pelanggan }}</td>
			</tr>
			<tr>
				<td class="left" style="width: 43px">Kode : </td>
				<td class="left">{{ $transaction->kode_transaksi }}</td>
				<td class="right">Tanggal : </td>
				<td class="right" style="width: 115px">{{ date('d M, Y', strtotime($transaction->created_at)) . ' ' . date('H:i', strtotime($transaction->created_at)) }}</td>
			</tr>
		</table>
		<hr class="hr-dash">
	</div>
	<div class="body">
		<table class="w-100" style="border-collapse: collapse;margin: 0; padding: 0">
			<thead>
				{{-- <tr>
					<td>Nama Barang</td>
					<td style="text-align: right">Qty</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr> --}}
				<tr>
					<td style="font-weight: bold">Nama Barang</td>
					<td style="font-weight: bold">Qty</td>
					<td style="text-align: right; font-weight: bold">Harga</td>
					<td style="text-align: right; font-weight: bold">Jumlah</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaksi)
				@if(($loop->index+1) <= 10)
				@php
				$counttotalbarang1 += $transaksi->jumlah;
				$countjumlahbarang1 += $transaksi->total_barang;
				@endphp
				{{-- <tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td style="text-align: right">{{ $transaksi->jumlah }}&nbsp;&nbsp;</td>
				</tr> --}}
				<tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td>{{ $transaksi->jumlah }} pcs</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->harga,2,',','.') }}</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->total_barang,2,',','.') }}</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
		<hr class="hr-dash">
		<table class="w-100" style="border-collapse: collapse;margin-bottom: 7px">
			<tr>
				<td class="left">Invoice 1/2 (Jumlah : {{ $counttotalbarang1 }})</td>
				<td class="right">{{ number_format($countjumlahbarang1,2,',','.') }}</td>
			</tr>
		</table>
		{{-- <table style="border-collapse: collapse;margin-top: 30px;width: 100%; margin-bottom: 5px; border-bottom: 1px solid">
			<tr>
				<td><b>Rekening Perusahan</b></td>
			</tr>
			<tr>
				<td style="width: 42%">NAMA BANK:</td>
				<td style="width: 33%">NOMOR REKENING:</td>
				<td style="width: 25%">NAMA REKENING:</td>
			</tr>
			<tr>
				<td style="width: 42%;">BANK CENTRAL ASIA</td>
				<td style="width: 33%;">8691990443</td>
				<td style="width: 25%;">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
			<tr>
				<td style="width: 42%; padding-bottom: 9px">BANK MANDIRI</td>
				<td style="width: 33%; padding-bottom: 9px">1570003487387</td>
				<td style="width: 25%; padding-bottom: 9px">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
		</table>
		<table style="border-collapse: collapse;width: 100%; font-weight: bold;margin-top: 9px">
			<tr>
				<td style="font-size: 9.5px; text-align: center">
				SIMA BANGUNAN PRODUK TERLENGKAP DENGAN HARGA TERBAIK | MENJUAL BERBAGAI KEBUTUHAN BANGUNAN RUMAH | BERDIRI SEJAK 1988
			</td>
			</tr>
		</table>
		<div style="font-size: 14px; width: 100%;text-align: center; margin-top: 3px; font-weight: bold">BEST CHOICE BUILDING MATERIAL</div>
		<div style="font-size: 9px; width: 100%;text-align: center; margin-top: 1px">www.simabangunan.com</div> --}}
	</div>
	<div class="header" style="margin-top: 200px">
		<div style="width: 100%; padding: 0; margin: 0;height: 50px; margin-top: -10px">
			{{-- <div style="display: inline-block; width: 7%; padding: 0; margin: 0;">
				<img src="./images/cmi_bw.png" style="width: 100%">
				<span style="height: 4px; display: block"></span>
			</div> --}}
			<div style="display: inline-block; width: 53%; padding: 0; margin: 0;">
				<p class="uppercase bold" style="white-space: nowrap;">{{ $market->nama_toko }}</p>
				<p class="top-min d-block">{{ $market->alamat }}</p>
				<p class="top-min d-block">{{ $market->no_telp }}</p>
			</div>
			<div style="display: inline-block; width: 47%; padding: 0; margin: 0; padding-left: 15px; text-align: right;">
				<p class="uppercase bold" style="font-size: 20px; margin-top: 5px; padding-right: 25px">INVOICE <small style="font-size: 14px !imporant;">2/2</small></p>
			</div>
		</div>
		<table class="w-100">
			<tr>
				<td class="left" style="width: 43px">Kasir : </td>
				<td class="left">{{ $transaction->kasir }}</td>
				<td class="right">Cust : </td>
				<td class="right">{{ $transaction->pelanggan }}</td>
			</tr>
			<tr>
				<td class="left" style="width: 43px">Kode : </td>
				<td class="left">{{ $transaction->kode_transaksi }}</td>
				<td class="right">Tanggal : </td>
				<td class="right" style="width: 115px">{{ date('d M, Y', strtotime($transaction->created_at)) . ' ' . date('H:i', strtotime($transaction->created_at)) }}</td>
			</tr>
		</table>
		<hr class="hr-dash">
	</div>
	<div class="body">
		<table class="w-100" style="border-collapse: collapse;margin: 0; padding: 0">
			<thead>
				{{-- <tr>
					<td>Nama Barang</td>
					<td style="text-align: right">Qty</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr> --}}
				<tr>
					<td style="font-weight: bold">Nama Barang</td>
					<td style="font-weight: bold">Qty</td>
					<td style="text-align: right; font-weight: bold">Harga</td>
					<td style="text-align: right; font-weight: bold">Jumlah</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaksi)
				@if(($loop->index+1) > 10 && ($loop->index+1) <= 20)
				@php
				$counttotalbarang2 += $transaksi->jumlah;
				$countjumlahbarang2 += $transaksi->total_barang;
				@endphp
				{{-- <tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td style="text-align: right">{{ $transaksi->jumlah }}&nbsp;&nbsp;</td>
				</tr> --}}
				<tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td>{{ $transaksi->jumlah }} pcs</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->harga,2,',','.') }}</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->total_barang,2,',','.') }}</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
		<hr class="hr-dash">
		<table class="w-100" style="border-collapse: collapse;margin-bottom: 7px">
			<tr>
				<td class="left" style="padding-bottom: 20px">Invoice 2/2 (Jumlah : {{ $counttotalbarang2 }})</td>
				<td class="right" style="padding-bottom: 20px">{{ number_format($countjumlahbarang2,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Subtotal (Jumlah : {{ $counttotalbarang1 + $counttotalbarang2 }})</td>
				<td class="right">{{ number_format($transaction->subtotal,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Diskon ({{ $transaction->diskon }}%)</td>
				<td class="right">{{ number_format($diskon?'-'.$diskon:$diskon,2,',','.') }}</td>
			</tr>
			@if($transaction->add_val)
			<tr>
				<td class="left">{{ $transaction->add_val }}</td>
				<td class="right">{{ number_format($transaction->value_add_val,2,',','.') }}</td>
			</tr>
			@endif
			<tr>
				<td class="left" style="font-weight: bold;padding: 0; margin: 0">Total</td>
				<td class="right" style="font-weight: bold;padding: 0; margin: 0">{{ number_format($transaction->total,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left"></td>
				<td class="right"></td>
			</tr>
			<tr>
				<td class="left" style="font-weight: bold;padding: 0; margin: 0">Pembayaran<style>
					.clsd {
					  padding-left: 4px !important;
					  padding-right: 2px !important;
					  padding-bottom: 1px !important;
					  border: 1px solid #495ff1;
					  border-left: 1px solid #495ff1 !important;
					  font-size: 12px;
					  color: #495ff1;
					  font-weight: bold;
					}
					.clsd.po {
					  border-color: #eb5454;
					  color: #eb5454;
					}
				  </style>
				  @if($transaction->po === 1)
				  <span class="clsd po">PO</span>
				  @elseif($transaction->po === 2)
				  <span class="clsd">CASH</span>
				  @else
				  <span class="clsd">TRANSFER</span>
				  @endif</td>
				<td class="right" style="font-weight: bold;padding: 0; margin: 0">{{ number_format($transaction->bayar,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Kembali</td>
				<td class="right">{{ number_format($transaction->kembali,2,',','.') }}</td>
			</tr>
		</table>
		{{-- <table style="border-collapse: collapse;margin-top: 30px;width: 100%; margin-bottom: 5px; border-bottom: 1px solid">
			<tr>
				<td><b>Rekening Perusahan</b></td>
			</tr>
			<tr>
				<td style="width: 42%">NAMA BANK:</td>
				<td style="width: 33%">NOMOR REKENING:</td>
				<td style="width: 25%">NAMA REKENING:</td>
			</tr>
			<tr>
				<td style="width: 42%;">BANK CENTRAL ASIA</td>
				<td style="width: 33%;">8691990443</td>
				<td style="width: 25%;">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
			<tr>
				<td style="width: 42%; padding-bottom: 9px">BANK MANDIRI</td>
				<td style="width: 33%; padding-bottom: 9px">1570003487387</td>
				<td style="width: 25%; padding-bottom: 9px">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
		</table>
		<table style="border-collapse: collapse;width: 100%; font-weight: bold;margin-top: 9px">
			<tr>
				<td style="font-size: 9.5px; text-align: center">
				SIMA BANGUNAN PRODUK TERLENGKAP DENGAN HARGA TERBAIK | MENJUAL BERBAGAI KEBUTUHAN BANGUNAN RUMAH | BERDIRI SEJAK 1988
			</td>
			</tr>
		</table>
		<div style="font-size: 14px; width: 100%;text-align: center; margin-top: 3px; font-weight: bold">BEST CHOICE BUILDING MATERIAL</div>
		<div style="font-size: 9px; width: 100%;text-align: center; margin-top: 1px">www.simabangunan.com</div> --}}
	</div>
	@elseif (count($transactions) > 20 && count($transactions) <= 30)
	@php
	$counttotalbarang1 = 0;
	$counttotalbarang2 = 0;
	$counttotalbarang3 = 0;
	$countjumlahbarang1 = 0;
	$countjumlahbarang2 = 0;
	$countjumlahbarang3 = 0;
	@endphp
	<div class="header">
		<div style="width: 100%; padding: 0; margin: 0;height: 50px; margin-top: -10px">
			{{-- <div style="display: inline-block; width: 7%; padding: 0; margin: 0;">
				<img src="./images/cmi_bw.png" style="width: 100%">
				<span style="height: 4px; display: block"></span>
			</div> --}}
			<div style="display: inline-block; width: 53%; padding: 0; margin: 0;">
				<p class="uppercase bold" style="white-space: nowrap;">{{ $market->nama_toko }}</p>
				<p class="top-min d-block">{{ $market->alamat }}</p>
				<p class="top-min d-block">{{ $market->no_telp }}</p>
			</div>
			<div style="display: inline-block; width: 47%; padding: 0; margin: 0; padding-left: 15px; text-align: right;">
				<p class="uppercase bold" style="font-size: 20px; margin-top: 5px; padding-right: 25px">INVOICE <small style="font-size: 14px !imporant;">1/3</small></p>
			</div>
		</div>
		<table class="w-100">
			<tr>
				<td class="left" style="width: 43px">Kasir : </td>
				<td class="left">{{ $transaction->kasir }}</td>
				<td class="right">Cust : </td>
				<td class="right">{{ $transaction->pelanggan }}</td>
			</tr>
			<tr>
				<td class="left" style="width: 43px">Kode : </td>
				<td class="left">{{ $transaction->kode_transaksi }}</td>
				<td class="right">Tanggal : </td>
				<td class="right" style="width: 115px">{{ date('d M, Y', strtotime($transaction->created_at)) . ' ' . date('H:i', strtotime($transaction->created_at)) }}</td>
			</tr>
		</table>
		<hr class="hr-dash">
	</div>
	<div class="body">
		<table class="w-100" style="border-collapse: collapse;margin: 0; padding: 0">
			<thead>
				{{-- <tr>
					<td>Nama Barang</td>
					<td style="text-align: right">Qty</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr> --}}
				<tr>
					<td style="font-weight: bold">Nama Barang</td>
					<td style="font-weight: bold">Qty</td>
					<td style="text-align: right; font-weight: bold">Harga</td>
					<td style="text-align: right; font-weight: bold">Jumlah</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaksi)
				@if(($loop->index+1) <= 10)
				@php
				$counttotalbarang1 += $transaksi->jumlah;
				$countjumlahbarang1 += $transaksi->total_barang;
				@endphp
				{{-- <tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td style="text-align: right">{{ $transaksi->jumlah }}&nbsp;&nbsp;</td>
				</tr> --}}
				<tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td>{{ $transaksi->jumlah }} pcs</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->harga,2,',','.') }}</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->total_barang,2,',','.') }}</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
		<hr class="hr-dash">
		<table class="w-100" style="border-collapse: collapse;margin-bottom: 7px">
			<tr>
				<td class="left">Invoice 1/3 (Jumlah : {{ $counttotalbarang1 }})</td>
				<td class="right">{{ number_format($countjumlahbarang1,2,',','.') }}</td>
			</tr>
		</table>
		{{-- <table style="border-collapse: collapse;margin-top: 30px;width: 100%; margin-bottom: 5px; border-bottom: 1px solid">
			<tr>
				<td><b>Rekening Perusahan</b></td>
			</tr>
			<tr>
				<td style="width: 42%">NAMA BANK:</td>
				<td style="width: 33%">NOMOR REKENING:</td>
				<td style="width: 25%">NAMA REKENING:</td>
			</tr>
			<tr>
				<td style="width: 42%;">BANK CENTRAL ASIA</td>
				<td style="width: 33%;">8691990443</td>
				<td style="width: 25%;">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
			<tr>
				<td style="width: 42%; padding-bottom: 9px">BANK MANDIRI</td>
				<td style="width: 33%; padding-bottom: 9px">1570003487387</td>
				<td style="width: 25%; padding-bottom: 9px">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
		</table>
		<table style="border-collapse: collapse;width: 100%; font-weight: bold;margin-top: 9px">
			<tr>
				<td style="font-size: 9.5px; text-align: center">
				SIMA BANGUNAN PRODUK TERLENGKAP DENGAN HARGA TERBAIK | MENJUAL BERBAGAI KEBUTUHAN BANGUNAN RUMAH | BERDIRI SEJAK 1988
			</td>
			</tr>
		</table>
		<div style="font-size: 14px; width: 100%;text-align: center; margin-top: 3px; font-weight: bold">BEST CHOICE BUILDING MATERIAL</div>
		<div style="font-size: 9px; width: 100%;text-align: center; margin-top: 1px">www.simabangunan.com</div> --}}
	</div>
	<div class="header" style="margin-top: 200px">
		<div style="width: 100%; padding: 0; margin: 0;height: 50px; margin-top: -10px">
			{{-- <div style="display: inline-block; width: 7%; padding: 0; margin: 0;">
				<img src="./images/cmi_bw.png" style="width: 100%">
				<span style="height: 4px; display: block"></span>
			</div> --}}
			<div style="display: inline-block; width: 53%; padding: 0; margin: 0;">
				<p class="uppercase bold" style="white-space: nowrap;">{{ $market->nama_toko }}</p>
				<p class="top-min d-block">{{ $market->alamat }}</p>
				<p class="top-min d-block">{{ $market->no_telp }}</p>
			</div>
			<div style="display: inline-block; width: 47%; padding: 0; margin: 0; padding-left: 15px; text-align: right;">
				<p class="uppercase bold" style="font-size: 20px; margin-top: 5px; padding-right: 25px">INVOICE <small style="font-size: 14px !imporant;">2/3</small></p>
			</div>
		</div>
		<table class="w-100">
			<tr>
				<td class="left" style="width: 43px">Kasir : </td>
				<td class="left">{{ $transaction->kasir }}</td>
				<td class="right">Cust : </td>
				<td class="right">{{ $transaction->pelanggan }}</td>
			</tr>
			<tr>
				<td class="left" style="width: 43px">Kode : </td>
				<td class="left">{{ $transaction->kode_transaksi }}</td>
				<td class="right">Tanggal : </td>
				<td class="right" style="width: 115px">{{ date('d M, Y', strtotime($transaction->created_at)) . ' ' . date('H:i', strtotime($transaction->created_at)) }}</td>
			</tr>
		</table>
		<hr class="hr-dash">
	</div>
	<div class="body">
		<table class="w-100" style="border-collapse: collapse;margin: 0; padding: 0">
			<thead>
				{{-- <tr>
					<td>Nama Barang</td>
					<td style="text-align: right">Qty</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr> --}}
				<tr>
					<td style="font-weight: bold">Nama Barang</td>
					<td style="font-weight: bold">Qty</td>
					<td style="text-align: right; font-weight: bold">Harga</td>
					<td style="text-align: right; font-weight: bold">Jumlah</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaksi)
				@if(($loop->index+1) > 10 && ($loop->index+1) <= 20)
				@php
				$counttotalbarang2 += $transaksi->jumlah;
				$countjumlahbarang2 += $transaksi->total_barang;
				@endphp
				{{-- <tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td style="text-align: right">{{ $transaksi->jumlah }}&nbsp;&nbsp;</td>
				</tr> --}}
				<tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td>{{ $transaksi->jumlah }} pcs</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->harga,2,',','.') }}</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->total_barang,2,',','.') }}</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
		<hr class="hr-dash">
		<table class="w-100" style="border-collapse: collapse;margin-bottom: 7px">
			<tr>
				<td class="left" style="padding-bottom: 20px">Invoice 2/3 (Jumlah : {{ $counttotalbarang2 }})</td>
				<td class="right" style="padding-bottom: 20px">{{ number_format($countjumlahbarang2,2,',','.') }}</td>
			</tr>
		</table>
		{{-- <table style="border-collapse: collapse;margin-top: 30px;width: 100%; margin-bottom: 5px; border-bottom: 1px solid">
			<tr>
				<td><b>Rekening Perusahan</b></td>
			</tr>
			<tr>
				<td style="width: 42%">NAMA BANK:</td>
				<td style="width: 33%">NOMOR REKENING:</td>
				<td style="width: 25%">NAMA REKENING:</td>
			</tr>
			<tr>
				<td style="width: 42%;">BANK CENTRAL ASIA</td>
				<td style="width: 33%;">8691990443</td>
				<td style="width: 25%;">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
			<tr>
				<td style="width: 42%; padding-bottom: 9px">BANK MANDIRI</td>
				<td style="width: 33%; padding-bottom: 9px">1570003487387</td>
				<td style="width: 25%; padding-bottom: 9px">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
		</table>
		<table style="border-collapse: collapse;width: 100%; font-weight: bold;margin-top: 9px">
			<tr>
				<td style="font-size: 9.5px; text-align: center">
				SIMA BANGUNAN PRODUK TERLENGKAP DENGAN HARGA TERBAIK | MENJUAL BERBAGAI KEBUTUHAN BANGUNAN RUMAH | BERDIRI SEJAK 1988
			</td>
			</tr>
		</table>
		<div style="font-size: 14px; width: 100%;text-align: center; margin-top: 3px; font-weight: bold">BEST CHOICE BUILDING MATERIAL</div>
		<div style="font-size: 9px; width: 100%;text-align: center; margin-top: 1px">www.simabangunan.com</div> --}}
	</div>
	<div class="header" style="margin-top: 200px">
		<div style="width: 100%; padding: 0; margin: 0;height: 50px; margin-top: -10px">
			{{-- <div style="display: inline-block; width: 7%; padding: 0; margin: 0;">
				<img src="./images/cmi_bw.png" style="width: 100%">
				<span style="height: 4px; display: block"></span>
			</div> --}}
			<div style="display: inline-block; width: 53%; padding: 0; margin: 0;">
				<p class="uppercase bold" style="white-space: nowrap;">{{ $market->nama_toko }}</p>
				<p class="top-min d-block">{{ $market->alamat }}</p>
				<p class="top-min d-block">{{ $market->no_telp }}</p>
			</div>
			<div style="display: inline-block; width: 47%; padding: 0; margin: 0; padding-left: 15px; text-align: right;">
				<p class="uppercase bold" style="font-size: 20px; margin-top: 5px; padding-right: 25px">INVOICE <small style="font-size: 14px !imporant;">3/3</small></p>
			</div>
		</div>
		<table class="w-100">
			<tr>
				<td class="left" style="width: 43px">Kasir : </td>
				<td class="left">{{ $transaction->kasir }}</td>
				<td class="right">Cust : </td>
				<td class="right">{{ $transaction->pelanggan }}</td>
			</tr>
			<tr>
				<td class="left" style="width: 43px">Kode : </td>
				<td class="left">{{ $transaction->kode_transaksi }}</td>
				<td class="right">Tanggal : </td>
				<td class="right" style="width: 115px">{{ date('d M, Y', strtotime($transaction->created_at)) . ' ' . date('H:i', strtotime($transaction->created_at)) }}</td>
			</tr>
		</table>
		<hr class="hr-dash">
	</div>
	<div class="body">
		<table class="w-100" style="border-collapse: collapse;margin: 0; padding: 0">
			<thead>
				{{-- <tr>
					<td>Nama Barang</td>
					<td style="text-align: right">Qty</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr> --}}
				<tr>
					<td style="font-weight: bold">Nama Barang</td>
					<td style="font-weight: bold">Qty</td>
					<td style="text-align: right; font-weight: bold">Harga</td>
					<td style="text-align: right; font-weight: bold">Jumlah</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaksi)
				@if(($loop->index+1) > 20 && ($loop->index+1) <= 30)
				@php
				$counttotalbarang3 += $transaksi->jumlah;
				$countjumlahbarang3 += $transaksi->total_barang;
				@endphp
				{{-- <tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td style="text-align: right">{{ $transaksi->jumlah }}&nbsp;&nbsp;</td>
				</tr> --}}
				<tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td>{{ $transaksi->jumlah }} pcs</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->harga,2,',','.') }}</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->total_barang,2,',','.') }}</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
		<hr class="hr-dash">
		<table class="w-100" style="border-collapse: collapse;margin-bottom: 7px">
			<tr>
				<td class="left" style="padding-bottom: 20px">Invoice 3/3 (Jumlah : {{ $counttotalbarang3 }})</td>
				<td class="right" style="padding-bottom: 20px">{{ number_format($countjumlahbarang3,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Subtotal (Jumlah : {{ $counttotalbarang1 + $counttotalbarang2 + $counttotalbarang3 }})</td>
				<td class="right">{{ number_format($transaction->subtotal,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Diskon ({{ $transaction->diskon }}%)</td>
				<td class="right">{{ number_format($diskon?'-'.$diskon:$diskon,2,',','.') }}</td>
			</tr>
			@if($transaction->add_val)
			<tr>
				<td class="left">{{ $transaction->add_val }}</td>
				<td class="right">{{ number_format($transaction->value_add_val,2,',','.') }}</td>
			</tr>
			@endif
			<tr>
				<td class="left" style="font-weight: bold;padding: 0; margin: 0">Total</td>
				<td class="right" style="font-weight: bold;padding: 0; margin: 0">{{ number_format($transaction->total,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left"></td>
				<td class="right"></td>
			</tr>
			<tr>
				<td class="left" style="font-weight: bold;padding: 0; margin: 0">Pembayaran<style>
					.clsd {
					  padding-left: 4px !important;
					  padding-right: 2px !important;
					  padding-bottom: 1px !important;
					  border: 1px solid #495ff1;
					  border-left: 1px solid #495ff1 !important;
					  font-size: 12px;
					  color: #495ff1;
					  font-weight: bold;
					}
					.clsd.po {
					  border-color: #eb5454;
					  color: #eb5454;
					}
				  </style>
				  @if($transaction->po === 1)
				  <span class="clsd po">PO</span>
				  @elseif($transaction->po === 2)
				  <span class="clsd">CASH</span>
				  @else
				  <span class="clsd">TRANSFER</span>
				  @endif</td>
				<td class="right" style="font-weight: bold;padding: 0; margin: 0">{{ number_format($transaction->bayar,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Kembali</td>
				<td class="right">{{ number_format($transaction->kembali,2,',','.') }}</td>
			</tr>
		</table>
		{{-- <table style="border-collapse: collapse;margin-top: 30px;width: 100%; margin-bottom: 5px; border-bottom: 1px solid">
			<tr>
				<td><b>Rekening Perusahan</b></td>
			</tr>
			<tr>
				<td style="width: 42%">NAMA BANK:</td>
				<td style="width: 33%">NOMOR REKENING:</td>
				<td style="width: 25%">NAMA REKENING:</td>
			</tr>
			<tr>
				<td style="width: 42%;">BANK CENTRAL ASIA</td>
				<td style="width: 33%;">8691990443</td>
				<td style="width: 25%;">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
			<tr>
				<td style="width: 42%; padding-bottom: 9px">BANK MANDIRI</td>
				<td style="width: 33%; padding-bottom: 9px">1570003487387</td>
				<td style="width: 25%; padding-bottom: 9px">FIKRY SAKTI FIRMANSYAH</td>
				
			</tr>
		</table>
		<table style="border-collapse: collapse;width: 100%; font-weight: bold;margin-top: 9px">
			<tr>
				<td style="font-size: 9.5px; text-align: center">
				SIMA BANGUNAN PRODUK TERLENGKAP DENGAN HARGA TERBAIK | MENJUAL BERBAGAI KEBUTUHAN BANGUNAN RUMAH | BERDIRI SEJAK 1988
			</td>
			</tr>
		</table>
		<div style="font-size: 14px; width: 100%;text-align: center; margin-top: 3px; font-weight: bold">BEST CHOICE BUILDING MATERIAL</div>
		<div style="font-size: 9px; width: 100%;text-align: center; margin-top: 1px">www.simabangunan.com</div> --}}
	</div>
	@endif
</body>
</html>