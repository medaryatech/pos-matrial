@extends('templates/main')
@section('css')
<link rel="stylesheet" href="{{ asset('css/transaction/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/manage_account/new_account/style.css') }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<style>
  .discbrg {
      position: absolute;
      right: 20px;
      top: -4px;
      bottom: 0;
      height: 20px;
      width: 20px;
      margin: auto;
    }
    .discbrg button {
      background: #dee4eb;
      border: unset;
      color: #0022aa;
      padding: 5px;
      width: 23px;
      height: 23px;
      border-radius: 23px;
      margin-left: 4px;
      transition: all .3s ease-in-out;
      margin-top: 1px;
    }
    .discbrg button.saveready {
      background: #0022aa;
      color: #dee4eb;
      transition: all .3s ease-in-out;
    }
    .discbrg button:hover {
      background: #c5d1dd;
      transition: all .3s ease-in-out;
    }
    .discbrg button.saveready:hover {
      background: #001e97;
      transition: all .3s ease-in-out;
    }
    #nilaidiscpcs {
      position: absolute;
      left: -42px;
      margin-top: 5px;
      width: 40px;
      text-align: center;
      padding-left: 17px;
    }
    .diskon-inputpcs {
      position: absolute;
      margin-left: -62px;
      text-align: center;
      padding-left: 8px;
    }
    #disclabel {
      position: absolute;
      color: #030aac;
      font-size: 8px;
      font-weight: 600;
      margin-top: 5px;
      margin-left: -68px;
      padding: 2px;
      padding-bottom: 1px;
      padding-left: 4px;
      padding-top: 2px;
      padding-right: 4px;
      border: 1px solid #030aac;
      border-radius: 6px;
      background: #f3f3f3;
    }

    #tambVar {
      background-color: #ffffff;
      transition: background-color .3s ease-in-out;
    }

    #tambVar:hover {
      background-color: #eee;
      transition: background-color .3s ease-in-out;
    }
  </style>
<div class="row page-title-header" style="display: none">
  <div class="col-12">
    <div class="page-header d-flex justify-content-start align-items-center">
      <h4 class="page-title">Transaksi</h4>
    </div>
  </div>
</div>
<div class="row modal-group">
  <div class="modal fade" id="scanModal" tabindex="-1" role="dialog" aria-labelledby="scanModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="scanModalLabel">Scan Barcode</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
              <div class="col-12">
                <div class="alert alert-danger kode_barang_error" role="alert" hidden="">
                  <i class="mdi mdi-information-outline"></i> Kode barang tidak tersedia
                </div>
              </div>
              <div class="col-12 text-center" id="area-scan">
              </div>
              <div class="col-12 barcode-result" hidden="">
                <h5 class="font-weight-bold">Hasil</h5>
                <div class="form-border">
                  <p class="barcode-result-text"></p>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer" id="btn-scan-action" hidden="">
          <button type="button" class="btn btn-primary btn-sm font-weight-bold rounded-0 btn-continue">Lanjutkan</button>
          <button type="button" class="btn btn-outline-secondary btn-sm font-weight-bold rounded-0 btn-repeat">Ulangi</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="varianModal" tabindex="-1" role="dialog" aria-labelledby="varianModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="varianModalLabel">Pilih Varian</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            
        </div>
        <div class="modal-footer" id="btn-scan-action" hidden="">
          {{-- <button type="button" class="btn btn-primary btn-sm font-weight-bold rounded-0 btn-continue">Lanjutkan</button>
          <button type="button" class="btn btn-outline-secondary btn-sm font-weight-bold rounded-0 btn-repeat">Ulangi</button> --}}
        </div>
      </div>
    </div>
  </div>
  {{-- <div class="modal fade" id="tableModal" tabindex="-1" role="dialog" aria-labelledby="tableModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tableModalLabel">Daftar Barang</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                <input type="text" class="form-control" name="search" placeholder="Cari barang">
              </div>  
            </div>
            <div class="col-12">
              <ul class="list-group product-list" style="overflow: auto">
                <style>
                  @media (max-width: 767px) {
                    .mxc {
                      width: max-content;
                    }
                  }
                </style>
                @foreach($products as $product)
                @if($supply_system->status == true)
                @if($product->stok != 0)
                <li class="list-group-item d-flex justify-content-between align-items-center active-list mxc">
                  <div class="text-group">
                    <p class="m-0">{{ $product->kode_barang }}</p>
                    <p class="m-0 txt-light" style="color:#7c7c7c !important;font-size: 14px !important;">{{ $product->nama_barang }}</p>
                  </div>
                  <div class="d-flex align-items-center">
                    <span class="ammount-box bg-secondary mr-1"><i class="mdi mdi-cube-outline"></i></span>
                    <p class="m-0" style="width: 60px; padding-left: 5px">{{ $product->stok }}</p>
                  </div>
                  <a href="#" class="btn btn-icons btn-rounded btn-inverse-outline-primary font-weight-bold btn-pilih" role="button"><i class="mdi mdi-chevron-right"></i></a>
                </li>
                @endif
                @else
                <li class="list-group-item d-flex justify-content-between align-items-center active-list mxc">
                  <div class="text-group">
                    <p class="m-0">{{ $product->kode_barang }}</p>
                    <p class="m-0 txt-light">{{ $product->nama_barang }}</p>
                  </div>
                  <div class="d-flex align-items-center">
                    <span class="ammount-box bg-green mr-1"><i class="mdi mdi-coin"></i></span>
                    <p class="m-0">Rp. {{ number_format($product->harga,2,',','.') }}</p>
                  </div>
                  <a href="#" class="btn btn-icons btn-rounded btn-inverse-outline-primary font-weight-bold btn-pilih" role="button"><i class="mdi mdi-chevron-right"></i></a>
                </li>
                @endif
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> --}}
  <div class="modal fade" id="newuserModal" tabindex="-1" role="dialog" aria-labelledby="newuserModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="newuserModalLabel"><div class="quick-link-wrapper d-md-flex flex-md-wrap">
            <ul class="quick-links">
              <li><a href="{{ url('distributor') }}">Daftar Akun</a></li>
              <li><a href="{{ url('distributor/new') }}">Akun Baru</a></li>
            </ul>
          </div></h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        {{-- <div class="row page-title-header">
          <div class="col-12">
            <div class="page-header d-flex justify-content-start align-items-center">
              <div class="quick-link-wrapper d-md-flex flex-md-wrap">
                <ul class="quick-links">
                  <li><a href="{{ url('distributor') }}">Daftar Akun</a></li>
                  <li><a href="{{ url('distributor/new') }}">Akun Baru</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div> --}}
        <div class="row">
          <div class="col-12">
            <div class="card card-noborder b-radius">
              <div class="card-body">
                <form action="{{ url('distributor/ajax/create') }}" method="post" name="create_dist_form"
                enctype="multipart/form-data">
                  @csrf
                  <div class="form-group row">
                    <label class="col-12 font-weight-bold col-form-label">Nama <span class="text-danger">*</span></label>
                    <div class="col-12">
                      <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
                    </div>
                    <div class="col-12 error-notice" id="nama_error"></div>
                  </div>
                  <div class="form-group row">
                    <label class="col-12 font-weight-bold col-form-label">Email <span class="text-danger">*</span></label>
                    <div class="col-12">
                      <input type="email" class="form-control" name="email" placeholder="Masukkan Email">
                    </div>
                    <div class="col-12 error-notice" id="email_error"></div>
                  </div>
                  <div class="form-group row">
                    <label class="col-12 font-weight-bold col-form-label">Nama Toko <span class="text-danger">*</span></label>
                    <div class="col-12">
                      <input type="text" class="form-control" name="nama_toko" placeholder="Masukkan Nama Toko">
                    </div>
                    <div class="col-12 error-notice" id="nama_toko_error"></div>
                  </div>
                  <div class="form-group row">
                  <label class="col-12 font-weight-bold col-form-label">Tanggal Join</span></label>
                  <div class="col-12">
                    <input type="text" class="form-control" name="tanggal_join" placeholder="Masukkan Tanggal Join" id="datepicker">
                  </div>
                </div>
                  <div class="form-group row">
                  <label class="col-12 font-weight-bold col-form-label">No. Telp <span class="text-danger">*</span></label>
                  <div class="col-12">
                    <input type="tel" class="form-control" name="telp" placeholder="Masukkan Telp">
                  </div>
                  <div class="col-12 error-notice" id="telp_error"></div>
                </div>
                <div class="form-group row">
                  <label class="col-12 font-weight-bold col-form-label">Alamat <span class="text-danger">*</span></label>
                  <div class="col-12">
                    <textarea type="text" class="form-control" name="alamat" placeholder="Masukkan Alamat"></textarea>
                  </div>
                  <div class="col-12 error-notice" id="alamat_error"></div>
                </div>
                  <div class="row mt-5">
                    <div class="col-12 d-flex justify-content-end">
                      <button class="btn simpan-btn btn-sm btn-adddist" type="submit"><i class="mdi mdi-content-save"></i> Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
  @if ($message = Session::get('transaction_success'))
  <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body bg-grey">
          <div class="row">
            <div class="col-12 text-center mb-4">
              <img src="{{ asset('gif/success4.gif') }}">
              <h4 class="transaction-success-text">Transaksi Berhasil</h4>
            </div>
            @php
            $transaksi = \App\Transaction::where('transactions.kode_transaksi', '=', $message)
            ->select('transactions.*')
            ->first();
            @endphp
            <div class="col-12">
              <table class="table-receipt">
                <tr>
                  <td>
                    <span class="d-block little-td">Kode Transaksi</span>
                    <span class="d-block font-weight-bold">{{ $message }}</span>
                  </td>
                  <td>
                    <span class="d-block little-td">Tanggal</span>
                    <span class="d-block font-weight-bold">{{ date('d M, Y', strtotime($transaksi->created_at)) }}</span>
                  </td>
                </tr>
                <tr>
                  <td>
                    <span class="d-block little-td">Kasir</span>
                    <span class="d-block font-weight-bold">{{ $transaksi->kasir }}</span>
                  </td>
                  <td>
                    <span class="d-block little-td">Total</span>
                    <span class="d-block font-weight-bold text-success">Rp. {{ number_format($transaksi->total,2,',','.') }}</span>
                  </td>
                </tr>
              </table>
              <table class="table-summary mt-3">
                <tr>
                  <td class="line-td" colspan="2"></td>
                </tr>
                <tr>
                  <td class="little-td big-td">Bayar 
                    <style>
                      .clsd {
                        padding: 2px 3px;
                        border-radius: 5px;
                        border: 1px solid #495ff1;
                        font-size: 10px;
                        margin-left: 3px;
                        color: #495ff1;
                        font-weight: bold;
                      }
                      .clsd.po {
                        border-color: #eb5454;
                        color: #eb5454;
                      }
                    </style>
                    @if($transaksi->po === 1)
                    <span class="clsd po">PO</span>
                    @elseif($transaksi->po === 2)
                    <span class="clsd">CASH</span>
                    @else
                    <span class="clsd">TRANSFER</span>
                    @endif</td>
                  <td>Rp. {{ number_format($transaksi->bayar,2,',','.') }}</td>
                </tr>
                <tr>
                  <td class="little-td big-td">Kembali</td>
                  <td>Rp. {{ number_format($transaksi->kembali,2,',','.') }}</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-close-modal" data-dismiss="modal">Tutup</button>
          {{-- <a href="javascript:void(0)" onclick="printReceipt('{{ $message }}')" class="btn btn-sm btn-cetak-pdf">Cetak Struk</a> --}}
          <a href="{{ url('/transaction/receipt/' . $message) }}" target="_blank" class="btn btn-sm btn-cetak-pdf">Cetak Struk</a>
          <a href="{{ url('/transaction/receipt-full/' . $message) }}" target="_blank" class="btn btn-sm btn-cetak-pdf">Cetak Invoice</a>
          <a href="{{ url('/transaction/receipt-do/' . $message) }}" target="_blank" class="btn btn-sm btn-cetak-pdf">Cetak Surat Jalan</a>
        </div>
      </div>
    </div>
  </div>
  @endif
</div>
<style>
  .badgegross {
    margin-left: 5px;
    color: green;
    font-size: 10px;
    border: 1px solid green;
    border-radius: 10px;
    padding: 2px;
    padding-left: 5px;
    padding-right: 5px;
    padding-bottom: 1px;
    vertical-align: 1px
  }
</style>
<form method="POST" name="transaction_form" id="transaction_form" action="{{ url('/transaction/process') }}">
  @csrf
  <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 mb-4">
      <div class="row">
        <div class="col-12 mb-4 bg-dark-blue" style="margin-bottom: 5px !important;">
          <div class="card card-noborder b-radius" style="border-bottom-left-radius: 0;border-bottom-right-radius: 0;">
            <div class="card-body" style="padding-top: 20px;padding-bottom: 20px;">
              <div class="row">
                <div class="col-12 d-flex justify-content-between align-items-center transaction-header">
                  <div class="d-flex justify-content-start align-items-center">
                    <div class="icon-holder">
                      <i class="mdi mdi-swap-horizontal"></i>
                    </div>
                    <div class="transaction-code ml-3">
                      <p class="m-0 text-white">Kode Transaksi</p>
                      <p class="m-0 text-white">T{{ date('dmYHis') }}</p>
                      <input type="text" name="kode_transaksi" value="T{{ date('dmYHis') }}" hidden="">
                      <input id="pricestats" type="text" name="price_status" value="1" hidden="">
                    </div>
                  </div>
                  <style>
                    .btn.rsldist {
                      /* background: #264bf1; */
                    }
                    .btn.active {
                      background: #fff;
                    }
                    .btn.active p {
                      color: #264bf1 !important;
                    }
                  </style>
                  <div id="myDIV" class="btn-group mt-h" style=";margin-left: auto;margin-right: 12px;border: 1px solid #fff; height: 34px; display:none">
                    <button class="btn rsldist active" type="button" id="dist">
                      <p style="margin:0; color: #ffffff;font-size: 12px;padding-top: 1px;font-weight: bold">Distributor</p>
                    </button>
                    <div style="background: #eee;height: 31.6px;width: .5px;"></div>
                    <button class="btn rsldist" type="button" id="resel">
                      <p style="margin:0; color: #ffffff;font-size: 12px;padding-top: 1px;font-weight: bold">Reseller</p>
                    </button>
                  </button>
                  </div>
                  <div class="btn-group mt-h">
                    {{-- <button class="btn btn-search" data-toggle="modal" data-target="#tableModal" type="button">
                      <i class="mdi mdi-magnify"></i>
                    </button>
                    <button class="btn btn-scan" data-toggle="modal" data-target="#scanModal" type="button">
                      <i class="mdi mdi-crop-free"></i>
                    </button> --}}
                    {{-- <button class="btn btn-varian" data-toggle="modal" data-target="#varianModal" type="button" hidden="">
                      <i class="mdi mdi-crop-free"></i>
                    </button> --}}
                    <button title="Sync Stock" alt="Refresh Stock" id="refresh-stock" class="btn btn-search" type="button" style="background: #fff;color: #264bf1;padding-right: 3px;padding-left: 7px;padding-top: 8px;border-top-right-radius: 3px;border-bottom-right-radius: 3px;">
                      <i class="mdi mdi-refresh"></i>
                    </button>
                    <style>
                      #update-stock-a:hover small {
                        text-decoration: underline
                      }
                    </style>
                    <a id="update-stock-a" href="/product" target="_blank" style="text-decoration: unset;color: #ffffff;padding-top: 3px;padding-left: 10px;"><small style="font-weight: bold;">EDIT STOCK</small></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12">
          <div class="form-group" style="margin-bottom: 5px">
            <input type="text" class="form-control" name="search" placeholder="Cari barang">
          </div>  
        </div>
        <div class="col-12" style="margin-bottom: 5px">
          <ul class="list-group product-list" style="overflow: auto;overflow: auto;flex-direction: row;border-bottom: 1px solid #dfdfdf;height: 131px; overflow-y: hidden" id="containerprod">
            <style>
              @media (max-width: 767px) {
                .mxc {
                  width: max-content;
                }
              }
              #selectitems {
                transition: background-color .3s ease-in-out;
              }
              #selectitems:hover {
                  background-color: #eee;
                  transition: background-color .3s ease-in-out;
                }
            </style>
            @foreach($products as $product)
            @if($supply_system->status == true)
            @if($product->stok != 0)
            @php
            if($product->gambar_produk) {
              $gamprod = 'storage/'.join('/', array_slice(explode('/', $product->gambar_produk), 1));
            } else {
              $gamprod = 'images/no-image.jpg';
            }
            $pricevar = array();
            $countvar=0;
            $firstprice = 0;
            $lastprice = 0;
            @endphp

            @foreach($varians as $varian)
              @php
              if($varian->kode_barang === $product->kode_barang) {
                $countvar += 1;
                array_push($pricevar, $varian->harga_varian);
                sort($pricevar);
              }
              @endphp
              @endforeach
              @foreach ($pricevar as $index => $valuesdfa)
                @php
                  if($index === array_key_first($pricevar)) {
                    $firstprice = $valuesdfa;
                  }
                  if($index === array_key_last($pricevar)) {
                    $lastprice = $valuesdfa;
                  }
                @endphp
              @endforeach

              
            <li class="list-group-item d-flex justify-content-between align-items-center active-list mxc" style="height: 113px; cursor: pointer" id="selectitems">
              <img src="{{ asset($gamprod) }}" style="height: 101px; margin-left: -15px; margin-right: 15px; user-select: none;">
              <div class="text-group" style="width: 100px;line-height: 18px">
                <p class="m-0 kodebrg" style="user-select: none; display: none">{{ $product->kode_barang }}</p>
                <p class="m-0 txt-light" style="color:#7c7c7c !important;font-size: 14px !important;user-select: none; font-weight: bold; display: inline">
                  {{ $product->nama_barang }}<br><small>Stok: {{ $product->stok }}</small><br>
                  @if($firstprice)
                    @if($firstprice && $lastprice)
                    <small style="font-size: 10px;">IDR {{ number_format($firstprice,0,',',',') }} - {{ number_format($lastprice,0,',',',') }}</small>
                    @else
                    <small>IDR {{ number_format($firstprice,0,',',',') }}</small>
                    @endif
                  @else
                  <small>IDR {{ number_format($product->harga,0,',',',') }}</small>
                  @endif
                  @if($countvar)
                    <br>
                    <small style="font-size: 10px;color: #0022aa;font-weight: 900;border: 1px solid #0022aa;border-radius: 10px;padding: 1px;padding-top: 2px;padding-left: 5px;padding-right: 5px;">
                      {{$countvar}} VARIAN
                    </small>
                  @endif
                </p>
                  
              </div>
              <div class="d-flex align-items-center" style="display: none !important">
                <span class="ammount-box bg-secondary mr-1"><i class="mdi mdi-cube-outline"></i></span>
                <p class="m-0" style="width: 60px; padding-left: 5px;user-select: none" id="stock-barang">{{ $product->stok }}</p>
              </div>
            </li>
            @endif
            @else
            <li class="list-group-item d-flex justify-content-between align-items-center active-list mxc" style="height: 113px; cursor: pointer" id="selectitems">
              <div class="text-group">
                <p class="m-0 kodebrg">{{ $product->kode_barang }}</p>
                <p class="m-0 txt-light">{{ $product->nama_barang }}</p>
              </div>
              <div class="d-flex align-items-center">
                <span class="ammount-box bg-green mr-1"><i class="mdi mdi-coin"></i></span>
                <p class="m-0">Rp. {{ number_format($product->harga,2,',','.') }}</p>
              </div>
            </li>
            @endif
            @endforeach
          </ul>
        </div>
        <div class="col-12">
          <div class="card card-noborder b-radius" style="border-top-left-radius: 0;border-top-right-radius: 0;">
            <div class="card-body" style="padding-top: 20px;padding-bottom: 20px;">
              <div class="row">
                <div class="col-12 d-flex justify-content-start align-items-center" style="display: none !important">
                  <div class="cart-icon mr-3">
                    <i class="mdi mdi-cart-outline"></i>
                  </div>
                  <p class="m-0 text-black-50">Daftar Pesanan</p>
                </div>
                <div class="col-12 mt-3 table-responsive" style="margin-top: 0 !important;">
                  <table class="table table-checkout">
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="card card-noborder b-radius">
        <div class="card-body">
          <div class="row">
            <div class="col-12 payment-1">
              <table class="table-payment-1">
                <tr>
                  <td class="text-left">Tanggal</td>
                  <td class="text-right">{{ date('d M, Y') }}</td>
                </tr>
                <tr>
                  <td class="text-left">Waktu</td>
                  <td class="text-right">{{ date('H:i') }}</td>
                </tr>
                <tr>
                  <td class="text-left">Kasir</td>
                  <td class="text-right">{{ auth()->user()->nama }}</td>
                </tr>
              </table>
            </div>
            <div class="col-12 mt-4">
              <table class="table-payment-2">
                <tr>
                  <td class="text-left">
                    <span class="subtotal-td">Subtotal</span>
                    <span class="jml-barang-td">0 Barang</span>
                  </td>
                  <td class="text-right nilai-subtotal1-td">Rp. 0</td>
                  <td hidden=""><input type="text" class="nilai-subtotal2-td" name="subtotal" value="0"></td>
                  <td hidden=""><input type="text" class="nilai-subtotal2-td-tothpp" name="subtotal_hpp" value="0"></td>
                </tr>
                <tr>
                  <td class="text-left">
                    <span class="diskon-td">Diskon</span>
                    <a href="#" class="ubah-diskon-td">Ubah diskon</a>
                    <a href="#" class="simpan-diskon-td" hidden="">Simpan</a>
                  </td>
                  <td class="text-right d-flex justify-content-end align-items-center pt-2">
                    <input type="number" class="form-control diskon-input mr-2" min="0" max="100" name="diskon" value="0" hidden="">
                    <span class="nilai-diskon-td mr-1">0</span>
                    <span>%</span>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" class="text-center nilai-total1-td">Rp. 0</td>
                  <td hidden=""><input type="text" class="nilai-total2-td" name="total" value="0"></td>
                  <td hidden=""><input type="text" class="nilai-total2-td-tothpp" name="total_hpp" value="0"></td>
                </tr>
              </table>
            </div>
            <div class="col-12 mt-2">
              <table class="table-payment-3">
                <tr>
                  <td style="padding-bottom: 0;">
                    <a href="/distributor/new" target="_blank" style="text-decoration: unset"><small>Add new customer</small></a>
                    {{-- <a href="javascript:void(0)" data-toggle="modal" data-target="#newuserModal" style="text-decoration: unset"><small>Add new customer</small></a> --}}
                    <style>
                      select#selectdist > option {
                        padding: 10px
                      }
                    </style>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">Nama</div>
                      </div>
                      <input type="text" class="form-control" name="pelanggan" placeholder="Pelanggan.." value="No Name" hidden="">
                      <style>
                        .select2-results__option {
                          font-size: 12px;
                        }
                        .select2-container--default .select2-results__option--highlighted.select2-results__option--selectable {
                          background-color: #465ef2;
                        }
                        .select2-search__field {
                          border-color: #bbbec1 !important;
                          font-size: 12px;
                        }
                        .select2-dropdown {
                          border-radius: 0;
                          border-color: #dee2e6;
                          box-shadow: 0 0 0 0 rgb(90 113 208 / 11%), 0 4px 16px 0 rgb(167 175 183 / 33%);
                        }
                        .select2.select2-container {
                          border: 1px solid #dee2e6;
                          border-left-color: transparent;
                          position: relative;
                          -webkit-box-flex: 1;
                          -ms-flex: 1 1 auto;
                          flex: 1 1 auto;
                          width: 1%;
                        }
                        .select2-container--default .select2-selection--single {
                          border-color: transparent !important;
                          outline: unset !important;
                        }
                        #select2-selectdist-container {
                          font-size: 12px;
                        }
                      </style>
                      <select class="form-control js-example-basic-single" name="id_pelanggan" style="cursor: pointer; visibility: hidden" id="selectdist">
                          <option value=0>No Name</option>
                          @foreach($distributors as $distributor)
                          <option value={{$distributor->id}}>{{$distributor->nama}}</option>
                          @endforeach
                      </select>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">Rp.</div>
                      </div>
                      <input type="number" class="form-control number-input input-notzero bayar-input" id="bayarsejml" name="bayar" placeholder="Masukkan nominal bayar..">
                      <div style="width: 100%">
                        <style>
                          .bful {
                            cursor: pointer;
                            display: inline-block;
                            border: 1px solid #dee2e6;
                            margin-top: 8px;
                            padding: 5px 10px;
                            font-size: 12px;
                            border-radius: 2px;
                            width: fit-content;
                            min-width: calc((100% / 3) - 4px);
                            text-align: center;
                            user-select: none;
                          }
                        </style>
                        <span id="bayarfull" class="bful" style="margin-right: 6px">0</span><span id="bayar50" class="bful" style="margin-right: 6px">50,000</span><span id="bayar100" class="bful">100,000</span>
                      </div>
                      <div style="width: 100%">
                        <style>
                          .bful2 {
                            transition: .1s ease-in-out;
                            cursor: pointer;
                            display: inline-block;
                            border: 1px solid #dee2e6;
                            margin-top: 8px;
                            padding: 5px 10px;
                            font-size: 11px;
                            border-radius: 2px;
                            width: fit-content;
                            min-width: calc((100% / 3) - 4px);
                            text-align: center;
                            user-select: none;
                          }
                          .bful2.active {
                            transition: .1s ease-in-out;
                            background: #465ef2;
                            color: #ffffff;
                          }
                          #po.active {
                            transition: .1s ease-in-out;
                            background: #eb5454;
                            color: #ffffff;
                          }
                        </style>
                        <input class="form-control" name="po" value=2 hidden="">
                        <span id="cash" class="bful2 active" style="margin-right: 6px">CASH</span><span id="tf" class="bful2" style="margin-right: 6px">TRANSFER</span><span id="po" class="bful2">PO</span>
                      </div>
                      <div style="width: 100%">
                      <style>
                        .addval {
                          margin-top: 8px;
                          width: calc((100% / 3) - 4px);
                        }

                        .addval2 {
                          width: calc(100% - 8px - calc((100% / 3) - 4px));
                        }
                      </style>
                        <input class="form-control addval" name="name_add_value" style="margin-right: 3px" placeholder="Ongkir, etc.." id="name_add_value">
                        <input type="number" class="form-control addval addval2 number-input" name="value_add_value" placeholder="Masukan nominal.." id="value_add_value" disabled>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr class="nominal-error" hidden="">
                  <td class="text-danger nominal-min">Nominal bayar kurang</td>
                </tr>
                <tr>
                  <td class="text-right">
                    <button class="btn btn-bayar" type="button">Bayar</button>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@section('script')
<script src="{{ asset('plugins/js/quagga.min.js') }}"></script>
<script src="{{ asset('js/transaction/script.js') }}"></script>
<script src="{{ asset('js/manage_distributor/new_distributor/script.js') }}"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">
const element = document.querySelector("#containerprod");
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
element.addEventListener('wheel', (event) => {
  event.preventDefault();

  element.scrollBy({
    left: event.deltaY < 0 ? -80 : 80,
    
  });
});
@if ($message = Session::get('both_error'))
  swal(
  "",
  "{{ $message }}",
  "error"
  );
@endif

@if ($message = Session::get('create_success'))
    swal(
        "Berhasil!",
        "{{ $message }}",
        "success"
    );
  @endif

@if ($message = Session::get('email_error'))
  swal(
  "",
  "{{ $message }}",
  "error"
  );
@endif

$( function() {
  $( "#datepicker" ).datepicker();
} );

@if ($message = Session::get('transaction_success'))
  $('#successModal').modal('show');
@endif

// $("form[name='create_dist_form']").on('submit', function(e){
//   e.preventDefault();
//   $.ajax({
//     url: "{{ url('/distributor/ajax/create') }}",
//     method: "POST",
//     data: $("form[name='create_dist_form']").serialize(),
//     success:function(response){
//       if(response.messages === "success"){
        
//       }
//     }
//   });
// });

function printReceipt(idtrx) {
        // e.preventDefault();
        $.ajax({
            url: "{{ url('/transaction/receipt') }}/"+idtrx,
            method: "GET",
            success:function(response){
            if(response.status === 1) {
                swal(
                "Print Receipt!",
                "Successfully Printed Receipt",
                "success"
                );
            }
            }
        });
    };

  $('#dist').on('click', dist);
  function dist(event) {
    $('#resel').removeClass('active');
    $(this).addClass('active');
    $('.table-checkout').html('');
    $('#pricestats').val(1);
    $('.nilai-total1-td').html('Rp. 0');
    $('#bayarsejml').val('');
    $('#bayarfull').html('0');
  }

  $('#resel').on('click', resel);
  function resel(event) {
    $('#dist').removeClass('active');
    $(this).addClass('active');
    $('.table-checkout').html('');
    $('#pricestats').val(2);
    $('.nilai-total1-td').html('Rp. 0');
    $('#bayarsejml').val('');
    $('#bayarfull').html('0');
  }


$('.form-control[name="id_pelanggan"]').on('change', showSelectedValue);
  function showSelectedValue(event) {
    var target = $(event.target);
    $('.form-control[name="pelanggan"]').val(target.find('option:selected').text());
  }

//   name_add_value
// value_add_value

$('.form-control[name="name_add_value"]').on('input', showSelectedValue11);
  function showSelectedValue11(event) {
    if(event.target.value) {
      document.getElementById("value_add_value").disabled = false;
    } else {
      document.getElementById("value_add_value").disabled = true;
      $('#value_add_value').val('');
    }
  }

  $('#cash').on('click', showSelectedValue1);
  function showSelectedValue1(event) {
    $('#tf').removeClass('active');
    $('#po').removeClass('active');
    $(this).addClass('active');
    $('.form-control[name="po"]').val(2);
  }

  $('#tf').on('click', showSelectedValue2);
  function showSelectedValue2(event) {
    $('#cash').removeClass('active');
    $('#po').removeClass('active');
    $(this).addClass('active');
    $('.form-control[name="po"]').val(3);
  }

  $('#po').on('click', showSelectedValue3);
  function showSelectedValue3(event) {
    $('#cash').removeClass('active');
    $('#tf').removeClass('active');
    $(this).addClass('active');
    $('.form-control[name="po"]').val(1);
  }

// $(document).on('click', '#listview', function(e){
//   e.preventDefault();
//   $(this).toggleClass('active');
//   if($(this).hasClass('active')) {
//     var arrayprodlist = <?php echo $products;?>;
//     arrayprodlist.forEach(element => {
//       if(element.stok > 0) {
//         if($('#dist').hasClass('active')) {
//           tambahData(element.kode_barang, element.nama_barang, element.harga, element.stok, 1, element.hpp, response.product.gambar_produk);
//         } else {
//           tambahData(element.kode_barang, element.nama_barang, element.harga_reseller, element.stok, 1, element.hpp, response.product.gambar_produk);
//         }
//         $(this).html('<i class="mdi mdi-file"></i>');
//       }
//     });
//   } else {
//     $('.table-checkout').html('');
//     $(this).html('<i class="mdi mdi-file-document"></i>');
//   }
// });

// $(document).on('click', '.btn-pilih', function(e){
//   e.preventDefault();
//   var kode_barang = $(this).prev().prev().children().first().text();
//   $.ajax({
//     url: "{{ url('/transaction/product') }}/" + kode_barang,
//     method: "GET",
//     success:function(response){
//       var check = $('.kode-barang-td:contains('+ response.product.kode_barang +')').length;
//       if(check == 0){
//         if($('#dist').hasClass('active')) {
//           tambahData(response.product.kode_barang, response.product.nama_barang, response.product.harga, response.product.stok, response.status, response.product.hpp, response.product.gambar_produk);
//         } else {
//           tambahData(response.product.kode_barang, response.product.nama_barang, response.product.harga_reseller, response.product.stok, response.status, response.product.hpp, response.product.gambar_produk);
//         }
//       }else{
//         swal(
//             "",
//             "Barang telah ditambahkan",
//             "error"
//         );
//       }
//     }
//   });
// });

function tambahData(kode, nama, harga, stok, status, hpp, gambar_produk, grosir, idvarian) {
  if(gambar_produk) {
    var url = window.location.origin+'/storage/'+(gambar_produk.split('/').slice(1).join('/'));
  } else {
    var url = window.location.origin+'/images/no-image.jpg';
  }

  let datagross = '';
  if(grosir) {
    grosir.forEach(element => {
      datagross += '<span><input type="text" class="harga_grosir" name="harga_grosir" hidden="" value="'+ element.harga_grosir +'"><input type="text" class="minimal_grosir" name="minimal_grosir" hidden="" value="'+ element.minimal_grosir +'"></span>'
    });
  }

  let varid;
  if(idvarian) {
    varid = idvarian;
  } else {
    varid = '';
  }
  
  var tambah_data = '<tr class="animatenewdata"><td style="padding-right: 0"><img src="'+url+'" style="border-radius: 0;object-fit: cover;"></td><td><input type="text" name="kode_barang[]" hidden="" value="'+ kode +'"><span class="nama-barang-td" style="margin-bottom: 0">'+ nama +'<input type="text" name="nama_barang[]" hidden="" value="'+ nama +'"></span><span class="kode-barang-td" style="display:none">'+ kode +'</span></td><td><input type="text" name="harga_barang[]" hidden="" value="'+ harga +'"><input type="text" name="harga_barang_hpp[]" hidden="" value="'+ hpp +'"><span>Rp. '+ parseInt(harga).toLocaleString() +'</span><input type="text" class="harga_backup" name="harga_backup" hidden="" value="'+ harga +'"></td><td style="position: relative;padding-right: 120px;"><div class="d-flex justify-content-start align-items-center" style="position: relative;"><input class="jmlbarang" type="text" name="jumlah_barang[]" value="1" style="position: absolute;width: 70px;margin: auto;text-align: center;padding: 0;display: block;top: 0;bottom: 0;left: 0;right: 0;background: transparent; border: unset;margin-left: 26px;"><a href="#" class="btn-operate mr-2 btn-tambah"><i class="mdi mdi-plus"></i></a><span class="ammount-product mr-2" style="width: 70px" unselectable="on" onselectstart="return false;" onmousedown="return false;"><p class="jumlah_barang_text" style="color: #ffffff;">1</p></span><a href="#" class="btn-operate btn-kurang"><i class="mdi mdi-minus"></i></a></div><div class="discbrg"><small id="disclabel" hidden="">DISKON</small><input type="number" class="form-control diskon-inputpcs" min="0" max="100" name="'+ kode +'" value="0" hidden=""><span id="nilaidiscpcs">0</span><button id="discpcs" type="button">%</button></div></td><td><input type="text" class="total_barang" name="total_barang[]" hidden="" value="'+ harga +'"><input type="text" class="total_barang_hpp" name="total_barang_hpp[]" hidden="" value="'+ hpp +'"><span>Rp. '+ parseInt(harga).toLocaleString() +'</span></td><td><a href="#" class="btn btn-icons btn-rounded btn-secondary ml-1 btn-hapus"><i class="mdi mdi-close"></i></a></td><td hidden=""><span>'+ stok +'</span><span>'+ status +'</span></td><td hidden="" class="data_grosir">' + datagross + '</td><td hidden="" class="data_varian">' + varid + '<input type="text" name="idvar[]" hidden="" value="'+ varid +'"></td></tr>';
    $('.table-checkout').append(tambah_data);
    subtotalBarang();
    diskonBarang();
    jumlahBarang();
    // $('.close-btn').click();
  
}

$(document).on('click', '#discpcs', function(e){
  if(!$(this).prev().prev().hasClass("is-invalid")) {
    e.preventDefault();
    if($(this).hasClass("saveready")) {
      $(this).prev().prev().prop('hidden', true);
      $(this).prev().prop('hidden', false);
      $(this).removeClass('saveready');
      $(this).html('%');
      if(parseInt($(this).prev().text()) > 0) {
        $(this).prev().prev().prev().prop('hidden', false);
      } else {
        $(this).prev().prev().prev().prop('hidden', true);
      }
      ///////////////
      var stk = $(this).parent().prev().children().first().val();
      var harga = getPrice(this, stk);
      var total_barang = harga * stk;
      $(this).parent().parent().next().children().first().val(total_barang);
      $(this).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
      subtotalBarang();
      diskonBarang();
      jumlahBarang();
      ///////////////
    } else {
      $(this).prev().prev().prev().prop('hidden', true);
      $(this).prev().prev().prop('hidden', false);
      $(this).prev().prev().select();
      $(this).prev().prop('hidden', true);
      $(this).addClass('saveready');
      $(this).html('✓');
    }
  } else {
    alert('Max Disc 100%');
  }
});

function getPrice(elem, stok) {
  let harga;
  if($(elem).parent().parent().parent().find('.data_grosir span')[0]) {
    const finddata = $(elem).parent().parent().parent().find('.data_grosir span').toArray().reverse().find(element => stok >= parseInt($(element).find('.minimal_grosir').val()));
    if(finddata) {
      harga = parseInt($(finddata).find('.harga_grosir').val());
      $(elem).parent().parent().prev().children().first().val(harga);
      $(elem).parent().parent().prev().children().eq(2).html('Rp. ' + harga.toLocaleString() + '<small class="badgegross">GROSIR</small>');
    } else {
      //BACKUPPRICE
      harga = parseInt($(elem).parent().parent().prev().children().last().val());
      $(elem).parent().parent().prev().children().first().val(harga);
      $(elem).parent().parent().prev().children().eq(2).html('Rp. ' + harga.toLocaleString());
    }
  } else {
    harga = parseInt($(elem).parent().parent().prev().children().last().val());
  }
  var discc = parseInt($(elem).parent().parent().children().last().children().first().next().val());
  if(discc > 0) {
    harga = harga - (harga * discc/100);
    $(elem).parent().parent().prev().children().first().val(harga);
    if($(elem).parent().parent().parent().find('.data_grosir span')[0]) {
      const finddata = $(elem).parent().parent().parent().find('.data_grosir span').toArray().reverse().find(element => stok >= parseInt($(element).find('.minimal_grosir').val()));
      if(finddata) {
        $(elem).parent().parent().prev().children().eq(2).html('Rp. ' + harga.toLocaleString() + '<small class="badgegross">GROSIR</small>');
      } else {
        $(elem).parent().parent().prev().children().eq(2).html('Rp. ' + harga.toLocaleString());
      }
    } else {
      $(elem).parent().parent().prev().children().eq(2).html('Rp. ' + harga.toLocaleString());
    }
    return harga;
  } else {
    return harga;
  }
}

$(document).on('input', '.diskon-inputpcs', function(){
  $(this).val($(this).val().replace(/^0+/, ''));
  if($(this).val() === '') {
    $(this).val(0);
    $(this).select();
  }
  $(this).next().html($(this).val());
  if($(this).val().length > 0){
    $(this).removeClass('is-invalid');
  }else{
    $(this).addClass('is-invalid');
  }
});

$(document).on('keypress', '.diskon-inputpcs', function(e){
  if (e.which == 13) {
    if($(this).val() <= 100) {
      $(this).next().next().click();
    } else {
      alert('Max Disc 100%');
    }
  }
});

$(document).on('click', '.btn-tambah', function(e){
  e.preventDefault();
  var stok = parseInt($(this).parent().parent().next().next().next().children().first().text());
  var status = parseInt($(this).parent().parent().next().next().next().children().eq(1).text());
  //////////////////CONVERSION VARIAN///////////////////////////
  var kode_barang = $(this).parent().parent().parent().find('.kode-barang-td')[0].textContent;
  var varian_id = $(this).parent().parent().parent().find('.data_varian')[0].textContent;
  if(varian_id) {
    var elem = this;
    var brgcod = kode_barang.split('-');
    var check = $('.kode-barang-td:contains('+ brgcod[0] +')').toArray();
    let total_dipakai = 0;
    check.forEach(element => {
      if(element.textContent !== kode_barang) {
        const val =  $(element).parent().next().next().children().first().children().first().val();
        const idcheck = parseInt($(element).parent().parent().find('.data_varian')[0].textContent);
        $.ajax({
            url: "{{ url('/transaction/conversion') }}/"+idcheck,
            method: "GET",
            success:function(responses){
              if(responses.varian) {
                total_dipakai += (val * responses.varian.conv_varian);
              }
            }
        });
      }
    });
    setTimeout(() => {
      $.ajax({
        url: "{{ url('/transaction/conversion') }}/"+varian_id,
        method: "GET",
        success:function(response){
          if(response.varian) {
            var totalconv = response.varian.conv_varian;
            var jumlah_barang = parseInt($(elem).prev().val());
            var convcount = ((jumlah_barang+1) * totalconv) + total_dipakai;
            if((stok >= convcount && status == 1) || status == 0){
              var tambah_barang = jumlah_barang + 1;
              $(elem).prev().val(tambah_barang);
              $(elem).next().children().first().html(tambah_barang);
              var harga = getPrice(elem, tambah_barang);
              var hpp = parseInt($(elem).parent().parent().prev().children().first().next().val());
              var total_barang = harga * tambah_barang;
              var total_barang_hpp = hpp * tambah_barang;
              $(elem).parent().parent().next().children().first().val(total_barang);
              $(elem).parent().parent().next().children().first().next().val(total_barang_hpp);
              $(elem).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
              subtotalBarang();
              diskonBarang();
              jumlahBarang();
            } else {
              alert("Stok kamu tidak cukup setelah di konversi, cuma ada " + stok + " barang, dari total " + convcount + " barang");
            }
          }
        }
    });
    }, 10);
    
  } else {
    //////////////////CONVERSION VARIAN///////////////////////////
    var jumlah_barang = parseInt($(this).prev().val());
    if((stok > jumlah_barang && status == 1) || status == 0){
      var tambah_barang = jumlah_barang + 1;
      $(this).prev().val(tambah_barang);
      $(this).next().children().first().html(tambah_barang);
      var harga = getPrice(this, tambah_barang);
      var hpp = parseInt($(this).parent().parent().prev().children().first().next().val());
      var total_barang = harga * tambah_barang;
      var total_barang_hpp = hpp * tambah_barang;
      $(this).parent().parent().next().children().first().val(total_barang);
      $(this).parent().parent().next().children().first().next().val(total_barang_hpp);
      $(this).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
      subtotalBarang();
      diskonBarang();
      jumlahBarang();
    } else {
      alert("Stok kamu cuma ada " + stok + " pcs");
    }
  }
});

$(document).on('input', '.jmlbarang', function(e){
  e.preventDefault();
  var stok = parseInt($(this).parent().parent().next().next().next().children().first().text());
  var status = parseInt($(this).parent().parent().next().next().next().children().eq(1).text());
  //////////////////CONVERSION VARIAN///////////////////////////
  var kode_barang = $(this).parent().parent().parent().find('.kode-barang-td')[0].textContent;
  var varian_id = $(this).parent().parent().parent().find('.data_varian')[0].textContent;
  if(varian_id) {
    var elem = this;
    var brgcod = kode_barang.split('-');
    var check = $('.kode-barang-td:contains('+ brgcod[0] +')').toArray();
    let total_dipakai = 0;
    check.forEach(element => {
      if(element.textContent !== kode_barang) {
        const val =  $(element).parent().next().next().children().first().children().first().val();
        const idcheck = parseInt($(element).parent().parent().find('.data_varian')[0].textContent);
        $.ajax({
            url: "{{ url('/transaction/conversion') }}/"+idcheck,
            method: "GET",
            success:function(responses){
              if(responses.varian) {
                total_dipakai += (val * responses.varian.conv_varian);
              }
            }
        });
      }
    });
    setTimeout(() => {
      $.ajax({
        url: "{{ url('/transaction/conversion') }}/"+varian_id,
        method: "GET",
        success:function(response){
          if(response.varian) {
            var totalconv = response.varian.conv_varian;
            var jumlah_barang = parseInt($(elem).val());
            var convcount = ((jumlah_barang) * totalconv) + total_dipakai;
            if((stok >= convcount && status == 1) || status == 0){
              var tambah_barang = jumlah_barang;
              if(tambah_barang === 0) {
              $(elem).val(1);
              $(elem).prev().val(1);
              $(elem).next().next().children().first().html(1);
              var harga = getPrice(elem,tambah_barang);
              var hpp = parseInt($(elem).parent().parent().prev().children().first().next().val());
              var total_barang = harga * 1;
              var total_barang_hpp = hpp * 1;
              $(elem).parent().parent().next().children().first().val(total_barang);
              $(elem).parent().parent().next().children().first().next().val(total_barang_hpp);
              $(elem).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
              subtotalBarang();
              diskonBarang();
              jumlahBarang();
              alert("Minimal Qty adalah " + 1 + " pcs");
            } else {
              $(elem).prev().val(tambah_barang);
              $(elem).next().next().children().first().html(tambah_barang);
              var harga = getPrice(elem, tambah_barang);
              var hpp = parseInt($(elem).parent().parent().prev().children().first().next().val());
              var total_barang = harga * tambah_barang;
              var total_barang_hpp = hpp * tambah_barang;
              $(elem).parent().parent().next().children().first().val(total_barang);
              $(elem).parent().parent().next().children().first().next().val(total_barang_hpp);
              $(elem).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
              subtotalBarang();
              diskonBarang();
              jumlahBarang();
            }
            } else {
              if($(elem).val() === '') {
              // $(elem).val(1);
              var tambah_barang = 1;
              $(elem).prev().val(tambah_barang);
              $(elem).next().next().children().first().html(tambah_barang);
              var harga = getPrice(elem, tambah_barang);
              var hpp = parseInt($(elem).parent().parent().prev().children().first().next().val());
              var total_barang = harga * tambah_barang;
              var total_barang_hpp = hpp * tambah_barang;
              $(elem).parent().parent().next().children().first().val(total_barang);
              $(elem).parent().parent().next().children().first().next().val(total_barang_hpp);
              $(elem).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
              subtotalBarang();
              diskonBarang();
              jumlahBarang();
            } else {
              var nilval = Math.floor((stok - total_dipakai) / totalconv);
              $(elem).val(nilval);
              var tambah_barang = nilval;
              $(elem).prev().val(tambah_barang);
              $(elem).next().next().children().first().html(tambah_barang);
              // var harga = parseInt($(elem).parent().parent().prev().children().first().val());
              var harga = getPrice(elem, tambah_barang);
              var hpp = parseInt($(elem).parent().parent().prev().children().first().next().val());
              var total_barang = harga * tambah_barang;
              var total_barang_hpp = hpp * tambah_barang;
              $(elem).parent().parent().next().children().first().val(total_barang);
              $(elem).parent().parent().next().children().first().next().val(total_barang_hpp);
              $(elem).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
              subtotalBarang();
              diskonBarang();
              jumlahBarang();
              alert("Stok kamu tidak cukup setelah di konversi, cuma ada " + stok + " barang, dari total " + convcount + " barang");
            }
            }
          }
        }
    });
    }, 10);
  } else {
    //////////////////CONVERSION VARIAN///////////////////////////
    var jumlah_barang = parseInt($(this).val());
    if((stok >= jumlah_barang && status == 1) || status == 0){
      var tambah_barang = jumlah_barang;
      if(tambah_barang === 0) {
        $(this).val(1);
        $(this).prev().val(1);
        $(this).next().next().children().first().html(1);
        var harga = getPrice(this,tambah_barang);
        var hpp = parseInt($(this).parent().parent().prev().children().first().next().val());
        var total_barang = harga * 1;
        var total_barang_hpp = hpp * 1;
        $(this).parent().parent().next().children().first().val(total_barang);
        $(this).parent().parent().next().children().first().next().val(total_barang_hpp);
        $(this).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
        subtotalBarang();
        diskonBarang();
        jumlahBarang();
        alert("Minimal Qty adalah " + 1 + " pcs");
      } else {
        $(this).prev().val(tambah_barang);
        $(this).next().next().children().first().html(tambah_barang);
        var harga = getPrice(this, tambah_barang);
        var hpp = parseInt($(this).parent().parent().prev().children().first().next().val());
        var total_barang = harga * tambah_barang;
        var total_barang_hpp = hpp * tambah_barang;
        $(this).parent().parent().next().children().first().val(total_barang);
        $(this).parent().parent().next().children().first().next().val(total_barang_hpp);
        $(this).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
        subtotalBarang();
        diskonBarang();
        jumlahBarang();
      }
    } else {
      if($(this).val() === '') {
        // $(this).val(1);
        var tambah_barang = 1;
        $(this).prev().val(tambah_barang);
        $(this).next().next().children().first().html(tambah_barang);
        var harga = getPrice(this, tambah_barang);
        var hpp = parseInt($(this).parent().parent().prev().children().first().next().val());
        var total_barang = harga * tambah_barang;
        var total_barang_hpp = hpp * tambah_barang;
        $(this).parent().parent().next().children().first().val(total_barang);
        $(this).parent().parent().next().children().first().next().val(total_barang_hpp);
        $(this).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
        subtotalBarang();
        diskonBarang();
        jumlahBarang();
      } else {
        $(this).val(stok);
        var tambah_barang = stok;
        $(this).prev().val(tambah_barang);
        $(this).next().next().children().first().html(tambah_barang);
        // var harga = parseInt($(this).parent().parent().prev().children().first().val());
        var harga = getPrice(this, tambah_barang);
        var hpp = parseInt($(this).parent().parent().prev().children().first().next().val());
        var total_barang = harga * tambah_barang;
        var total_barang_hpp = hpp * tambah_barang;
        $(this).parent().parent().next().children().first().val(total_barang);
        $(this).parent().parent().next().children().first().next().val(total_barang_hpp);
        $(this).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
        subtotalBarang();
        diskonBarang();
        jumlahBarang();
        alert("Stok kamu cuma ada " + stok + " pcs");
      }
    }
  }
});

$(document).on('click', '.btn-kurang', function(e){
  e.preventDefault();
    var jumlah_barang = parseInt($(this).prev().prev().prev().val());
    if(jumlah_barang > 1){
      var kurang_barang = jumlah_barang - 1;
      $(this).prev().prev().prev().val(kurang_barang);
      $(this).prev().children().first().html(kurang_barang);
      var harga = getPrice(this, kurang_barang);
      var hpp = parseInt($(this).parent().parent().prev().children().first().next().val());
      var total_barang = harga * kurang_barang;
      var total_barang_hpp = hpp * kurang_barang;
      $(this).parent().parent().next().children().first().val(total_barang);
      $(this).parent().parent().next().children().first().next().val(total_barang_hpp);
      $(this).parent().parent().next().children().eq(2).html('Rp. ' + parseInt(total_barang).toLocaleString());
      subtotalBarang();
      diskonBarang();
      jumlahBarang();
    } else {
      alert("Stok kamu cuma ada " + stok + " pcs");
    }
});

function setTambahData(kode, nama, harga, stok, status, hpp, idvar) {
  var checkas = $('.kode-barang-td:contains('+ kode +')')[0]?.textContent;
  if(checkas !== kode){
    var brgcod = kode.split('-');
    var check = $('.kode-barang-td:contains('+ brgcod[0] +')').toArray();
    let total_dipakai = 0;
    check.forEach(element => {
      const val =  $(element).parent().next().next().children().first().children().first().val();
      const idcheck = parseInt($(element).parent().parent().find('.data_varian')[0].textContent);
      $.ajax({
          url: "{{ url('/transaction/conversion') }}/"+idcheck,
          method: "GET",
          success:function(responses){
            if(responses.varian) {
              total_dipakai += (val * responses.varian.conv_varian);
            }
          }
      });
    });
    setTimeout(() => {
      $.ajax({
        url: "{{ url('/transaction/conversion') }}/"+idvar,
        method: "GET",
        success:function(response){
          if(response.varian) {
            if(parseInt(stok) >= (total_dipakai + response.varian.conv_varian)) {
              tambahData(kode, nama, parseInt(harga), parseInt(stok), parseInt(status), parseInt(hpp), null, null, idvar);
              setTimeout(() => {
                const removelem = $('.animatenewdata').toArray();
                removelem.forEach(element => {
                  element.classList = 'disanim';
                });
              }, 300);
            } else {
              alert("Stok kamu tidak cukup setelah di konversi, cuma ada " + stok + " barang, dari total " + (total_dipakai + response.varian.conv_varian) + " barang");
            }
          }
        }
    });
    }, 10);
  } else {
    const arrydata = $('.table-checkout tr').toArray();
    arrydata.forEach(element => {
      var text = element.getElementsByClassName("kode-barang-td")[0].textContent;
      if(text === kode) {
        element.getElementsByClassName("btn-tambah")[0].click();
        element.classList = 'animatenewdata';
      }
    });
    setTimeout(() => {
      const removelem = $('.animatenewdata').toArray();
      removelem.forEach(element => {
        element.classList = 'disanim';
      });
    }, 300);
  }
  
}

$(document).on('click', '#selectitems', function(e){
  e.preventDefault();
  var kode_barang = $(this).find('.kodebrg').text();
  $.ajax({
    url: "{{ url('/transaction/product') }}/" + kode_barang,
    method: "GET",
    success:function(response){
      var check = $('.kode-barang-td:contains('+ response.product.kode_barang +')')[0]?.textContent;
      if(check !== response.product.kode_barang){
        // if($('#dist').hasClass('active')) {
          if(response.varian) {
            $('#varianModal').modal('show');
            let dom_varian = '';
            response.varian.forEach(function(e) {
              dom_varian += `<li class="list-group-item d-flex justify-content-between align-items-center active-list mxc" id="tambVar" style="height: 90px; cursor: pointer"
              onclick="setTambahData('${e.kode_barang}-${e.sku_varian}', '${response.product.nama_barang} ${e.nama_varian}', '${e.harga_varian}', '${response.product.stok}', '${response.status}', '${e.hpp_varian}', ${e.id})">
                <div class="text-group" style="width: 100%;line-height: 18px">
              <p class="m-0 kodebrg" style="user-select: none; display: none">${e.kode_barang}-${e.sku_varian}</p>
              <p class="m-0 txt-light" style="color:#7c7c7c !important;font-size: 14px !important;user-select: none; font-weight: bold; display: inline">
                ${response.product.nama_barang} ${e.nama_varian}<br><small>IDR ${e.harga_varian.toLocaleString()}</small></p>
            </div>
            <div class="d-flex align-items-center" style="display: none !important">
              <span class="ammount-box bg-secondary mr-1"><i class="mdi mdi-cube-outline"></i></span>
            </div></li>`
            });
            $('#varianModal .modal-body').html(dom_varian);
          } else {
            tambahData(response.product.kode_barang, response.product.nama_barang, response.product.harga, response.product.stok, response.status, response.product.hpp, response.product.gambar_produk, response.harga_grosir?response.harga_grosir:null, null);
          }
          
        // } else {
        //   tambahData(response.product.kode_barang, response.product.nama_barang, response.product.harga_reseller, response.product.stok, response.status, response.product.hpp, response.product.gambar_produk, response.harga_grosir?response.harga_grosir:null);
        // }
        setTimeout(() => {
          const removelem = $('.animatenewdata').toArray();
          removelem.forEach(element => {
            element.classList = 'disanim';
          });
        }, 300);
      }else{
        const arrydata = $('.table-checkout tr').toArray();
        arrydata.forEach(element => {
          var text = element.getElementsByClassName("kode-barang-td")[0].textContent;
          if(text === kode_barang) {
            element.getElementsByClassName("btn-tambah")[0].click();
            element.classList = 'animatenewdata';
          }
        });
        setTimeout(() => {
          const removelem = $('.animatenewdata').toArray();
          removelem.forEach(element => {
            element.classList = 'disanim';
          });
        }, 300);
        // swal(
        //     "",
        //     "Barang telah ditambahkan",
        //     "error"
        // );
      }
    }
  });
});

$(document).on('click', '#refresh-stock', function(e){
  e.preventDefault();
  var kode_barang = $(this).find('.kodebrg').text();
  $.ajax({
    url: "{{ url('/transaction/refresh-stock') }}",
    method: "GET",
    success:function(response){
      response.products.forEach(element => {
        const dataasdasd = $('#containerprod #selectitems').toArray().find(element99 => element99.getElementsByClassName('kodebrg')[0].textContent === element.kode_barang);
        if(dataasdasd) {
          dataasdasd.children[2].children[1].textContent = element.stok;
          dataasdasd.children[1].children[1].children[1].textContent = 'Stok: '+element.stok;
          dataasdasd.children[1].children[1].children[3].textContent = 'IDR '+(element.harga/1000).toFixed(3)
        }
        const dataasdasd2 = $('.table.table-checkout tr').toArray().find(element98 => element98.children[1].children[0].value === element.kode_barang);
        if(dataasdasd2) {
          dataasdasd2.children[6].children[0].textContent = element.stok
        }
      });
      let arriy = '<option value="0">No Name</option>';
      Object.values(response.distributors).sort((a, b) => (a.nama > b.nama) ? 1 : -1).forEach(elementd => {
        arriy += `<option value="${elementd.id}">${elementd.nama}</option>`
      });
      $('#selectdist').html('');
      $('#selectdist').append(arriy);
      if(response.products.length > 0) {
        swal(
          "Sinkronisasi Data!",
          "Sinkronisasi Data berhasil",
          "success"
        );
      }
    }
  });
});

function startScan() {
  Quagga.init({
    inputStream : {
      name : "Live",
      type : "LiveStream",
      target: document.querySelector('#area-scan')
    },
    decoder : {
      readers : ["ean_reader"],
      multiple: false
    },
    locate: false
  }, function(err) {
      if (err) {
          return
      }
      Quagga.start();
  });

  Quagga.onDetected(function(data){
    $('#area-scan').prop('hidden', true);
    $('#btn-scan-action').prop('hidden', false);
    $('.barcode-result').prop('hidden', false);
    $('.barcode-result-text').html(data.codeResult.code);
    $('.kode_barang_error').prop('hidden', true);
    stopScan();
  });
}

$(document).on('click', '.btn-scan', function(){
  $('#area-scan').prop('hidden', false);
  $('#btn-scan-action').prop('hidden', true);
  $('.barcode-result').prop('hidden', true);
  $('.barcode-result-text').html('');
  $('.kode_barang_error').prop('hidden', true);
  startScan();
});

$(document).on('click', '.btn-repeat', function(){
  $('#area-scan').prop('hidden', false);
  $('#btn-scan-action').prop('hidden', true);
  $('.barcode-result').prop('hidden', true);
  $('.barcode-result-text').html('');
  $('.kode_barang_error').prop('hidden', true);
  startScan();
});

$(document).on('click', '#bayarfull', function(){
  var subtotal = parseInt($('input[name=subtotal]').val());
  var diskon = parseInt($('input[name=diskon]').val());
  let add_val = 0;
  if($('input[name=value_add_value]').val()) {
    add_val = parseInt($('input[name=value_add_value]').val());
  }
  var total = subtotal - (subtotal * diskon / 100 - add_val);
  $('#bayarsejml').val(total);
});

$(document).on('click', '#bayar50', function(){
  const checknominal = $('#bayarsejml').val();
  if(checknominal) {
    $('#bayarsejml').val((parseInt(checknominal)+50000));
  } else {
    var total = 50000;
    $('#bayarsejml').val(total);
  }
});

$(document).on('click', '#bayar100', function(){
  const checknominal = $('#bayarsejml').val();
  if(checknominal) {
    $('#bayarsejml').val((parseInt(checknominal)+100000));
  } else {
    var total = 100000;
    $('#bayarsejml').val(total);
  }
});

// $(document).on('click', '.btn-continue', function(e){
//   e.stopPropagation();
//   var kode_barang = $('.barcode-result-text').text();
//   $.ajax({
//     url: "{{ url('/transaction/product/check') }}/" + kode_barang,
//     method: "GET",
//     success:function(response){
//       var check = $('.kode-barang-td:contains('+ response.product.kode_barang +')').length;
//       if(response.check == 'tersedia'){
//         if(check == 0){
//           if($('#dist').hasClass('active')) {
//             tambahData(response.product.kode_barang, response.product.nama_barang, response.product.harga, response.product.stok, response.status, response.product.hpp, response.product.gambar_produk, response.harga_grosir?response.harga_grosir:null);
//           } else {
//             tambahData(response.product.kode_barang, response.product.nama_barang, response.product.harga_reseller, response.product.stok, response.status, response.product.hpp, response.product.gambar_produk, response.harga_grosir?response.harga_grosir:null);
//           }
//           $('.close-btn').click();  
//         }else{
//           swal(
//               "",
//               "Barang telah ditambahkan",
//               "error"
//           );
//         }
//       }else{
//         $('.kode_barang_error').prop('hidden', false);
//       }
//     }
//   });
// });

$(document).on('click', '.btn-bayar', function(){
  var total = parseInt($('.nilai-total2-td').val());
  var bayar = parseInt($('.bayar-input').val());
  var check_barang = parseInt($('.jumlah_barang_text').length);
  if(bayar >= total){
    $('.nominal-error').prop('hidden', true);
    if(check_barang != 0){
      if($('.diskon-input').attr('hidden') != 'hidden'){
        $('.diskon-input').addClass('is-invalid');
      }else{
        if($('#name_add_value').val() && !$('#value_add_value').val()) {
          swal(
            "",
            "Masukan Nominal Additional Value",
            "error"
        );
        } else {
          $('#transaction_form').submit();
        }
      }
    }else{
      swal(
          "",
          "Pesanan Kosong",
          "error"
      );
    }
  }else{
    if(isNaN(bayar)) {
      $('.bayar-input').valid();
    }else{
      $('.nominal-error').prop('hidden', false);
    }
    
    if(check_barang == 0){
      swal(
          "",
          "Pesanan Kosong",
          "error"
      );
    }
  }
});
</script>
@endsection