@extends('templates/main')
@section('css')
<link rel="stylesheet" href="{{ asset('css/report/report_transaction/style.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/css/datedropper.css') }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
@endsection
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header d-flex justify-content-between align-items-center">
      <h4 class="page-title">Laba</h4>
      <div class="print-btn-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <i class="mdi mdi-export print-icon"></i>
            </div>
            <button class="btn btn-print" type="button" data-toggle="modal" data-target="#cetakModal">Export</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row modal-group">
  <div class="modal fade" id="cetakModal" tabindex="-1" role="dialog" aria-labelledby="cetakModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="cetakModalLabel">Export Laporan Laba</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ url('/report/income/export') }}" name="export_form" method="POST" target="_blank">
            @csrf
            <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-5 border rounded-left offset-col-1">
                    <div class="form-radio">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="jns_laporan" value="period" checked> Periode</label>
                    </div>
                  </div>
                  <div class="col-5 border rounded-right">
                    <div class="form-radio">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="jns_laporan" value="manual"> Manual</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 period-form">
                <div class="form-group row">
                  <div class="col-10 p-0 offset-col-1">
                    <p>Pilih waktu dan periode</p>
                  </div>
                  <div class="col-4 p-0 offset-col-1">
                    <input type="number" class="form-control form-control-lg time-input number-input dis-backspace input-notzero" name="time" value="1" min="1" max="4">
                  </div>
                  <div class="col-6 p-0">
                    <select class="form-control form-control-lg period-select" name="period">
                      <option value="hari" selected="">Hari</option>
                      <option value="minggu">Minggu</option>
                      <option value="bulan">Bulan</option>
                      <option value="tahun">Tahun</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-12 manual-form" hidden="">
                <div class="form-group row">
                  <div class="col-10 p-0 offset-col-1">
                    <p>Pilih tanggal awal dan akhir</p>
                  </div>
                  <div class="col-10 p-0 offset-col-1 input-group mb-2">
                    <input type="text" name="tgl_awal_export" class="form-control form-control-lg date" placeholder="Tanggal awal">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <i class="mdi mdi-calendar calendar-icon"></i>
                      </div>
                    </div>
                  </div>
                  <div class="col-10 p-0 offset-col-1 input-group">
                    <input type="text" name="tgl_akhir_export" class="form-control form-control-lg date" placeholder="Tanggal akhir">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <i class="mdi mdi-calendar calendar-icon"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-10 p-0 offset-col-1">
                    <p>Format</p>
                  </div>
                  <div class="col-10 p-0 offset-col-1">
                    <select class="form-control form-control-lg" name="filetype">
                      {{-- <option value="pdf" selected="">PDF</option> --}}
                      <option value="excel" selected="">EXCEL</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-export">Export</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12 mb-4">
    <div class="card card-noborder b-radius">
      <div class="card-body">
        <div class="row">
          <div class="col-12 text-center">
            <p class="m-0">Total Laba</p>
            <h2 class="font-weight-bold" id="totalp">Rp. {{ number_format($income - $total_hpp,0,',','.') }}</h2>
            <p class="m-0 txt-light" id="datarange">{{ str_replace(',','',date('d M, Y', strtotime($end_date))) }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 mb-4">
    <div class="card card-noborder b-radius">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="d-flex justify-content-between align-items-center">
              <h5 id="pemasukane">&nbsp;&nbsp;</h5>
              <style>
                .msktgl {
                  width: auto;
                  margin-left: auto;
                  margin-right: 10px;
                  height: 35px;
                  border-color: #ced4da;
                  border-radius: 3px;
                  width: 100px;
                }
              </style>
              <input type="text" class="form-control msktgl" name="tanggal_transaksi" placeholder="Tanggal.." id="datepicker">
              <div class="dropdown">
                <button class="btn btn-filter-chart icon-btn dropdown-toggle" type="button" id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Pilih Tanggal
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1">
                  <a class="dropdown-item chart-filter" href="#" data-filter="hari">Pilih Tanggal</a>
                  <a class="dropdown-item chart-filter" href="#" data-filter="minggu">1 Minggu Terakhir</a>
                  <a class="dropdown-item chart-filter" href="#" data-filter="bulan">1 Bulan Terakhir</a>
                  <a class="dropdown-item chart-filter" href="#" data-filter="tahun">1 Tahun Terakhir</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 mt-4" id="canvase" hidden="">
            <canvas id="myChart" style="width: 100%; height: 350px;"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 grid-margin">
    <div class="card card-noborder b-radius">
      <div class="card-body">
        <div class="row">
          <div class="col-12 mb-2">
            <form action="{{ url('report/transaction/filter/income') }}" method="get">
              @csrf
              <div class="form-group row align-items-center filter-group">
                <div class="col-lg-4 col-md-12 col-sm-12 col-12 search-div">
                  <input type="text" name="search" class="form-control form-control-lg" placeholder="Cari transaksi" value="{{ $keyword }}">
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12 input-group">
                  <input type="text" name="tgl_awal" class="form-control form-control-lg date" placeholder="Tanggal awal" value="{{$start_date_search}}">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <i class="mdi mdi-calendar calendar-icon"></i>
                    </div>
                  </div>
                  @if($start_date_search)
                  <div class="input-group-append" style="cursor: pointer" onclick="deleteTglAwal()">
                    <div class="input-group-text">
                      <i class="mdi mdi-close"></i>
                    </div>
                  </div>
                  @endif
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12 input-group tgl-akhir-div">
                  <input type="text" name="tgl_akhir" class="form-control form-control-lg date" placeholder="Tanggal akhir" value="{{$end_date_search}}">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <i class="mdi mdi-calendar calendar-icon"></i>
                    </div>
                  </div>
                  @if($end_date_search)
                  <div class="input-group-append" style="cursor: pointer" onclick="deleteTglAkhir()">
                    <div class="input-group-text">
                      <i class="mdi mdi-close"></i>
                    </div>
                  </div>
                  @endif
                </div>
                <div class="col-lg-2 col-md-4 col-sm-12 col-12 filter-btn-div">
                  <button class="btn btn-filter btn-sm btn-block" type="submit">Filter</button>
                </div>
              </div>
            </form>
          </div>
        	<div class="col-12">
            <ul class="list-date" style="overflow: auto;">
              @foreach($transactions as $transaction)
                <li class="txt-light filter-date" style="display: none">{{ date('d M, Y', strtotime($transaction->created_at)) }}</li>
                <table class="table table-custom">
                  <tr class="filter-header" style="display: none">
                      <th>Kode Transaksi</th>
                      <th>Total</th>
                      <th>Qty</th>
                      <th>Pelanggan</th>
                      <th>Pembayaran</th>
                      <th>Total Laba</th>
                      <th></th>
                  </tr>
                  <tr class="head">
                    @php
                    $transaksi = \App\Transaction::where('kode_transaksi', $transaction->kode_transaksi)
                    ->select('transactions.*')
                    ->first();
                    $products = \App\Transaction::where('kode_transaksi', $transaction->kode_transaksi)
                    ->select('transactions.*')
                    ->get();
                    $tgl_transaksi = \App\Transaction::where('kode_transaksi', '=' , $transaction->kode_transaksi)
                    ->select('created_at')
                    ->first();
                    @endphp
                    <td class="col-2">
                      <span class="d-block font-weight-bold big-font">{{ $transaction->kode_transaksi }}</span>
                      <span class="d-block mt-2 txt-light"><small>{{ date('d M, Y', strtotime($transaction->created_at)) . ' pada ' . date('H:i', strtotime($transaction->created_at)) }}</small></span>
                    </td>
                    <td class="col-3"><span class="ammount-box bg-green"><i class="mdi mdi-coin"></i></span>Rp. {{ number_format($transaction->total,2,',','.') }}<br /><small class="text-success ml-3 pl-4">{{ $transaction->diskon > 0? ' Diskon (' . $transaction->diskon . '%)':'' }}</small></td>
                    <td class="col-2 text-success font-weight-bold">
                      @php
                      $products = \Illuminate\Support\Facades\DB::table('transactions')
                          ->where('kode_transaksi', $transaction->kode_transaksi)
                          ->get();
                      $sum_jumlah = $products->sum('jumlah');
                      @endphp
                      {{ number_format($sum_jumlah, 0, ',', '.') }} Pcs</td>
                    <td class="col-2 font-weight-bold ">{{ $transaction->pelanggan }}</td>
                    @if($transaction->po === 1)
                        <td class="col-2 text-danger font-weight-bold">PO</td>
                    @elseif($transaction->po === 2)
                        <td class="col-2 text-success font-weight-bold">CASH</td>
                    @else
                        <td class="col-2 text-success font-weight-bold">TRANSFER</td>
                    @endif
                    @php
                    $transaksi = \App\Transaction::where('kode_transaksi', $transaction->kode_transaksi)
                    ->select('transactions.*')
                    ->first();
                    @endphp
                    <td><span class="ammount-box bg-green"><i class="mdi mdi-coin"></i></span>Rp. {{ number_format(($transaksi->total - $transaksi->total_hpp),2,',','.') }}<small class="text-success">{{ $transaksi->diskon > 0? ' Diskon (' . $transaksi->diskon . '%)':'' }}</small></td>
                    <td class="col-1">
                        <button class="btn btn-selengkapnya font-weight-bold" type="button" data-target="#dropdownTransaksi{{ $transaction->kode_transaksi }}"><i class="mdi mdi-chevron-down arrow-view"></i></button>
                    </td>
                  </tr>
                  <tr id="dropdownTransaksi{{ $transaction->kode_transaksi }}" data-status="0" class="dis-none">
                    <td colspan="7" class="dropdown-content">
                      <div class="d-flex justify-content-between align-items-center" style="justify-content: flex-start !important;">
                        <div class="kasir mb-3" style="margin-right: 10px;">
                          Kasir : {{ $transaction->kasir }}
                        </div>
                        @if($transaction->add_val)
                            <div class="kasir mb-3" style="color: coral;">
                                {{$transaction->add_val}} : Rp. {{ number_format($transaction->value_add_val,2,',','.') }}
                            </div>
                        @endif
                        <div class="total-barang mb-3" style="margin-left: auto;">
                            <a onclick="return confirm('Are you sure?')" href="{{ url('/transaction/retur/' . $transaction->kode_transaksi) }}" class="btn" type="button" style="margin-left: 10px; background: #ff6258; color: #ffffff">Retur</a>
                        </div>
                        <div class="total-barang mb-3">
                            {{-- <a href="javascript:void(0)" onclick="printReceipt('{{ $transaction->kode_transaksi }}')" class="btn" type="button" style="margin-left: 7px; background: #435df2; color: #ffffff">Cetak Struk</a> --}}
                            <a href="{{ url('/transaction/receipt/' . $transaction->kode_transaksi) }}" target="_blank" class="btn" type="button" style="margin-left: 7px; background: #435df2; color: #ffffff">Cetak Struk</a>
                        </div>
                        <div class="total-barang mb-3">
                            <a href="{{ url('/transaction/receipt-full/' . $transaction->kode_transaksi) }}" target="_blank" class="btn" type="button" style="margin-left: 7px; background: #435df2; color: #ffffff">Cetak Invoice</a>
                        </div>
                        <div class="total-barang mb-3">
                            <a href="{{ url('/transaction/receipt-do/' . $transaction->kode_transaksi) }}" target="_blank" class="btn" type="button" style="margin-left: 7px; background: #435df2; color: #ffffff">Cetak Surat Jalan</a>
                        </div>
                        @if($transaction->po === 1)
                            <div class="total-barang mb-3">
                                <a href="{{ url('/transaction/pocash/' . $transaction->kode_transaksi) }}" class="btn" type="button" style="margin-left: 7px; background: #19d895; color: #ffffff">Bayar Cash</a>
                            </div>
                            <div class="total-barang mb-3">
                                <a href="{{ url('/transaction/potransfer/' . $transaction->kode_transaksi) }}" class="btn" type="button" style="margin-left: 7px; background: #19d895; color: #ffffff">Bayar Transfer</a>
                            </div>
                        @endif
                      </div>
                      <table class="table">
                        @foreach($products as $product)
                        <tr>
                          <td><span class="numbering">{{ $loop->iteration }}</span></td>
                          <td>
                            <span class="bold-td">{{ $product->nama_barang }}</span>
                            <span class="light-td mt-1">{{ $product->kode_barang }}</span>
                          </td>
                          <td><span class="ammount-box-2 bg-secondary"><i class="mdi mdi-cube-outline"></i></span> {{ $product->jumlah }}</td>
                          <td>
                            <span class="light-td mb-1">Harga</span>
                            <span class="bold-td">Rp. {{ number_format($product->harga,2,',','.') }}</span>
                          </td>
                          <td>
                            <span class="light-td mb-1">HPP</span>
                            <span class="bold-td">Rp. {{ number_format($product->hpp,2,',','.') }}</span>
                          </td>
                          <td>
                            <span class="light-td mb-1">Total Barang</span>
                            <span class="bold-td">Rp. {{ number_format($product->total_barang,2,',','.') }}</span>
                          </td>
                          <td>
                            <span class="light-td mb-1">Total Laba</span>
                            <span class="bold-td">Rp. {{ number_format(($product->total_barang - $product->total_barang_hpp),2,',','.') }}</span>
                          </td>
                        </tr>
                        @endforeach
                      </table>
                    </td>
                  </tr>
                </table>
              @endforeach
            </ul>
        	</div>
          <div class="d-flex col-12 justify-content-end mt-2">
              {{ $transactions->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{ asset('plugins/js/datedropper.js') }}"></script>
<script src="{{ asset('js/report/report_transaction/script.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<script type="text/javascript">
var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today = (month)+"/"+(day)+"/"+now.getFullYear() ;
$(".msktgl").val(today);
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [
        @if(count($incomes) != 0)
        @foreach($incomes as $income)
        "{{ date('j M Y', strtotime($income)) }}",
        @endforeach
        @endif
        ],
        datasets: [{
            label: '',
            data: [
            @if(count($incomes) != 0)
            @foreach($incomes as $income)
            @php
            $total = \App\Transaction::whereDate('created_at', $income)->sum('total_barang')-\App\Transaction::whereDate('created_at', $income)->sum('total_barang_hpp');;
            @endphp
            "{{ $total }}",
            @endforeach
            @endif
            ],
            backgroundColor: 'RGB(211, 234, 252)',
            borderColor: 'RGB(44, 77, 240)',
            borderWidth: 3
        }]
    },
    options: {
        title: {
            display: false,
            text: ''
        },
        scales: {
            yAxes: [{
              ticks: {
                  beginAtZero: false,
                  callback: function(value, index, values) {
                    if (parseInt(value) >= 1000) {
                       return 'Rp. ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    } else {
                       return 'Rp. ' + value;
                    }
                 }
              }
          }]
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
               label: function(tooltipItem) {
                      return tooltipItem.yLabel;
               }
            }
        }
    }
});

function changeData(chart, label_array, data_array){
  label_array.forEach((element, i) => {
      const dt = new Date(element);
      label_array[i] = dt.toLocaleString('en-GB', {
          day: 'numeric', // numeric, 2-digit
          year: 'numeric', // numeric, 2-digit
          month: 'short', // numeric, 2-digit, long, short, narrow
      });
  });
  chart.data = {
      labels: label_array,
      datasets: [{
          label: '',
          data: data_array,
          backgroundColor: 'RGB(211, 234, 252)',
          borderColor: 'RGB(44, 77, 240)',
          borderWidth: 3
      }]
  }
  chart.update();
}

$(document).on('submit', 'form[name=filter_form]', function(e){
  e.preventDefault();
  var request = new FormData(this);
  $.ajax({
      url: "{{ url('/report/transaction/filter/income') }}",
      method: "POST",
      data: request,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data){
        $('.list-date').html(data);
        $('input[name=search]').val('');
        dateListCheck();
      }
  });
});

$(document).on('click', '.chart-filter', function(e){
  e.preventDefault();
  var data_filter = $(this).attr('data-filter');
  if(data_filter == 'hari'){
    $('.btn-filter-chart').html('Hari Ini');
    $('#pemasukane').html('&nbsp;&nbsp;');
    $("#canvase").prop('hidden', true);
    $(".msktgl").prop('hidden', false);
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = (month)+"/"+(day)+"/"+now.getFullYear() ;
    $(".msktgl").val(today);
  }else if(data_filter == 'minggu'){
    $('.btn-filter-chart').html('1 Minggu Terakhir');
    $('#pemasukane').html('Laba&nbsp;&nbsp;');
    $("#canvase").prop('hidden', false);
    $(".msktgl").prop('hidden', true);
  }else if(data_filter == 'bulan'){
    $('.btn-filter-chart').html('1 Bulan Terakhir');
    $('#pemasukane').html('Laba&nbsp;&nbsp;');
    $("#canvase").prop('hidden', false);
    $(".msktgl").prop('hidden', true);
  }else if(data_filter == 'tahun'){
    $('.btn-filter-chart').html('1 Tahun Terakhir');
    $('#pemasukane').html('Laba&nbsp;&nbsp;');
    $("#canvase").prop('hidden', false);
    $(".msktgl").prop('hidden', true);
  }
  $.ajax({
    url: "{{ url('/report/laba/chart') }}/" + data_filter,
    method: "GET",
    success:function(response){
      if(response.incomes.length != 0){
        changeData(myChart, response.incomes, response.total);
        let sum = 0;
        response.total.forEach(element => {
          sum += Number(element);
        });
        let datasum;
        if (parseInt(sum) >= 1000) {
            datasum = 'Rp. ' + sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        } else {
            datasum = 'Rp. ' + sum;
        }
        $('#totalp').html(datasum);
        var firstItem = response.incomes[0];
        var lastItem = response.incomes[response.incomes.length-1];
        if(firstItem === lastItem) {
          $('#datarange').html(firstItem);
        } else {
          $('#datarange').html(firstItem+' - '+lastItem);
        }
      }
    }
  });
});
$(document).on('change', '.msktgl', function(e){
  e.preventDefault();
  var now = new Date(e.target.value);
  var day = ("0" + now.getDate()).slice(-2);
  var month = ("0" + (now.getMonth() + 1)).slice(-2);
  var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
  $.ajax({
    url: "{{ url('/report/laba/total') }}/" + today,
    method: "GET",
    success:function(response){
      if(response.incomes.length != 0){
        changeData(myChart, response.incomes, response.total);
        let sum = 0;
        response.total.forEach(element => {
          sum += Number(element);
        });
        let datasum;
        if (parseInt(sum) >= 1000) {
            datasum = 'Rp. ' + sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        } else {
            datasum = 'Rp. ' + sum;
        }
        $('#totalp').html(datasum);
        var firstItem = response.incomes[0];
        $('#datarange').html(firstItem);
      }
    }
  });
});

function printReceipt(idtrx) {
        // e.preventDefault();
        $.ajax({
            url: "{{ url('/transaction/receipt') }}/"+idtrx,
            method: "GET",
            success:function(response){
            if(response.status === 1) {
                swal(
                "Print Receipt!",
                "Successfully Printed Receipt",
                "success"
                );
            }
            }
        });
    };


$( function() {
  $( "#datepicker" ).datepicker();
} );
</script>
@endsection