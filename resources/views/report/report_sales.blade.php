@extends('templates/main')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/report/report_transaction/style.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/css/datedropper.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div class="row page-title-header">
        <div class="col-12">
            <div class="page-header d-flex justify-content-between align-items-center">
                <h4 class="page-title">Laporan Penjualan Customer</h4>
                <div class="print-btn-group" style="visibility: hidden">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="mdi mdi-export print-icon"></i>
                            </div>
                            <button class="btn btn-print" type="button" data-toggle="modal"
                                data-target="#cetakModal">Export</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-5 mb-4">
            <div class="card card-noborder b-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <p class="m-0">Total Penjualan <span
                                    id="namep">{{ $all_customers->first()->pelanggan }}</span></p>
                            <h2 class="font-weight-bold" id="totalp">Rp. {{ number_format($income, 0, ',', '.') }}</h2>
                            <p class="m-0 txt-light" id="datarange">
                                {{ str_replace(',', '', date('d M, Y', strtotime($start_date))) }} sampai {{ str_replace(',', '', date('d M, Y', strtotime($end_date))) }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-7    mb-4">
            <div class="card card-noborder b-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 d-flex justify-content-end align-items-center">
                            <div class="table-wrapper mr-auto" style="border: 1px solid #eee;padding: 8px;width: 100%">
                                <div class="top d-flex align-items-center justify-content-between mb-3">
                                    <h5 style="font-size: 14px;padding-top: 8px;padding-left: 8px;font-weight: 900;">
                                        TOP 5 CUSTOMER
                                    </h5>
                                    <div class="dropdown">
                                        <button class="btn btn-filter-chart icon-btn dropdown-toggle" type="button"
                                            id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            1 Minggu Terakhir
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1">
                                            <a class="dropdown-item top-customers-filter" href="#" data-filter="minggu">1
                                                Minggu
                                                Terakhir</a>
                                            <a class="dropdown-item top-customers-filter" href="#" data-filter="bulan">1
                                                Bulan
                                                Terakhir</a>
                                            <a class="dropdown-item top-customers-filter" href="#" data-filter="tahun">1
                                                Tahun
                                                Terakhir</a>
                                        </div>
                                    </div>
                                </div>
                                <style>
                                    .theadth {
                                        height: 6px !important;
                                        line-height: 5px !important;
                                        padding-top: 14px !important;
                                        font-size: 12px !important;
                                        font-weight: 900 !important;
                                        color: #ffffff !important;
                                        background-color: #3252f0 !important;
                                    }
                                    .table th, .table tbody td {
                                        border-width: 0.5px;
                                        border-color: #eee;
                                    }
                                </style>
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col" class="theadth">No</th>
                                            <th scope="col" class="theadth">Nama</th>
                                            <th scope="col" class="theadth">Transaksi</th>
                                            <th scope="col" class="theadth">Omzet</th>
                                        </tr>
                                    </thead>
                                    <tbody id="top-customers-table">
                                    </tbody>
                                </table>
                            </div>
                            <style>
                                .select2-results__option {
                                    font-size: 12px;
                                }

                                .select2-container--default .select2-results__option--highlighted.select2-results__option--selectable {
                                    background-color: #465ef2;
                                }

                                .select2-search__field {
                                    border-color: #bbbec1 !important;
                                    font-size: 12px;
                                }

                                .select2-dropdown {
                                    border-radius: 0;
                                    border-color: #dee2e6;
                                    box-shadow: 0 0 0 0 rgb(90 113 208 / 11%), 0 4px 16px 0 rgb(167 175 183 / 33%);
                                }

                                .select2.select2-container {
                                    border: 1px solid #dee2e6;
                                    position: relative;
                                    -webkit-box-flex: 1;
                                    -ms-flex: 1 1 auto;
                                    flex: 1 1 auto;
                                    width: 1%;
                                }

                                .select2-container--default .select2-selection--single {
                                    border-color: transparent !important;
                                    outline: unset !important;
                                }

                                #select2-selectdist-container {
                                    font-size: 12px;
                                }

                                .select2-container--default .select2-selection--single .select2-selection__rendered {
                                    font-size: 14px;
                                }

                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin">
            <div class="card card-noborder b-radius">
                <div class="card-body">
                    <div class="filter-customers mb-4">
                        <label for="all_customers" class="mb-0 mr-2 font-weight-bold" style="font-size: 14px">Pilih Customer</label>
                        <select id="all_customers" name="all_customers">
                            @foreach ($all_customers as $customer)
                                <option value="{{ $customer->id_pelanggan }}">{{ $customer->pelanggan }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-12 mb-2">
                            <form name="filter_form" method="POST">
                                @csrf
                                <div class="form-group row align-items-center filter-group">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-12 search-div">
                                        <input type="text" name="search" class="form-control form-control-lg"
                                            placeholder="Cari transaksi">
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 input-group">
                                        <input type="text" name="tgl_awal" class="form-control form-control-lg date"
                                            placeholder="Tanggal awal">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <i class="mdi mdi-calendar calendar-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 input-group tgl-akhir-div">
                                        <input type="text" name="tgl_akhir" class="form-control form-control-lg date"
                                            placeholder="Tanggal akhir">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <i class="mdi mdi-calendar calendar-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-12 col-12 filter-btn-div">
                                        <button class="btn btn-filter btn-sm btn-block" type="button">Filter</button>
                                    </div>
                                    <input type="hidden" name="idCustomer">
                                </div>
                            </form>
                        </div>
                        {{-- <div class="d-flex col-12 mt-3 justify-content-end">
                            {{ $transactions->links() }}
                        </div> --}}
                        <div class="col-12" id="tableTransactionWrapper"></div>
                        {{-- <div class="d-flex col-12 justify-content-end mt-2">
                            {{ $transactions->links() }}
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('plugins/js/datedropper.js') }}"></script>
    <script src="{{ asset('js/report/report_transaction/script.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script type="text/javascript">
        $(document).on('submit', 'form[name=filter_form]', function(e) {
            e.preventDefault();
            var request = new FormData(this);
            $.ajax({
                url: "{{ url('/report/sales/filter') }}",
                method: "POST",
                data: request,
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    $('#tableTransactionWrapper').html(data);
                    $('input[name=search]').val('');
                    dateListCheck();
                }
            });
        });

        $(function() {
            $('#all_customers').select2();
            setIdCustomer({{ $all_customers->first()->id_pelanggan }});
            getTopCustomers()
        });

        $(document).on('change', '#all_customers', function(e) {
            e.preventDefault();
            $('#namep').text($(e)[0].target.selectedOptions[0].label)
            const id = e.currentTarget.value;
            setIdCustomer(id)
        });

        function setIdCustomer(id) {
            $('input[name=idCustomer]').val(id)
            $('form[name=filter_form]').trigger('submit');
        }

        $(document).on('click', '.top-customers-filter', function(e) {
            e.preventDefault();
            var data_filter = $(this).attr('data-filter');
            let period = 'minggu';
            if (data_filter == 'bulan') {
                $('.btn-filter-chart').html('1 Bulan Terakhir');
                period = 'bulan';
            } else if (data_filter == 'tahun') {
                $('.btn-filter-chart').html('1 Tahun Terakhir');
                period = 'tahun';
            } else {
                $('.btn-filter-chart').html('1 Minggu Terakhir');
                period = 'minggu';
            }
            getTopCustomers(period)
        });

        function getTopCustomers(period = 'minggu') {
            $.ajax({
                url: "{{ url('/report/sales/top-customers') }}/" + period,
                method: "GET",
                success: function(data) {
                    $('#top-customers-table').html(data);
                }
            });
        }
    </script>
@endsection
