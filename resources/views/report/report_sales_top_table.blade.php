@foreach ($top_customers as $top_customer)
    <tr>
        <th scope="row">{{ $loop->index + 1 }}</th>
        <td>{{ $top_customer->pelanggan }}</td>
        <td>{{ $top_customer->total_transaksi }} <small>TRX</small></td>
        <td>Rp. {{ number_format($top_customer->total_omzet, 0, ',', '.') }}</td>
    </tr>
@endforeach