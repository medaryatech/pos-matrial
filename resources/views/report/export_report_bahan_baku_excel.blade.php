<table>
    <tr>
        <th style="font-weight: bold">LAPORAN BAHAN BAKU</th>
    <tr>
</table>
<table>
    <tr>
        <th style="font-weight: bold">Nama Barang</th>
		<th style="font-weight: bold">Kode Barang</th>
        <th style="font-weight: bold">Stok</th>
        <th style="font-weight: bold">Satuan</th>
        <th style="font-weight: bold">Harga Satuan</th>
        <th style="font-weight: bold">Jumlah</th>
    </tr>
    @foreach($bahan_baku as $product)
    <tr>
		<td>{{ $product->nama_bahan }}</td>
        <td style="text-align: left">{{ $product->kode_bahan }}</td>
        <td style="text-align: left">{{ $product->stok }}</td>
        @if($product->berat_bahan === 'kg')
        <td>Kilogram</td>
        @elseif ($product->berat_bahan === 'g')
        <td>Gram</td>
        @elseif ($product->berat_bahan === 'ml')
        <td>Miligram</td>
        @elseif ($product->berat_bahan === 'oz')
        <td>Ons</td>
        @elseif ($product->berat_bahan === 'l')
        <td>Liter</td>
        @elseif ($product->berat_bahan === 'ml')
        <td>Mililiter</td>
        @else
        <td>Pcs</td>
        @endif
        <td>Rp. {{ number_format($product->harga,2,',','.') }}</td>
        <td>Rp. {{ number_format(($product->harga * $product->stok),2,',','.') }}</td>
    </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
		<td></td>
        <td style="font-weight: bold">Total</td>
        <td style="font-weight: bold">Rp. {{ number_format($count,2,',','.') }}</td>
</table>