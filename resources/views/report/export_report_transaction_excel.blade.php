<table>
    <tr>
        {{-- <td><img src="./images/cmi_bw.png"></td> --}}
        <td></td>
        <td>{{ $market->nama_toko }}</td>
    </tr>
    <tr>
        <td></td>
        <td>{{ $market->alamat }}</td>
    </tr>
    <tr>
        <td></td>
        <td>{{ $market->no_telp }}</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>LAPORAN TRANSAKSI</td>
    </tr>
    <tr>
        <td></td>
        @if(date('d M, Y', strtotime($tgl_awal)) === date('d M, Y', strtotime($tgl_akhir)))
            <td>Periode Laporan {{ date('d M, Y', strtotime($tgl_awal))}}</td>
        @else
            <td>Periode Laporan {{ date('d M, Y', strtotime($tgl_awal)) . ' - ' . date('d M, Y', strtotime($tgl_akhir))}}</td>
        @endif
    </tr>
    <tr>
        @php
            $nama_users = explode(' ',auth()->user()->nama);
            $nama_user = $nama_users[0];
        @endphp
        <td></td>
        <td>Dicetak {{ \Carbon\Carbon::now()->isoFormat('DD MMM, Y') }} Oleh {{ $nama_user }}</td>
    </tr>
</table>
@php
    $pemasukan = 0;
    $pemasukan_po = 0;
    $pemasukan_cash = 0;
    $pemasukan_transfer = 0;
    $kode_transaksi = '';
    $date = '';
@endphp

<table>
    @foreach($transactions as $transaction)
        <tr><td></td></tr>
        @if($date != date('d M, Y', strtotime($transaction->created_at)))
            <tr><td></td><td style="font-weight: bold">{{ date('d M, Y', strtotime($transaction->created_at)) }}</td></tr>
        @endif
        <tr>
            <td></td>
            <td style="font-weight: bold">Kode Transaksi</td>
            <td style="font-weight: bold">Jam</td>
            <td style="font-weight: bold">Pelanggan</td>
            <td style="font-weight: bold">Total Barang</td>
            @if($transaction->add_val)
                <td style="font-weight: bold">{{$transaction->add_val}}</td>
            @endif
            <td style="font-weight: bold">Pembayaran</td>
            <td style="font-weight: bold">Total Transaksi</td>
        </tr>
        @php
            $products = \Illuminate\Support\Facades\DB::table('transactions')
                ->where('kode_transaksi', $transaction->kode_transaksi)
                ->get();
            $sum_jumlah = $products->sum('jumlah');
        @endphp
        <tr>
            <td></td>
            <td style="font-weight: bold">{{ $transaction->kode_transaksi }}</td>
            <td style="font-weight: bold">{{ date('H:i', strtotime($transaction->created_at)) }}</td>
            <td style="font-weight: bold">{{ $transaction->pelanggan }}</td>
            <td style="font-weight: bold">{{ $sum_jumlah }} Pcs</td>
            @if($transaction->add_val)
                <td style="font-weight: bold">Rp. {{ number_format($transaction->value_add_val,2,',','.') }}</td>
            @endif
            @if($transaction->po === 1)
                <td style="font-weight: bold">PO</td>
            @elseif($transaction->po === 2)
                <td style="font-weight: bold">CASH</td>
            @else
                <td style="font-weight: bold">TRANSFER</td>
            @endif
            <td style="font-weight: bold">Rp. {{ number_format($transaction->total,2,',','.') }}&nbsp;<small>{{ $transaction->diskon > 0? ' Diskon (' . $transaction->diskon . '%)':'' }}</small></td>
        </tr>
        <tr>
            <td style="text-align: center">#</td>
            <td>Nama</td>
            <td>Kode Barang</td>
            <td>Harga</td>
            <td>Qty</td>
            <td>Jumlah</td>
        </tr>
        @foreach($products as $product)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $product->nama_barang }}</td>
                <td>{{ $product->kode_barang }}</td>
                <td>Rp. {{ number_format($product->harga,2,',','.') }}</td>
                <td>{{ $product->jumlah }}</td>
                <td>Rp. {{ number_format($product->total_barang,2,',','.') }}</td>
            </tr>
        @endforeach
        {{ $date = date('d M, Y', strtotime($transaction->created_at)) }}
    @endforeach
</table>

{{--@foreach($dates as $date)--}}
{{--<table>--}}
{{--	<tr><td></td><td style="font-weight: bold">{{ date('d M, Y', strtotime($date)) }}</td></tr>--}}
{{--	@php--}}
{{--	$transactions = \App\Transaction::select('kode_transaksi', 'created_at', 'total', 'pelanggan', 'po', 'diskon', 'add_val', 'value_add_val')--}}
{{--	->whereDate('transactions.created_at', $date)--}}
{{--	->distinct()--}}
{{--	->latest()--}}
{{--	->get();--}}
{{--	@endphp--}}
{{--    @foreach($transactions as $transaction)--}}
{{--	<tr>--}}
{{--		<td></td>--}}
{{--        <td style="font-weight: bold">Kode Transaksi</td>--}}
{{--		<td style="font-weight: bold">Jam</td>--}}
{{--        <td style="font-weight: bold">Pelanggan</td>--}}
{{--        <td style="font-weight: bold">Total Barang</td>--}}
{{--        @if($transaction->add_val)--}}
{{--        <td style="font-weight: bold">{{$transaction->add_val}}</td>--}}
{{--        @endif--}}
{{--        <td style="font-weight: bold">Total Transaksi</td>--}}
{{--        <td style="font-weight: bold">Pembayaran</td>--}}
{{--    </tr>--}}
{{--    <tr>--}}
{{--        @php--}}
{{--        $products = \App\Transaction::where('kode_transaksi', $transaction->kode_transaksi)--}}
{{--        ->select('transactions.*')--}}
{{--        ->get();--}}
{{--        @endphp--}}
{{--		<td></td>--}}
{{--		<td style="font-weight: bold">{{ $transaction->kode_transaksi }}</td>--}}
{{--        <td style="font-weight: bold">{{ date('H:i', strtotime($transaction->created_at)) }}</td>--}}
{{--        @php--}}
{{--        $pemasukan += $transaction->total;--}}
{{--        if($transaction->po === 1) {--}}
{{--            $pemasukan_po += $transaction->total;--}}
{{--        }elseif($transaction->po === 2) {--}}
{{--            $pemasukan_cash += $transaction->total;--}}
{{--        }else {--}}
{{--            $pemasukan_transfer += $transaction->total;--}}
{{--        }--}}
{{--        $sumjml = \App\Transaction::where('kode_transaksi', $transaction->kode_transaksi)--}}
{{--        ->select('transactions.*');--}}
{{--        @endphp--}}
{{--        <td style="font-weight: bold">{{ $transaction->pelanggan }}</td>--}}
{{--        <td style="font-weight: bold">{{ $sumjml->sum('jumlah') }} Pcs</td>--}}
{{--        @if($transaction->add_val)--}}
{{--        <td style="font-weight: bold">Rp. {{ number_format($transaction->value_add_val,2,',','.') }}</td>--}}
{{--        @endif--}}
{{--        <td style="font-weight: bold">Rp. {{ number_format($transaction->total,2,',','.') }}&nbsp;<small>{{ $transaction->diskon > 0? ' Diskon (' . $transaction->diskon . '%)':'' }}</small></td>--}}
{{--        @if($transaction->po === 1)--}}
{{--        <td style="font-weight: bold">PO</td>--}}
{{--        @elseif($transaction->po === 2)--}}
{{--        <td style="font-weight: bold">CASH</td>--}}
{{--        @else--}}
{{--        <td style="font-weight: bold">TRANSFER</td>--}}
{{--        @endif--}}
{{--    </tr>--}}
{{--    <tr>--}}
{{--        <td>--}}
{{--            <table>--}}
{{--				<tr>--}}
{{--					<td></td>--}}
{{--					<td></td>--}}
{{--					<td></td>--}}
{{--					<td></td>--}}
{{--					<td></td>--}}
{{--					<td></td>--}}
{{--				</tr>--}}
{{--				<tr>--}}
{{--					<td style="text-align: center">#</td>--}}
{{--					<td>Nama</td>--}}
{{--					<td>Kode Barang</td>--}}
{{--					<td>Harga</td>--}}
{{--					<td>Qty</td>--}}
{{--					<td>Jumlah</td>--}}
{{--				</tr>--}}
{{--                @foreach($products as $product)--}}
{{--                <tr>--}}
{{--					<td style="text-align: center">{{ $loop->iteration }}</td>--}}
{{--					<td>{{ $product->nama_barang }}</td>--}}
{{--                    <td style="text-align:left">{{ $product->kode_barang }}</td>--}}
{{--                    <td>Rp. {{ number_format($product->harga,2,',','.') }}</td>--}}
{{--					<td style="text-align:left">{{ $product->jumlah }}</td>--}}
{{--                    <td>Rp. {{ number_format($product->total_barang,2,',','.') }}</td>--}}
{{--                </tr>--}}
{{--                @endforeach--}}
{{--            </table>--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--    @endforeach--}}
{{--</table>--}}
{{--@endforeach--}}
    <tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
        <td style="font-weight: bold">CASH: </td>
        <td style="font-weight: bold">Rp. {{ number_format($incomecash,2,',','.') }}</td>
    </tr>
    <tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
        <td style="font-weight: bold">TRANSFER: </td>
        <td style="font-weight: bold">Rp. {{ number_format($incometransfer,2,',','.') }}</td>
    </tr>
    <tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
        <td style="font-weight: bold">PO: </td>
        <td style="font-weight: bold">Rp. {{ number_format($incomepo,2,',','.') }}</td>
    </tr>
    <tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
        <td style="font-weight: bold">OMZET:</td>
        <td style="font-weight: bold">Rp. {{ number_format($income,2,',','.') }}</td>
    </tr>
