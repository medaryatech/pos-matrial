@php
$transactions = DB::table('transactions')
->select('kode_transaksi', 'created_at', 'total', 'diskon', 'pelanggan', 'po', 'kasir', 'add_val', 'value_add_val')
->whereBetween('created_at', [$start_date2, $end_date2])
->groupBy('kode_transaksi')
->orderBy('created_at', 'desc')
->paginate(20);
@endphp
@foreach($transactions as $transaction)
<li class="txt-light filter-date" style="display: none">{{ date('d M, Y', strtotime($transaction->created_at)) }}</li>
  <div class="table-responsive">
    <table class="table table-custom">
      <tr class="filter-header" style="display: none">
        <th>Kode Transaksi</th>
        <th>Total</th>
        <th>Qty</th>
        <th>Pelanggan</th>
        <th>Pembayaran</th>
        <th></th>
      </tr>
      <tr class="head">
        <td class="col-2">
            <span class="d-block font-weight-bold big-font">{{ $transaction->kode_transaksi }}</span>
            <span class="d-block mt-2 txt-light"><small>{{ date('d M, Y', strtotime($transaction->created_at)) . ' pada ' . date('H:i', strtotime($transaction->created_at)) }}</small></span>
        </td>
        <td class="col-3"><span class="ammount-box bg-green"><i class="mdi mdi-coin"></i></span>Rp. {{ number_format($transaction->total,2,',','.') }}<br /><small class="text-success ml-3 pl-4">{{ $transaction->diskon > 0? ' Diskon (' . $transaction->diskon . '%)':'' }}</small></td>
        <td class="col-2 text-success font-weight-bold">
            @php
            $products = \Illuminate\Support\Facades\DB::table('transactions')
                ->where('kode_transaksi', $transaction->kode_transaksi)
                ->get();
            $sum_jumlah = $products->sum('jumlah');
            @endphp
            {{ number_format($sum_jumlah, 0, ',', '.') }} Pcs</td>
        <td class="col-2 font-weight-bold ">{{ $transaction->pelanggan }}</td>
        @if($transaction->po === 1)
            <td class="col-2 text-danger font-weight-bold">PO</td>
        @elseif($transaction->po === 2)
            <td class="col-2 text-success font-weight-bold">CASH</td>
        @else
            <td class="col-2 text-success font-weight-bold">TRANSFER</td>
        @endif
        <td class="col-1">
            <button class="btn btn-selengkapnya font-weight-bold" type="button" data-target="#dropdownTransaksi{{ $transaction->kode_transaksi }}"><i class="mdi mdi-chevron-down arrow-view"></i></button>
        </td>
      </tr>
      <tr id="dropdownTransaksi{{ $transaction->kode_transaksi }}" data-status="0" class="dis-none">
        <td colspan="6" class="dropdown-content">
          <div class="d-flex justify-content-between align-items-center" style="justify-content: flex-start !important;">
              <div class="kasir mb-3" style="margin-right: 10px;">
                  Kasir : {{ $transaction->kasir }}
              </div>
              @if($transaction->add_val)
                  <div class="kasir mb-3" style="color: coral;">
                      {{$transaction->add_val}} : Rp. {{ number_format($transaction->value_add_val,2,',','.') }}
                  </div>
              @endif
              <div class="total-barang mb-3" style="margin-left: auto;">
                  <a onclick="return confirm('Are you sure?')" href="{{ url('/transaction/retur/' . $transaction->kode_transaksi) }}" class="btn" type="button" style="margin-left: 10px; background: #ff6258; color: #ffffff">Retur</a>
              </div>
              <div class="total-barang mb-3">
                  {{-- <a href="javascript:void(0)" onclick="printReceipt('{{ $transaction->kode_transaksi }}')" class="btn" type="button" style="margin-left: 7px; background: #435df2; color: #ffffff">Cetak Struk</a> --}}
                  <a href="{{ url('/transaction/receipt/' . $transaction->kode_transaksi) }}" target="_blank" class="btn" type="button" style="margin-left: 7px; background: #435df2; color: #ffffff">Cetak Struk</a>
              </div>
              <div class="total-barang mb-3">
                  <a href="{{ url('/transaction/receipt-full/' . $transaction->kode_transaksi) }}" target="_blank" class="btn" type="button" style="margin-left: 7px; background: #435df2; color: #ffffff">Cetak Invoice</a>
              </div>
              <div class="total-barang mb-3">
                  <a href="{{ url('/transaction/receipt-do/' . $transaction->kode_transaksi) }}" target="_blank" class="btn" type="button" style="margin-left: 7px; background: #435df2; color: #ffffff">Cetak Surat Jalan</a>
              </div>
              @if($transaction->po === 1)
                  <div class="total-barang mb-3">
                      <a href="{{ url('/transaction/pocash/' . $transaction->kode_transaksi) }}" class="btn" type="button" style="margin-left: 7px; background: #19d895; color: #ffffff">Bayar Cash</a>
                  </div>
                  <div class="total-barang mb-3">
                      <a href="{{ url('/transaction/potransfer/' . $transaction->kode_transaksi) }}" class="btn" type="button" style="margin-left: 7px; background: #19d895; color: #ffffff">Bayar Transfer</a>
                  </div>
              @endif
          </div>
          <table class="table">
            @foreach($products as $product)
            <tr>
              <td><span class="numbering">{{ $loop->iteration }}</span></td>
              <td>
                <span class="bold-td">{{ $product->nama_barang }}</span>
                <span class="light-td mt-1">{{ $product->kode_barang }}</span>
              </td>
              <td><span class="ammount-box-2 bg-secondary"><i class="mdi mdi-cube-outline"></i></span> {{ $product->jumlah }}</td>
              <td>
                <span class="light-td mb-1">Harga</span>
                <span class="bold-td">Rp. {{ number_format($product->harga,2,',','.') }}</span>
              </td>
              <td>
                <span class="light-td mb-1">Total Barang</span>
                <span class="bold-td">Rp. {{ number_format($product->total_barang,2,',','.') }}</span>
              </td>
            </tr>
            @endforeach
          </table>
        </td>
      </tr>
    </table>
  </div>
@endforeach
<script>
function printReceipt(idtrx) {
        // e.preventDefault();
        $.ajax({
            url: "{{ url('/transaction/receipt') }}/"+idtrx,
            method: "GET",
            success:function(response){
            if(response.status === 1) {
                swal(
                "Print Receipt!",
                "Successfully Printed Receipt",
                "success"
                );
            }
            }
        });
    };
</script>