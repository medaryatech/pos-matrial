<table>
	<tr>
        {{-- <td><img src="./images/cmi_bw.png"></td> --}}
		<td></td>
        <td>{{ $market->nama_toko }}</td>
    </tr>
	<tr>
		<td></td>
		<td>{{ $market->alamat }}</td>
    </tr>
	<tr>
		<td></td>
		<td>{{ $market->no_telp }}</td>
    </tr>
	<tr>
		<td></td>
    </tr>
    <tr>
		<td></td>
        <td>LAPORAN LABA</td>
    </tr>
    <tr>
		<td></td>
        @if(date('d M, Y', strtotime($tgl_awal)) === date('d M, Y', strtotime($tgl_akhir)))
        <td>Periode Laporan {{ date('d M, Y', strtotime($tgl_awal))}}</td>
        @else 
        <td>Periode Laporan {{ date('d M, Y', strtotime($tgl_awal)) . ' - ' . date('d M, Y', strtotime($tgl_akhir))}}</td>
        @endif
    </tr>
	<tr>
        @php
        $nama_users = explode(' ',auth()->user()->nama);
        $nama_user = $nama_users[0];
        @endphp
		<td></td>
        <td>Dicetak {{ \Carbon\Carbon::now()->isoFormat('DD MMM, Y') }} Oleh {{ $nama_user }}</td>
    </tr>
</table>
@php
$pemasukan = 0;
$pemasukan_hpp = 0;
$pemasukan_po = 0;
$pemasukan_cash = 0;
$pemasukan_transfer = 0;
$kode_transaksi = '';
$date = '';
@endphp
<table>
    @foreach($transactions as $transaction)
    <tr><td></td></tr>
    @if($date != date('d M, Y', strtotime($transaction->created_at)))
        <tr><td></td><td style="font-weight: bold">{{ date('d M, Y', strtotime($transaction->created_at)) }}</td></tr>
    @endif
	<tr>
		<td></td>
        <td style="font-weight: bold">Kode Transaksi</td>
		<td style="font-weight: bold">Jam</td>
        <td style="font-weight: bold">Pelanggan</td>
        <td style="font-weight: bold">Total Barang</td>
        @if($transaction->add_val)
        <td style="font-weight: bold">{{$transaction->add_val}}</td>
        @endif
        <td style="font-weight: bold">Total Transaksi</td>
        <td style="font-weight: bold">Total Laba</td>
    </tr>
    @php
        $products = \Illuminate\Support\Facades\DB::table('transactions')
            ->where('kode_transaksi', $transaction->kode_transaksi)
            ->get();
        $sum_jumlah = $products->sum('jumlah');
    @endphp
    <tr>
		<td></td>
		<td style="font-weight: bold">{{ $transaction->kode_transaksi }}</td>
        <td style="font-weight: bold">{{ date('H:i', strtotime($transaction->created_at)) }}</td>
        @php
        $pemasukan += $transaction->total;
        $pemasukan_hpp += $transaction->total_hpp;
        $sumjml = \App\Transaction::where('kode_transaksi', $transaction->kode_transaksi)
        ->select('transactions.*');
        @endphp
        <td style="font-weight: bold">{{ $transaction->pelanggan }}</td>
        <td style="font-weight: bold">{{ $sum_jumlah }} Pcs</td>
        @if($transaction->add_val)
        <td style="font-weight: bold">Rp. {{ number_format($transaction->value_add_val,2,',','.') }}</td>
        @endif
        <td style="font-weight: bold">Rp. {{ number_format($transaction->total,2,',','.') }}&nbsp;<small>{{ $transaction->diskon > 0? ' Diskon (' . $transaction->diskon . '%)':'' }}</small></td>
        <td style="font-weight: bold">Rp. {{ number_format(($transaction->total - $transaction->total_hpp),2,',','.') }}&nbsp;<small>{{ $transaction->diskon > 0? ' Diskon (' . $transaction->diskon . '%)':'' }}</small></td>
    </tr>
    <tr>
        <td>
            <table>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td style="text-align: center">#</td>
					<td>Nama</td>
					<td>Harga</td>
                    <td>HPP</td>
					<td>Qty</td>
					<td>Jumlah</td>
                    <td>Laba</td>
				</tr>
                @foreach($products as $product)
                <tr>
					<td style="text-align: center">{{ $loop->iteration }}</td>
					<td>{{ $product->nama_barang }}</td>
                    <td>Rp. {{ number_format($product->harga,2,',','.') }}</td>
                    <td>Rp. {{ number_format($product->hpp,2,',','.') }}</td>
					<td style="text-align:left">{{ $product->jumlah }}</td>
                    <td>Rp. {{ number_format($product->total_barang,2,',','.') }}</td>
                    <td>Rp. {{ number_format(($product->total_barang - $product->total_barang_hpp),2,',','.') }}</td>
                </tr>
                @endforeach
                {{-- {{ $date = date('d M, Y', strtotime($transaction->created_at)) }} --}}
            </table>
        </td>
    </tr>
    @endforeach
</table>

<table>
    <tr>
		<td></td>
        <td></td>
		<td></td>
		<td></td>
		<td></td>
        <td style="font-weight: bold">JUMLAH OMZET</td>
        <td style="font-weight: bold">Rp. {{ number_format($pemasukan,2,',','.') }}</td>
    </tr>
    <tr>
		<td></td>
        <td></td>
		<td></td>
		<td></td>
		<td></td>
        <td style="font-weight: bold">JUMLAH LABA</td>
        <td style="font-weight: bold">Rp. {{ number_format(($pemasukan - $pemasukan_hpp),2,',','.') }}</td>
    </tr>
</table>