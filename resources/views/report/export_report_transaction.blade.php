<!DOCTYPE html>
<html>
<head>
	@if(date('d M, Y', strtotime($tgl_awal)) === date('d M, Y', strtotime($tgl_akhir)))
	<title>Laporan Transaksi ({{ date('d M, Y', strtotime($tgl_awal))}})</title>
	@else
	<title>Laporan Transaksi ({{ date('d M, Y', strtotime($tgl_awal)) . ' - ' . date('d M, Y', strtotime($tgl_akhir))}})</title>
	@endif
	<style type="text/css">
		html{
			font-family: "Arial", sans-serif;
			margin: 0;
			padding: 0;
		}
		.header{
			background-color: #d3eafc;
			padding: 60px 90px;
		}
		.body{
			padding: 40px 90px;
		}
		/* Text */
		.text-20{
			font-size: 20px;
		}
		.text-18{
			font-size: 18px;
		}
		.text-16{
			font-size: 16px;
		}
		.text-14{
			font-size: 14px;
		}
		.text-12{
			font-size: 12px;
		}
		.text-10{
			font-size: 10px;
		}
		.font-bold{
			font-weight: bold;
		}
		.text-left{
			text-align: left;
		}
		.text-right{
			text-align: right;
		}
		.txt-dark{
			color: #5b5b5b;
		}
		.txt-dark2{
			color: #1d1d1d;
		}
		.txt-blue{
			color: #2a4df1;
		}
		.txt-light{
			color: #acacac;
		}
		.txt-green{
			color: #19d895;
		}
		.txt-red{
			color: #dc3131;
		}
		p{
			margin: 0;
		}

		.d-block{
			display: block;
		}
		.w-100{
			width: 100%;
		}
		.img-td{
			width: 40px;
		}
		.img-td img{
			width: 3rem;
		}
		.mt-2{
			margin-top: 10px;
		}
		.mb-1{
			margin-bottom: 5px;
		}
		.mb-4{
			margin-bottom: 20px;
		}
		.pt-30{
			padding-top: 30px;
		}
		.pt-15{
			padding-top: 15px;
		}
		.pt-5{
			padding-top: 5px;
		}
		.pb-5{
			padding-bottom: 5px;
		}
		table{
			border-collapse: collapse;
		}
		thead tr td{
			border-bottom: 0.5px solid #d9dbe4;
			color: #7e94f6;
			font-size: 12px;
			padding: 5px;
			text-transform: uppercase;
		}
		tbody tr td{
			padding: 7px;
		}
		.border-top-foot{
			border-top: 0.5px solid #d9dbe4;
		}
		.mr-20{
			margin-right: 20px;
		}
		ul{
			padding: 0;
		}
		ul li{
			list-style-type: none;
		}
		.w-300p{
			width: 300px;
		}
	</style>
</head>
<body>
	<div class="header">
		<table class="w-100">
			<tr>
				{{-- <td class="img-td text-left"><img src="./images/cmi_bw.png"></td> --}}
				<td class="text-left">
					<p class="text-12 txt-dark d-block" style="font-weight: bold">{{ $market->nama_toko }}</p>
					<p class="text-10 txt-dark d-block">{{ $market->alamat }}</p>
					<p class="text-10 txt-dark d-block">{{ $market->no_telp }}</p>
				</td>
				<td class="text-right">
					<p class="text-20 txt-blue font-bold">LAPORAN TRANSAKSI</p>
				</td>
			</tr>
			<tr>
				<td class="text-left txt-blue text-12 font-bold pt-30" colspan="2">Periode Laporan</td>
				<td class="text-right text-12 txt-dark pt-30">Dicetak {{ \Carbon\Carbon::now()->isoFormat('DD MMM, Y') }}</td>
			</tr>
			<tr>
				@if(date('d M, Y', strtotime($tgl_awal)) === date('d M, Y', strtotime($tgl_akhir)))
				<td class="text-left text-12 txt-dark2" colspan="2">{{ date('d M, Y', strtotime($tgl_awal))}}</td>
				@else
				<td class="text-left text-12 txt-dark2" colspan="2">{{ date('d M, Y', strtotime($tgl_awal)) . ' - ' . date('d M, Y', strtotime($tgl_akhir))}}</td>
				@endif
				@php
				$nama_users = explode(' ',auth()->user()->nama);
				$nama_user = $nama_users[0];
				@endphp
				<td class="text-right text-12 txt-blue">Oleh {{ $nama_user }}</td>
			</tr>
		</table>
	</div>
	<div class="body">
		<ul>
            @php
                $date = '';
            @endphp

            @foreach($transactions as $transaction)
                @if($date != date('d M, Y', strtotime($transaction->created_at)))
                    <li class="txt-light">{{ date('d M, Y', strtotime($transaction->created_at)) }}</li>
                @endif
                <table class="table table-custom">
                    @if($date != date('d M, Y', strtotime($transaction->created_at)))
                        <tr>
                            <th>Kode Transaksi</th>
                            <th>Qty</th>
                            <th>Pelanggan</th>
                            <th>Pembayaran</th>
                            <th>Total</th>
                        </tr>
                    @endif

                    <tr class="head">
                        <td class="col-2">
                            <span class="d-block font-weight-bold big-font">{{ $transaction->kode_transaksi }}</span>
{{--                            <span class="d-block mt-2 txt-light"><small>{{ date('d M, Y', strtotime($transaction->created_at)) . ' pada ' . date('H:i', strtotime($transaction->created_at)) }}</small></span>--}}
                        </td>
                        <td class="col-2 text-success font-weight-bold">
                            @php
                                $products = \Illuminate\Support\Facades\DB::table('transactions')
                                    ->where('kode_transaksi', $transaction->kode_transaksi)
                                    ->get();
                                $sum_jumlah = $products->sum('jumlah');
                            @endphp
                            {{ number_format($sum_jumlah, 0, ',', '.') }} Pcs</td>
                        <td class="col-2 font-weight-bold ">{{ $transaction->pelanggan }}</td>
                        @if($transaction->po === 1)
                            <td class="col-2 text-danger font-weight-bold">PO</td>
                        @elseif($transaction->po === 2)
                            <td class="col-2 text-success font-weight-bold">CASH</td>
                        @else
                            <td class="col-2 text-success font-weight-bold">TRANSFER</td>
                        @endif
                        <td class="col-3"><span class="ammount-box bg-green"><i class="mdi mdi-coin"></i></span>Rp. {{ number_format($transaction->total,2,',','.') }}<br /><small class="text-success ml-3 pl-4">{{ $transaction->diskon > 0? ' Diskon (' . $transaction->diskon . '%)':'' }}</small></td>
                    </tr>
                    <tr>
                        <td colspan="6" class="dropdown-content">
                            <table class="table">
                                <tr>
                                    <td>No.</td>
                                    <td>Barang</td>
                                    <td>Qty.</td>
                                    <td>Harga</td>
                                    <td>Total</td>
                                </tr>
                                @foreach($products as $product)
                                    <tr>
                                        <td><span class="numbering">{{ $loop->iteration }}</span></td>
                                        <td>
                                            <span class="bold-td">{{ $product->nama_barang }}</span>
                                            <span class="light-td mt-1">{{ $product->kode_barang }}</span>
                                        </td>
                                        <td><span class="ammount-box-2 bg-secondary"><i class="mdi mdi-cube-outline"></i></span> {{ $product->jumlah }}</td>
                                        <td>
                                            <span class="bold-td">Rp. {{ number_format($product->harga,2,',','.') }}</span>
                                        </td>
                                        <td>
                                            <span class="bold-td">Rp. {{ number_format($product->total_barang,2,',','.') }}</span>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                        </td>
                    </tr>
                </table>

                @php
                    $date = date('d M, Y', strtotime($transaction->created_at));
                @endphp
            @endforeach
		</ul>
{{--		<table class="w-100">--}}
{{--			<tfoot>--}}
{{--				<tr>--}}
{{--					<td class="border-top-foot"></td>--}}
{{--				</tr>--}}
{{--				<tr>--}}
{{--					<td class="text-14 pt-15 text-right">--}}
{{--						<span class="mr-20">CASH</span>--}}
{{--						<span class="txt-blue font-bold">Rp. {{ number_format($pemasukan_cash,2,',','.') }}</span>--}}
{{--					</td>--}}
{{--				</tr>--}}
{{--				<tr>--}}
{{--					<td class="text-14 pt-15 text-right">--}}
{{--						<span class="mr-20">TRANSFER</span>--}}
{{--						<span class="txt-blue font-bold">Rp. {{ number_format($pemasukan_transfer,2,',','.') }}</span>--}}
{{--					</td>--}}
{{--				</tr>--}}
{{--				<tr>--}}
{{--					<td class="text-14 pt-15 text-right">--}}
{{--						<span class="mr-20">PO</span>--}}
{{--						<span class="txt-blue font-bold">Rp. {{ number_format($pemasukan_po,2,',','.') }}</span>--}}
{{--					</td>--}}
{{--				</tr>--}}
{{--				<tr>--}}
{{--					<td class="text-14 pt-15 text-right">--}}
{{--						<span class="mr-20"></span>--}}
{{--						<span class="txt-blue font-bold"></span>--}}
{{--					</td>--}}
{{--				</tr>--}}
{{--				<tr>--}}
{{--					<td class="text-14 pt-15 text-right">--}}
{{--						<span class="mr-20">OMZET</span>--}}
{{--						<span class="txt-blue font-bold">Rp. {{ number_format($pemasukan,2,',','.') }}</span>--}}
{{--					</td>--}}
{{--				</tr>--}}
{{--			</tfoot>--}}
{{--		</table>--}}
	</div>
</body>
</html>
