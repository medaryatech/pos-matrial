<table>
    <tr>
        <th style="font-weight: bold">LAPORAN STOK</th>
    <tr>
</table>
<table class="table table-custom">
        <tr>
			<th style="font-weight: bold">Nama Barang</th>
            <th style="font-weight: bold">Kode Barang</th>
            @if($supply_system->status == true)
            <th style="font-weight: bold">Stok</th>
            @endif
            <th style="font-weight: bold">Harga Satuan</th>
            @if($supply_system->status == true)
            <th style="font-weight: bold">Jumlah</th>
            @endif
        </tr>
        @foreach($products as $product)
        <tr>
            <td>{{ $product->nama_barang }}</td>
			<td style="text-align: left">{{ $product->kode_barang }}</td>
            @if($supply_system->status == true)
            <td style="text-align: left">{{ $product->stok }}</td>
            @endif
            <td>Rp. {{ number_format($product->harga,2,',','.') }}</td>
            @if($supply_system->status == true)
            <td>Rp. {{ number_format(($product->harga * $product->stok),2,',','.') }}</td>
            @endif
        </tr>
        @endforeach
        @if($supply_system->status == true)
        <tr>
            <td></td>
            <td></td>
			<td></td>
            <td style="font-weight: bold">Total</td>
            <td style="font-weight: bold">Rp. {{ number_format($count,2,',','.') }}</td>
        </tr>
        @endif
</table>