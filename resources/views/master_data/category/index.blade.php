@extends('templates/main')
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header d-flex justify-content-between align-items-center">
      <h4 class="page-title">Master Data Category</h4>
      <div class="d-flex justify-content-start">
        <button type="button" class="btn btn-primary" onclick="tambahData()">Tambah Data</button>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12 mb-4">
    <div class="card card-noborder b-radius">
      <div class="card-body">
        <div class="row">
          <div class="col-12 table-responsive">
            <table class="table table-custom">
              <thead>
                <tr>
                  <th>No</th>     
                  <th>Nama</th>       
                  <th>Kategory</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($category as $key => $val)
                  <tr>
                      <td>{{ $key+1 }}</td>
                      <td>{{ $val->nama ?? '' }}</td>
                      <td>{{ $val->keterangan ?? '' }}</td>
                      <td>
                        <button type="button" class="btn btn-sm btn-warning" onclick="editData({{ $val->id }})"><span class="mdi mdi-pencil"></span></button>
                        <button type="button" class="btn btn-sm btn-danger btnDelete" data-id="{{ $val->id }}"><span class="mdi mdi-close"></span></button>
                      </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <div class="modal fade" id="modalTambah" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">Tambah Data</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="form-create" action="{{ route('category_store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="modal-body px-5">
            <div class="form-group">
              <label for="nama" class="col-form-label">Nama Kategori</label>
              <input type="text" class="form-control" name="nama" id="nama" required>
            </div> 

            <div class="form-group">
              <label for="keterangan" class="col-form-label">Keterangan</label>
                <textarea class="form-control" name="keterangan" id="keterangan" cols="30" rows="10"></textarea>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" id="btnSubmit">Tambah Data</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Hapus Data</h5>
          <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="#" method="POST" id="formDeleteBr" enctype="multipart/form-data">
          @method('delete')
          @csrf
          <div class="modal-body px-5">
            <p>Apakaah anda yakin ingin menghapus data ini ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Hapus Data</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('script')
  <script>
      function tambahData() {
      $('#modalTitle').html('Tambah Data');
      $('#nama, #keterangan').val('');
      $('#btnSubmit').html('Tambah Data');
      $('div #modalTambah').modal('show');
    }

    function editData(id) {
      $('#modalTitle').html('Edit Data');
      let url = "{{ route('category_getData',':id') }}";
      url = url.replace(':id', id);

      let url_update = "{{ route('category_update',':id') }}";
      url_update = url_update.replace(':id', id);

      $.ajax({
        url : url,
        type: 'get',
        success:function(res) {
          $('#nama').val(res.nama);
          $('#keterangan').val(res.keterangan);
          
          $('#form-create').attr('action', url_update);
          $('#btnSubmit').html('Update Data');
          $('div #modalTambah').modal('show');
        }
      });
    }
    
    $('.btnDelete').on('click', function() {
      let id = $(this).data('id');
      let url = "{{ route('category_delete',':id') }}";
      url = url.replace(':id', id);
      $('#formDeleteBr').attr('action', url);
      $('#modalDelete').modal('show');
    });
  </script>
@endsection