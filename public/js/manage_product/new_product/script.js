(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));

$(".number-input").inputFilter(function(value) {
  return /^-?\d*$/.test(value); 
});

$(document).on('input propertychange paste', '.number-input', function(e){
  var val = $(this).val()
  var reg = /^0/gi;
  if (val.match(reg)) {
      $(this).val(val.replace(reg, ''));
  }
});

function stopScan(){
  Quagga.stop();
}

function startScan() {
  Quagga.init({
    inputStream : {
      name : "Live",
      type : "LiveStream",
      target: document.querySelector('#area-scan')
    },
    decoder : {
      readers : ["ean_reader"],
      multiple: false
    },
    locate: false
  }, function(err) {
      if (err) {
          console.log(err);
          return
      }
      console.log("Initialization finished. Ready to start");
      Quagga.start();
  });

  Quagga.onDetected(function(data){
    $('#area-scan').prop('hidden', true);
    $('.barcode-result').prop('hidden', false);
    $('#btn-scan-action').prop('hidden', false);
    $('.barcode-result-text').html(data.codeResult.code);
    stopScan();
  });
}

$(document).on('click', '.btn-scan', function(){
  $('#area-scan').prop('hidden', false);
  $('.barcode-result').prop('hidden', true);
  $('#btn-scan-action').prop('hidden', true);
  $('.barcode-result-text').html('');
  startScan();
});

$(document).on('click', '.btn-continue', function(){
  $('input[name=kode_barang]').val($('.barcode-result-text').text());
  $('.close-btn').click();
});

$(document).on('click', '.btn-repeat', function(){
  $('#area-scan').prop('hidden', false);
  $('.barcode-result').prop('hidden', true);
  $('#btn-scan-action').prop('hidden', true);
  $('.barcode-result-text').html('');
  startScan();
});

$('#scanModal').on('hidden.bs.modal', function (e) {
  stopScan();
})

$('input[name=excel_file]').change(function(){
    var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
    $('.excel-name').html(filename);
    $('.btn-upload').prop('hidden', false);
});

$(document).on('click', '.excel-file', function(e){
  e.preventDefault();
  $('input[name=excel_file]').click();
});

$(function() {
  $("form[name='create_form']").validate({
    rules: {
      kode_barang: "required",
      nama_barang: "required",
      jenis_barang: "required",
      stok: "required",
      hpp: "required",
      harga: "required",
      // harga_grosir: "required",
      // harga_reseller: "required"
    },
    messages: {
      kode_barang: "Kode barang tidak boleh kosong",
      nama_barang: "Nama barang tidak boleh kosong",
      jenis_barang: "Silakan pilih jenis barang",
      stok: "Stok barang tidak boleh kosong",
      hpp: "HPP barang tidak boleh kosong",
      harga: "Harga barang tidak boleh kosong",
      // harga_grosir: "Harga grosir tidak boleh kosong",
      // harga_reseller: "Harga barang tidak boleh kosong"
    },
    errorPlacement: function(error, element) {
        var name = element.attr("name");
        $("#" + name + "_error").html(error);
    },
    submitHandler: function(form) {
      if (grosirValidate() && variansValidate()) form.submit();
    }
  });
});

// grosir start ------------------------------
$(document).on('focus', '.min-grosir, .harga-grosir', function() {
  $(this).removeClass('error');
  resetGrosirError();
})

function grosirValidate() {
  if ($('#status').val() === '0') return true

	if (!grosirRequired()) {
		$('#harga_grosir_error').show().text('Min dan Harga Grosir tidak boleh kosong')
		return
	}

  if (grosirCompareValue() !== 0) {
    $('#harga_grosir_error').show().text(ERROR_MESSAGE[grosirCompareValue() - 1])
		return
  }
  return true
}

function grosirRequired() {
	let valid = true
	$('.container-gross .gross').each(function(i, el) {
		// min-grosir required --------------------
		const elMinGrosir = $(el).find('.min-grosir');
		const valueMinGrosir = parseInt(elMinGrosir.val());
		if (!valueMinGrosir) {
			elMinGrosir.addClass('error')
			valid = false
		}
		// harga-grosir required --------------------
		const elHargaGrosir = $(el).find('.harga-grosir');
		const valueHargaGrosir = parseInt(elHargaGrosir.val());
		if (!valueHargaGrosir) {
			elHargaGrosir.addClass('error')
			valid = false
		}
	})
	return valid
}

function grosirCompareValue() {
  let valid = 0, valueMinBefore, valueHargaGrosirBefore;
	$('.container-gross .gross').each(function(i, el) {
		// min-grosir compare --------------------
    const elMinGrosir = $(el).find('.min-grosir');
    const valueMinGrosirNow = parseInt(elMinGrosir.val());
    if (i === 0) {
      if (valueMinGrosirNow < 2) {
        elMinGrosir.addClass('error')
        valid = 1
        return false
      }
    } else {
      if (valueMinGrosirNow <= valueMinBefore) {
        elMinGrosir.addClass('error')
        valid = 3
        return false
      }
    }
    valueMinBefore = valueMinGrosirNow;
    
		// harga-grosir compare --------------------
		const elHargaGrosir = $(el).find('.harga-grosir');
		const valueHargaGrosirNow = parseInt(elHargaGrosir.val());
		const valueHargaJual = parseInt($('[name=harga]').val().replace(/[^0-9]+/g, ''));
    if (i === 0) {
      if (valueHargaGrosirNow >= valueHargaJual) {
        elHargaGrosir.addClass('error')
        valid = 2
        return false
      }
    } else {
      if (valueHargaGrosirNow >= valueHargaGrosirBefore) {
        elHargaGrosir.addClass('error')
        valid = 4
        return false
      }
    }
    valueHargaGrosirBefore = valueHargaGrosirNow
	})
	return valid
}

function resetGrosirError() {
  $('#harga_grosir_error').hide().text('')
}

$(document).on('focus', '.nama-varian, .conv-varian, .hpp-varian, .sku-varian, .harga-varian', function() {
  $(this).removeClass('error');
  resetVariansError();
})
function variansValidate() {
  if ($('#varians').val() === '0') return true

	if (!variansRequired()) {
		$('#harga_varians_error').show().text('Varians tidak boleh kosong')
		return
	}

  return true
}
function grosirRequired() {
	let valid = true
	$('.container-gross .gross').each(function(i, el) {
		// min-grosir required --------------------
		const elMinGrosir = $(el).find('.min-grosir');
		const valueMinGrosir = parseInt(elMinGrosir.val());
		if (!valueMinGrosir) {
			elMinGrosir.addClass('error')
			valid = false
		}
		// harga-grosir required --------------------
		const elHargaGrosir = $(el).find('.harga-grosir');
		const valueHargaGrosir = parseInt(elHargaGrosir.val());
		if (!valueHargaGrosir) {
			elHargaGrosir.addClass('error')
			valid = false
		}
	})
	return valid
}
function variansRequired() {
	let valid = true
	$('.container-varians .varians').each(function(i, el) {
		// nama-varian required --------------------
		const elNamaVarian = $(el).find('.nama-varian');
		const valueNamaVarian = elNamaVarian.val();
		if (!valueNamaVarian) {
			elNamaVarian.addClass('error')
			valid = false
		}
    // conv-varian required --------------------
		const elConvVarian = $(el).find('.conv-varian');
		const valueConvVarian = parseInt(elConvVarian.val());
		if (!valueConvVarian) {
			elConvVarian.addClass('error')
			valid = false
		}
		// hpp-varian required --------------------
		const elHppVarian = $(el).find('.hpp-varian');
		const valueHppVarian = parseInt(elHppVarian.val());
		if (!valueHppVarian) {
			elHppVarian.addClass('error')
			valid = false
		}
    // sku-varian required --------------------
		const elSkuVarian = $(el).find('.sku-varian');
		const velueSkuVarian = parseInt(elSkuVarian.val());
		if (!velueSkuVarian) {
			elSkuVarian.addClass('error')
			valid = false
		}
		// harga-varian required --------------------
		const elHargaVarian = $(el).find('.harga-varian');
		const valueHargaVarian = parseInt(elHargaVarian.val());
		if (!valueHargaVarian) {
			elHargaVarian.addClass('error')
			valid = false
		}
	})
	return valid
}
function resetVariansError() {
  $('#harga_varians_error').hide().text('')
}

const ERROR_MESSAGE = [
  'Min harus lebih dari 1',
  'Harga Grosir harus kurang dari harga jual',
  'Min harus lebih dari min sebelumnya',
  'Harga Grosir harus kurang dari harga grosir sebelumnya',
]
// grosir end ------------------------------